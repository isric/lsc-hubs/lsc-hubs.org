# lsc-hubs.org cms

A cms for https://lsc-hubs.org.

## Hugo
The CMS is based on [Hugo CMS](https://gohugo.io). In Hugo and alikes, content is not stored in a database, but stored as text files on a git repository. This has 2 big advantages. Content changes are tracable (who did what when). The published website is a plain html website, which is very safe and fast. 


## Content editing
At every commit (to the master branch) a new version of the website is generated and published at the development environment https://dev.lsc-hubs.org (wait 5-10 minutes). Every commit starting with `feat:` or `fix:` results in a new [release tag](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/-/tags) and a deployment to [production](https://lsc-hubs.org). Example commit message:

```
fix: Added a new dashboard item related to the recent visit to Rwanda
```
You can follow the publish process at https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/-/pipelines (in some cases publication errors occur, in such a scenario check here, or ask for support)

Notice that you can also install `hugo` locally (linux: `sudo apt install hugo`, windows: `chocolatery install hugo`). Clone this git repository, then navigate with console to the repository and run `hugo serve`. You can now make changes in the content and see instant updates on http://localhost:1313.

## Modifications in detail

Content files are stored as [markdown](https://en.wikipedia.org/wiki/Markdown) in [content](content).

The website layout (by [monomo](https://momono.online/)) is applied in html and css templates using a customised hugo theme based on bootstrap.

Pages are placed in /content/{topic}/{name}.md, and genrally have this header (hugo calls it `frontmatter`):

```
---
title: My Second Post
date: 2021-12-24T11:50:18+01:00
author: Mr. Shou
draft: false # change to true to hide
weight: # indicates the order of appearence
---
```

Images (as .gif,.png.svg,.jpg) should be uploaded to /static/images and can be referenced as /images/{name}.png. 

Use either the markdon style to include an image

```
![alt text](http://example.com/image)
```

or a hugo shortcode (with some specific styling)

```
{{% fig style="width:100%" align="tc-l" title="Wageningen University" src="/images/WUR_Website_LogoCard.png" %}}
```

## Dashboard
The progress dashboard is a special section in this website. Each dashboard item is an individual file in the /content/dashboard folder. In the configuration you can specify a type of card ( text, image (+image: /images/{name}.png), number and a background color (background: 'bg-success bg-opacity-25')). Also we did experiments with [charts](layouts/shortcodes/chart.html) and maps on these cards.

## Homepage 
The sections of the layout of the homepage are hardcoded in [index.html](layouts/index.html). The actual text content is taken from [homepage](content/homepage).

## Partners
A dedicated file is available for each team-member and partner (role: partner), on each partner you can use the [staff-shortcode](layouts/shortcodes/staff.html) to add staff members.  

## Search
Search is implemented clientside, an index of search terms is included with the search page. The search box in the header first opens the search page before triggering a search. 

## Hub prototpes

We've started prototyping the hubs at https://lsc-hubs.org/hubs/kenya, although we'll soon be migrating that to [dedicated environments](https://lsc-hubs.github.io/hub-core/). ([kenya hub](https://kenya.lsc-hubs.org) is already migrated)

## Need help?
Create an issue on [issues](issues) or ask [Ingrid](mailto:ingrid.haas@isric.org) or [Paul](mailto:paul.vangenuchten@isric.org). Also improvement suggestions for this content are very welcomed.

## Some findings while setting up ci-cd

- To trigger submodule build, add `GIT_SUBMODULE_STRATEGY=recursive` to variables
- To add `--build-arg=HUGO_ENV_ARG=production` to kaniko build
- building for production fails with a message that extended version is needed to compile scss.


## History

2025-01-07 cleaned up harbor repo for older images