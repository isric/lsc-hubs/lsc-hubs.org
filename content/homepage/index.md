---
title : 'Data for land decisions' 
headless : true
heroHeading: 'Data for land decisions'
heroSubHeading: 'New land, soil and crop (LSC) information services in East Africa'
---

New land, soil and crop (LSC) information services in East Africa