---
title: 'The project team'
weight: 3
background: ''

---



{{% rw class="mt-2 partners"  %}}
{{% cl3 class="bg-white text-dark whitebox" %}}

{{% fig style="width:100%" align="tc-l" title="Wageningen University" src="/images/WUR_Website_LogoCard.png" %}}

### Wageningen University & Research

*The Netherlands*

Wageningen University & Research is a collaboration between Wageningen University and Wageningen Research with three core areas of research carried out internationally: 1) food, feed & biobased production, 2)
natural resources & living environment and 3) society & well-being.


[Learn more](https://www.wur.nl)

{{% /cl3 %}}
{{% cl3 class="bg-white text-dark whitebox" %}}

{{% fig style="width:100%" align="tc-l" title="ISRIC" src="/images/4_ISRIC_ImageCards.jpg" %}}

### ISRIC - World Soil Information

*The Netherlands*

ISRIC - World Soil Information is an independent foundation and a custodian of global soil information which they produce, gather, compile and serve together with our partners at global, national and regional
levels.

[Learn more](https://www.isric.org)

{{% /cl3 %}}
{{% cl3 class="bg-white text-dark whitebox" %}}

{{% fig style="width:100%" align="tc-l" title="ILRI" src="/images/ILRI_Website_LogoCard.png" %}}

### International Livestock Research Institute (ILRI)

International Livestock Research Institute (ILRI) works for better lives through livestock in developing countries. ILRI is a CGIAR research centre, co-hosted by Kenya and Ethiopia,
 and provides global research partnership for a food-secure future.

[Learn more](https://www.ilri.org)

{{% /cl3 %}}
{{% cl3 class="bg-white text-dark whitebox" %}}

{{% fig style="width:100%" align="tc-l" title="EIAR" src="/images/EIAR_website_LogoCard.png" %}}

### Ethiopian Institute of Agricultural Research (EIAR)

*Ethiopia*

As a national research institute, Ethiopian Institute of Agricultural Research (EIAR) 
aspires to see improved livelihood of all Ethiopians engaged in agriculture, 
agro-pastoralism, and pastoralism through market-competitive agricultural technologies.

[Learn more](http://www.eiar.gov.et/)

{{% /cl3 %}}
{{% cl3 class="bg-white text-dark whitebox" %}}

{{% fig style="width:100%" align="tc-l" title="" src="/images/KALRO_website_LogoCard.png" %}}

### Kenya Agricultural & Livestock Research Organization (KALRO)

*Kenya*

Kenya Agricultural & Livestock Research Organization (KALRO) is a corporate body created under the Kenya Agricultural and Livestock Research Act of 2013 to establish a framework for coordination of agricultural research
in Kenya that is dynamic, innovative, responsive and well-coordinated.

[Learn more](https://www.kalro.org/)

{{% /cl3 %}}
{{% cl3 class="bg-white text-dark whitebox" %}}

{{% fig style="width:100%" align="tc-l" title="" src="/images/RAB_logo.jpg" %}}

### Rwanda Agriculture and Animal Resources Development Board (RAB)

*Rwanda*

The Rwanda Agriculture and Animal Resources Development Board (RAB) champions the Rwandan agriculture sector into a knowledge-based, technology-driven and market-oriented industry, using modern methods in crop, animal,
fisheries, forestry and soil and water management.
                
[Learn more](http://www.rab.gov.rw/index.php?id=180)

{{% /cl3 %}}
{{% /rw %}}


