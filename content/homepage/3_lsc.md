---
title: 'Land, Soil and Crop Information Services to support Climate-Smart Agriculture'
weight: 3
background: ''
---



{{% cl2 w="6" class=" " %}} 

The objective of this project is to develop sustainable land, soil, crop information hubs in national agricultural research organizations in East Africa to enhance the effectiveness of national Agricultural Knowledge and Innovation Systems (AKIS) and contribute to rural transformation and climate-smart agriculture.

The consortium is comprised of three international partners and three national implementing partners in East Africa:

- ISRIC - World Soil Information 
- Wageningen University & Research
- International Livestock Research Institute (ILRI)
- Ethiopian Institute of Agricultural Research (EIAR)
- Kenya Agriculture and Livestock Research Organisation (KALRO)
- Rwanda Agriculture and Animal Resources Development Board (RAB)

{{% /cl2 %}}
{{% cl2 w="6" class="" %}}
This four-year project (2021-2024) is jointly funded by the European Union's Development Smart Innovation through Research in Agriculture (DeSIRA) program, The Netherlands' Ministry of Foreign Affairs and a contribution from ISRIC. Read more about the project's theory of change on the  DeSIRA website  and about the project framework on the  ISRIC website.

{{< button class="btn btn-primary btn-lg rounded-0 arrow-right-light mt-5" relref="#funders" >}}More about the project funders{{< /button >}}

{{% /cl2 %}}


