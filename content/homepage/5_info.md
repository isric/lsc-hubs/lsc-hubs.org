---
title: 'Focus countries'
weight: 5
background: ''
---

The expected impact of this LSC-IS initiative is a contribution to an increased agricultural productivity and farm income especially for small scale farmers in Ethiopia, Kenya, and Rwanda.
