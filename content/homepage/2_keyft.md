---
title: 'Key features of LSC Data Hubs'
weight: 2
background: ''
title1 : 'National-level data hub'
block1 : 'In Ethiopia, Kenya and Rwanda, land, soil and crop data from many sources are being consolidated into data portals, or hubs, where these data can be stored, processed and visualized.'
title2 : 'Technology driven solutions'
block2 : 'LSC Hubs take advantage of the latest data technology so that people who need knowledge about land, soil and crops have the best information possible. These hubs support agricultural decision-making and innovation.'
title3 : 'Improved accessibility to agricultural data'
block3 : 'A land soil and crop data hub links existing data sources and portals to facilitate easier access while maintaining national data ownership.'
title4 : 'Information for climate-smart agriculture'
block4 : 'These data hubs support diverse practices which help the agricultural sector adapt and build resilience to climate change. The hubs account for changing environmental conditions and facilitate relevant climate change mitigation.'
---

<div>
<a href="https://ethiopia.lsc-hubs.org" class="btn btn-primary btn-sm rounded-0 arrow-right-light">Ethiopia hub</a>
</div><div>
<a href="https://kenya.lsc-hubs.org" class="btn btn-primary btn-sm rounded-0 arrow-right-light">Kenya hub</a>
</div><div>
<a href="https://rwanda.lsc-hubs.org" class="btn btn-primary btn-sm rounded-0 arrow-right-light">Rwanda hub</a>
</div>