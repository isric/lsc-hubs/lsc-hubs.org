---
title: 'Data for land decisions'
weight: 1
background: ''
btn_features : 'Learn more about LSC Hubs'
btn_dashboard : 'Project progress'
---

New land, soil and crop (LSC) information services in East Africa
