---
title: "DLR begins covariate preparation"
weight: 1002
background: 
media: image
value: /images/logo_dlr_flat.png
featured: true
date: 2023-07-20
---

German Space Agency (DLR) has begun their work with Land Soil Crop Hubs, incorporating Earth Observation inputs (e.g. satellite and remote sensing data) to provide the project with innovative soil mapping methodologies. Preliminary products will be available for review later this year. Check back for an update!
