---
title: "KENYA: Donors and partners project monitoring visit"
weight: 987
background:
author: Silvana Summa
media: image
value: /images/01-lsc-is-kenya-visit.webp
featured: true
date: 2024-03-20
---

Donors and partners of the LSC-IS project have recently visited project sites in Taita Taveta County, Kenya, to monitor the progress made through their support.

[Read more](2024/03/20/insightful-monitoring-visit-by-lsc-is-project-donors-and-partners-in-kenya/)
