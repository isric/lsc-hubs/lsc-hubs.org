---
title: "RWANDA: Regions of focus"
weight: 1005
background: ''
media : 'image'
value : '/images/_Rwanda_2023.png'
featured: true

---
The two districts of focus in Rwanda for piloting the information services are:

- Musanze
- Rwamagana
