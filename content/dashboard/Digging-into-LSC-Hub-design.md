---
title: "Digging into LSC Hub design"
weight: 1000
background: 
author: Emily Toner
media: image
value: /images/AboutTheCatalogue.png
featured: true
date: 2023-09-15
---

Behind the scenes on a data and information hub, the catalogue is the principal component of the platform's architecture. 

[Learn about the development of the LSC Hubs catalogue prototype.](https://lsc-hubs.org/2023/09/11/lsc-catalogue-prototype-developed/)
