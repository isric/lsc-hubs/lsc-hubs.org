---
title: "RWANDA: LSC-IS DSM products workshop"
weight: 985
background:
author: Silvana Summa
media: image
value: /images/2024_07_Rwanda_DSM-workshop.webp
featured: true
date: 2024-07-04
---

A three-day LSC-IS Digital Soil Mapping (DSM) products workshop was recently held by project team members from ISRIC - World Soil Information and Rwanda Agriculture and Animal Resources Development Board (RAB) in Huye, Rwanda. The workshop focused on evaluating suitable datasets in conjunction with soil survey and science experts.


