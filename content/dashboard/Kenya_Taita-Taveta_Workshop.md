---
title: "KENYA: Workshop in Taita-Taveta County"
weight: 1012
background: 
media: image
value: /images/group2.jpg
featured: true
date: 2022-10-13
---

- Date: 13-14 October 2022
- Participants: 48

The two days’ stakeholder workshop in Taita-Taveta County outlined the importance of engaging with county level and engaging local level stakeholders in making their needs regarding data availability and access explicit. “Engaging the end-users in the LSC hub design makes this project different” was mentioned during the evaluation of the workshop. 
