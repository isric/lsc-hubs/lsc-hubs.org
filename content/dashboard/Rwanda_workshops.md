---
title: 'RWANDA: National and regional stakeholder workshops'
weight: 1004
background: 
media: image
value: /images/kigaliwshop.png
featured: true
date: 2022-11-01
---

A series of consultation workshops was held in Rwanda at national and regional levels during November 2022. They served to identify the needs and demands among stakeholders in the country with regards to land, soil and crop information.
[Read more](https://lsc-hubs.org/2022/12/20/rwanda-workshop-series/) 
