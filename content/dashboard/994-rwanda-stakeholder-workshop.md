---
title: "RWANDA: National training workshop on LSC Hub held in Kigali"
weight: 994
background: 
author: Thaïsa van der Woude
media: image
value: /images/lsc53381cropped.webp
featured: true
date: 2023-12-06
---

The Land, Soil, and Crop Information Services (LSC-IS) to support Climate Smart Agriculture (CSA) project organised a national workshop in Rwanda from 30 November to 1 December 2023.

The workshop, hosted by Rwanda Agriculture and Animal Resources Development Board (RAB), was coordinated by the International Livestock Research Institute and supported by the staff of ISRIC - World Soil Information, Wageningen Centre for Development Innovation (WCDI), Wageningen Environmental Research, World Agroforestry (CIFOR-ICRAF), Association for Strengthening Agricultural Research in Eastern and Central Africa (ASARECA) and International Union for Conservation of Nature (IUCN).


[Read more](https://lsc-hubs.org/2023/12/07/national-training-workshop-on-lsc-hub-held-in-rwanda/)
