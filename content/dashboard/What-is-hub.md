---
title: "What is a land, soil, crop 'hub'?"
weight: 1001
background: 
media: image
value: /images/Hub-visualv04.webp
featured: true
date: 2023-08-08
---

The land, soil and crop (LSC) Hubs are easy-to-access online platforms,  managed by the Ethiopia Institute of Agricultural Research (EIAR), Kenya Agriculture and Livestock Research Organisation (KALRO) and Rwanda Agriculture Board (RAB), enabling stakeholders to access information on soil, crops, land and climate such as data, maps and advisory services.

[Learn more](https://lsc-hubs.org/2023/08/07/explainer-what-is-a-land-soil-crop-hub/)
