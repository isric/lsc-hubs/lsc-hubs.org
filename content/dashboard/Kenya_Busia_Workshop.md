---
title: "KENYA: Workshop in Busia County"
weight: 1012
background: 
media: image
value: /images/consortium.png
featured: true
date: 2022-09-27
---


- Date: 27-28 September 2022
- Participants: 41

The outcome of the two days’ stakeholder workshop in Busia County was a set of clearly defined user requirements and needs that guide the design of the LSC-hub in Kenya 
