---
title: "Metadata inventory"
weight: 1006
background: 'bg-success bg-opacity-25'
media: image
value: /images/LSC_Hubs_Metadata_Form.png
featured: true
date: 2023-03-23
---

LSC-hubs developed a process to enable project partners to describe their data’s metadata systematically. The process includes:

- An inventory metadata template in Excel or
- An Open Data Kit (ODK) form;

By standardizing the metadata collection for the data which will eventually feature in the Land Soil Crop Hub (LSC Hub), the metadata can be seamlessly uploaded to the LSC Hubs. 

[Learn more](/2023/03/23/process-established-for-metadata-inventory/)
