---
title: "RWANDA: Information access for farmers"
weight: 995
background: 
author: Emily Toner
media: image
value: /images/lsc53364970357_049eaa060c_b.webp
featured: true
date: 2023-12-01
---

Mr. Francois Uwumukiza, from the Embassy of the Kingdom of The Netherlands in Rwanda says:
 'Our transformation journey is still long but having a functional Information hub is a first step towards farmers accessing information, and data. The hub requires the commitment of all relevant government officials to make sure that it is functional for farmers by encouraging its use, maintaining it, and entering data regularly so farmers can benefit.'
