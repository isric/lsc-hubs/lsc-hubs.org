---
title: "KENYA: Two sub-national workshops on LSC hub"
weight: 990
background:
author: Silvana Summa
media: image
value: /images/2024_02_wp5_workshop_kenya_03.webp
featured: true
date: 2024-02-19
---

The Land Soil and Crop (LSC) Hubs project organised two sub-national workshops in early February of this year as a counterpart to the national workshop held in Nairobi in December 2023. The four-day workshops occurred in Voi, Taita Taveta county, and Busia, Busia county.

[Read more](https://lsc-hubs.org/2024/02/02/insights-from-sub-national-lsc-hub-workshops-in-kenya/)
