---
title: "Ethiopia's Ag-Data Hub technical expert consultation workshop: A step toward transforming agriculture through data innovation"
weight: 977
background:
author: Silvana Summa
media: image
value: /images/4-DSCF1537.jpg
featured: true
date: 2025-02-04
---

On 22 January 2025, representatives from various organisations convened in Addis Ababa for the technical expert consultation workshop of Ethiopia's Ag-Data Hub. The Ag-Data Hub is set to improve data harvesting, curation, and access to legacy databases and external sources. To ensure seamless integration and data exchange between systems involved in agricultural data production, the LSC-Hub is being aligned with Ethiopia's broader data ecosystem, particularly within the Ethiopian Institute of Agricultural Research (EIAR).

<a href="/2025/01/22/ethiopias-ag-data-hub-technical-expert-consultation-workshop-a-step-toward-transforming-agriculture-through-data-innovation/">
Read more</a>