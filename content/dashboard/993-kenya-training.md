---
title: "KENYA: Data management training delivered"
weight: 993
background: 
author: Thaïsa van der woude
media: image
value: /images/Kenya_training_2023.png
featured: true
date: 2023-12-14
---

From 11-14, December, members of Kenya Agriculture and Livestock Research Organization (KALRO), the Kenya Department of Remote Sensing and Resource Surveys, the Kenya Forest Service and the Kenya Met Department came together at a data management training led by ISRIC - World Soil Information as part of the Land Soil Crop Hubs (LSC Hubs) project.  

[Read more](https://lsc-hubs.org/2023/12/14/data-management-training-completed-in-kenya/)
