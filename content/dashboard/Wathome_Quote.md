---
title: 'KENYA: Improving soil fertility management'
weight: 1014
background: 'bg-success bg-opacity-25'
media : image
value: /images/Stephan.png
featured: true
---
Mr. Stephen Wathome, Agriculture, Job Creation & Resilience Section, Delegation of the European Union to Kenya says: 

"Improving soil fertility management and reducing land degradation continues to be a priority for the EU. It is through such programmes as DeSIRA LSC - IS that support is being extended...Generation and appropriate dissemination of Land, Soil and Crop information is crucial for effective decision making and innovation."
