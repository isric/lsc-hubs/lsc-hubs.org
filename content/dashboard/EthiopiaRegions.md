---
title: "ETHIOPIA: Regions of focus"
weight: 1003
background: 
media: image
value: /images/_Ethiopia_2023.png
featured: true
---

The two pilot regions for LSC Hubs in Ethiopia are Basona Worena and Adami Tulu Jido Kombolcha.
