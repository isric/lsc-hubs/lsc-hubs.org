---
title: "ETHIOPIA: National workshop to gather feedback on LSC hub"
weight: 988
background:
author: Silvana Summa
media: image
value: /images/2024_02_Ethiopia_Workshop_WP4_01.jpg
featured: true
date: 2024-02-16
---

Twenty-two participants provided feedback on Ethiopia’s LSC hub prototype for further improvement and development during a national workshop in Adama, Ethiopia.

[Read more](2024/02/16/national-workshop-to-gather-feedback-on-ethiopias-lsc-hub/)
