---
title: "RWANDA: Digital soil mapping training delivered"
weight: 994
background: 
author: Emily Toner
media: image
value: /images/rwanda-training-4887.webp 
featured: true
date: 2023-12-06
---

From November 21-23, 2023 the project carried out training on digital soil mapping in Musanze, Rwanda. Project team members from ISRIC - World Soil Information delivered the training to Rwandan professionals with most coming from LSC Hubs project partner Rwanda Agriculture and Animal Resources Development Board (RAB). The focus was joint development of a new set of high-resolution digital soil maps of Rwanda.

[Read more](https://lsc-hubs.org/2023/12/04/digital-soil-mapping-training-completed-in-rwanda/)
