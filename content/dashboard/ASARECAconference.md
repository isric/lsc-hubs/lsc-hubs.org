---
title: "LSC Hubs expands network at ASARECA conference"
weight: 1004
background: 
media: image
value: /images/52936977024_a4ef10738c_o.jpg 
featured: true
date: 2023-06-08
---

On 18 May 2023, LSC Hubs project team presented a session at the ASARECA Agriculture Ministerial Conference in Uganda titled: Land, Soil, Crop Information Services for Climate-Resilient Food Systems in Africa. The presentation reached 15 Ministires of Agriculture in East and Central Africa.

[Learn more about the session.](https://lsc-hubs.org/2023/06/08/asareca-conference-provides-valuable-connections-for-agriculture-knowledge-and-innovation/)
