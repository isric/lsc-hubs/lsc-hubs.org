---
title: 'KENYA: Hub architectural design'
weight: 1009
background: 
author: Thaïsa van der Woude
date: 2023-03-20
media : image
value: /images/KE_Hub_architectural_design.png
featured: true
---

During the January 2023 meeting of the KALRO and ISRIC teams, a draft plan was created that focuses on catalogue service. Catalogue service is the main element of an LSC Hub that provides FAIR data management functionality to the land, soil and crop datasets available at KALRO. 
