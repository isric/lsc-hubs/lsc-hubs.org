---
title: "ETHIOPIA: Regional stakeholder workshops"
weight: 1008
background: 
media: image
value: /images/ziwaybanner.jpg
featured: true
date: 2023-01-31
---

In January 2023, two regional level workshops were organised in Ethiopia, in:

- Batu (Ziway) and 
- Basona Werena

[Learn more](https://lsc-hubs.org/2023/02/13/lsc-hubs-to-benefit-different-agroecological-zones-in-ethiopia/)
