---
title: 'KENYA: Assessing technical capacity with KALRO'
weight: 1008
background: ''
date: 2023-01-17
author: Thaïsa van der Woude
media : image
value: /images/KE_Assess_technical_capacity_KALRO.png
featured: true
---

ISRIC and KALRO met from 17-20 January 2023 in Nairobi to: 

- assess technical capacity (human, software, hardware) to operate, maintain and use the hub; 
- understand current portals/apps/tools developed by KALRO that support agricultural decision-making; 
- initiate discussions about the architectural design for Kenya’s LSC Hub. 

The technical capacity assessment showed that the KALRO IT team is a good team with highly diverse skills. 
