---
title: "ETHIOPIA: LSC-IS DSM training"
weight: 983
background:
author: Silvana Summa
media: image
value: /images/2024_10_Ethiopia_DSM-workshop.webp
featured: true
date: 2024-10-14
---

From 7-11 October 2024, the LSC-IS project team conducted a five-day training on Digital Soil Mapping (DSM) at the Ethiopian Institute of Agricultural Research (EIAR). The training covered key aspects of DSM, including data preparation, spatial modelling using machine learning, and the application of automated tools like the DSM workflow developed by ISRIC.


