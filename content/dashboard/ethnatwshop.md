---
title: "ETHIOPIA: National stakeholder workshop"
weight: 1009
background: 
media: image
value: /images/ethworkshopbanner.png
featured: true
date: 2023-01-31
---

- Date: 23-31 January 2023
- Participants: 60

Policymakers and agricultural experts highlighted the urgent need to expand data-driven decisionmaking, to help Ethiopian agriculture cater to the country's food and nutrition needs amid climate change and land degradation. [Read more](https://lsc-hubs.org/2023/02/06/new-ethiopian-agriculture-data-hub-will-support-climate-smart-decisions/)

