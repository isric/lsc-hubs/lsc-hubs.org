---
title: "High resolution covariates from DLR"
weight: 982
background:
author: Betony Colman
media: image
value: /images/dlr-kenya.jpg
featured: true
date: 2024-11-05
---

Last year the German Space Agency (DLR) began using ScMap to produce innovative Sentinel-2 derived, 20m resolution, products for soil property mapping. This year, we tested the mean reflectance and standard-deviation products as covariates for digital soil mapping. In most tests, these covariates improved soil property map accuracy. DLR also provided bare surface and vegetation masks and a bare surface reflectance composite, crucial for modelling in partially vegetated areas by isolating bare surface data from vegetation. The details will follow - a scientific paper is in the works at DLR. 


