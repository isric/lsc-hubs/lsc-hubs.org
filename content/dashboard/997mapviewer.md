---
title: "Mapviewer for Land, Soil and Crop maps"
weight: 997
background:
author: Thaïsa van der Woude
media: image
value: /images/Mapviewer-LSC-IS.webp
featured: true
date: 2023-11-01
---

Based on project partner feedback, a mapviewer was created to visualize and compare various land, soil and crop maps. This first version of the mapviewer contains soil maps created in this project, and spatial data available from the LSC data catalogue. The mapviewer will be futher developed based on input from project partners.