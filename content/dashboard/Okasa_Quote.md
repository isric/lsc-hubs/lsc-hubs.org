---
title: 'KENYA: Improved data availability and access is essential!'
weight: 1016
background: ''
media : 'image'
value : '/images/okosa.jpg'
featured: true
---

Elizabeth Okosa, KALRO (Picture by ASARECA)

"Most of the data used to understand and improve agricultural practices is static, heterogeneous, and not timely hence, difficult for farmers to apply it to improve their agricultural practices". (Elizabeth Okosa, KALRO). This project works with the conviction that land, soil and crop information services can help to improve the efficacy of CSA related policies, plans and practices. 
