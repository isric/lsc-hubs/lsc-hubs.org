---
title: 'KENYA: Food basket for the coastal region'
weight: 1008
background: ''
media : 'image'
value : '/images/presentation.jpg'
featured: true
---

John Mlamba, Director Climate Change and Environment - County Government of Taita-Taveta (picture by ASARECA) says:

“Taita Taveta County (Kenya) was the food basket for the coastal region, which is not the case anymore because of over-cultivation and soil degradation. This project is a great opportunity for us to provide the farmers with sound information on soil to improve our agriculture”.
