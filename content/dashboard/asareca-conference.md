---
title: 'ASARECA Agricultural Ministerial Conference'
weight: 1007
background: 
media: image
value: /images/asareca.png
featured: true
date: 2023-05-08
---

A delegation from the Land Soil Crop Hubs project will present at the ASARECA Agriculture Ministerial Conference in Uganda from 17-19 May, 2023.
[Read more](/2023/05/25/lsc-information-services-for-climate-resilient-food-systems-at-asareca-conference/)
