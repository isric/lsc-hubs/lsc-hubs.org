---
title: "Researchers across Africa convene in Mombasa to address climate change and soil health challenges"
weight: 979
background:
author: Silvana Summa
media: image
value: /images/mombassa-meeting.jpg
featured: true
date: 2024-09-23
---

The LSC-IS team participated in the [AICCRA](https://aiccra.cgiar.org/) training on Climate-Smart Agriculture and Soil Health in Eastern and Southern Africa, which took place in Mombasa, Kenya, on 23-27 September 2024. The LSC-IS project team members led the design and delivery of the training. Participants included representatives from the Ethiopian Institute of Agriculture Research (EIAR), Kenya Agricultural & Livestock Research Organisation (KALRO), Rwanda Agriculture and Animal Resources Development Board (RAB), International Union for Conservation of Nature (IUCN), ASARECA, International Livestock Research Institute (ILRI), and ICRAF.

<a href="/2024/12/12/researchers-across-africa-convene-in-mombasa-to-address-climate-change-and-soil-health-challenges/">Read more</a>