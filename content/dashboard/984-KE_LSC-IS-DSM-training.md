---
title: "KENYA: LSC-IS DSM training"
weight: 984
background:
author: Silvana Summa
media: image
value: /images/2024_08_Kenya_DSM-workshop.jpg
featured: true
date: 2024-08-12
---

From 5 to 9 August 2024, ISRIC - World Soil Information and the Kenya Agricultural and Livestock Research Organization (KALRO) organised and facilitated a five-day training on Digital Soil Mapping (DSM) in Naivasha, Kenya. The workshop was designed to help participants utilise DSM for improved decision-making and agricultural practices in Kenya .


