---
title: "KENYA: 2-day workshop in Voi, Tiata Taveta county"
weight: 991
background:
author: Silvana Summa
media: image
value: /images/kenya_workshop_feb_2024.jpeg
featured: true
date: 2024-02-02
---

A two-day workshop was recently held in Voi, Tiata Taveta county, Kenya. The LSC-IS Kenya team worked with local infomediaries and stakeholders in mapping soil, water and cropping challenges faced in the region. 
The team also crafted concrete visions for its future landscapes. They explored what information could be drawn from the LSC-IS hub to cater to the actions needed to achieve said visions and whether that information was sufficient for Taita Taveta's context.
