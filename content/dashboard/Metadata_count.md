---
title: "Datasets collected"
weight: 1003
background: 'bg-success bg-opacity-25'
media: number
value: 240+
featured: true
date: 2023-03-23
---

We compiled more than 240 available land, soil, and crop information sources for each project country: Ethiopia, Kenya and Rwanda. This data consists of national and global spatial vector data (point, line, polygon), gridded maps and related metadata, and is collected from various sources:

- Country project partners (EIAR, KALRO and RAB);
- ISRIC – World Soil Information;
- Project partners (e.g. ICRAF and IUCN);
- Third parties (online sources).
