---
title: "New user-friendly design implemented for LSC hub Kenya"
weight: 989
background:
author: Thaïsa van der Woude
media: image
value: /images/2024_02_Kenya_hub_new-design.webp
featured: true
date: 2024-02-29
---

The Land Soil and Crop (LSC) Hubs project implemented a new user-friendly design for the LSC hub based on feedback obtained during the WP4 stakeholders meetings. This improved design was first implemented in Kenya and will soon be available for the hubs in Rwanda and Ethiopia. Improvements in design will continue to be developed in the near future after processing more feedback from users.

[View new design](https://kenya.lsc-hubs.org)
