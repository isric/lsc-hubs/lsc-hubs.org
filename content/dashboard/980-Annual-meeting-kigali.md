---
title: "Project partners convene in Kigali for LSC-IS Annual Meetin"
weight: 980
background:
author: Silvana Summa
media: image
value: /images/LSC-IS-APM-01.jpg
featured: true
date: 2024-12-09
---

Land Soil Crop Hubs (LSC Hubs) project partners gathered in Kigali, Rwanda, from 25-26 November 2024 for the Annual Project Meeting to evaluate progress made in 2024 and plan for the project's final year in 2025.

[Read more](/2024/12/02/project-partners-convene-in-kigali-for-land-soil-crop-information-services-annual-meeting/)