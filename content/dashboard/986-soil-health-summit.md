---
title: "Land, Soil, Crop Information Services for Climate-Smart Agriculture at Africa Fertilizer & Soil Health Summit"
weight: 986
background:
author: Silvana Summa
media: image
value: /images/LSC-IS-AFSHS.webp
featured: true
date: 2024-04-15
---

The Land, Soil, and Crop Information Services for Climate-Smart Agriculture in Africa project will be presented at the Africa Fertilizer and Soil Health Summit in Kenya on 7-9 May 2024. On 8 May, the LSC-IS team will present at the side event 'Monitoring soil health and soil health investments at different scales', organised by the European Commission, ISRIC, and CIFOR-ICRAF. The LSC-IS presentation, led by Dr. Michael Okoti from the Kenya Agricultural and Livestock Research Organization (KALRO), will focus on national soil information systems.

[Learn more about the event](2024/04/15/land-soil-crop-information-services-for-climate-smart-agriculture-at-africa-fertilizer-soil-health-summit/)

