---
title: "DSM workflow presented at IUSS workshop on Soil Mapping for Sustainable Land Use Planning in India"
weight: 978
background:
author: Silvana Summa
media: image
value: /images/2025-DSM-workflow-IUSS-workshop-India.jfif
featured: true
date: 2025-01-28
---

The Digital Soil Mapping (DSM) seedling workflow, used for DSM training in the LSC-IS project, was presented at the 3rd joint workshop of the International Union of Soil Sciences (IUSS) Working Groups Digital Soil Mapping and Global Soil Map. The workshop under the theme 'Soil Mapping for Sustainable Land Use Planning' took place from 21 to 24 January 2025 in Bengaluru, India.

The workflow provides users with comprehensive guidance on how to effectively generate soil maps.
<!--<a href="/2024/12/12/researchers-as/">Read more</a>-->