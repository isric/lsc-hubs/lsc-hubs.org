---
title: "KENYA: Counties of focus"
weight: 1015
background: 
media: image
value: /images/_Kenya_2023.png
featured: true
---

Based on the criteria presented below, two-counties were chosen i.e. Busia and Taita Taveta. The selection of pilot counties in Kenya has been based on: 1. Representation of various agro-ecological zones; 2. GDP per-capita that is lower than national average though has potential for agricultural development; 3. Potential for agricultural production.  4. Characterised by diverse farming systems. 5. Climate risk profile available  6. Presence of other projects that can complement and synergise with DeSIRA LCS-IS. 7. Security and ease of project implementation.  
