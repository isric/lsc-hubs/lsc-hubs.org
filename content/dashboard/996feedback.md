---
title: "Hub user experience feedback"
weight: 996
background: 
author: Emily Toner
media: image
value: /images/shutterstock_2250874923.webp
featured: true
date: 2023-11-01
---

The Land Soil Crop Hub prototypes that were developed for use in Kenya, Ethiopia and Rwanda are now in the iterative process of national-level partners, especially KALRO and RAB, shaping the user experience. The hub design is evolving based on their input. The beta version of the LSC hub will be released by the end of 2023. 


