---
title: 'KENYA: National workshop in Nairobi'
weight: 1013
background: 
media: image
value: /images/workshop2.jpg
featured: true
date: 2022-09-19
---

- Date: 19-20 September 2022
- Participants: 41

Some quotes of the workshop included: “Many different actors and stakeholder group have been part of this workshop  with space and time for open conversation - both formally and informally”. “We have to unfortunately underline that important data simply do not reach farmers”. “If farmers want to obtain data they generally have to pay a -not small- amount in KSh!”
<a href="https://lsc-hubs.org/2022/09/19/desira-lsc-is-project-engages-more-than-130-land-soil-crop-data-users-in-kenya/">Read more</a> 
