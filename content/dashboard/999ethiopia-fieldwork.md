---
title: "ETHIOPIA: Research fieldwork underway"
weight: 999
background: 
author: Emily Toner
media: image
value: /images/taye-pic-03.webp
featured: true
date: 2023-10-23
---

'...[the] research is novel in that it goes beyond mapping only basic soil properties but extends this to more complex soil properties, such as the root zone plant available water holding capacity. This is highly relevant information for farmers and land managers,' explained Prof. dr. Gerard Heuvelink.

[Read more ](https://lsc-hubs.org/2023/10/23/fieldwork-underway-to-support-climate-smart-fertiliser-recommendations-in-ethiopia)
