---
title: "VIDEO TOUR: Land Soil Crop Hub Prototype"
weight: 998
background: 
author: Emily Toner
media: image
value: /images/VideoGraphic-LSC-Hubs-Prototype.webp
featured: true
date: 2023-10-31
---

In this five-minute video, ISRIC - World Soil Information project coordinator Thaisa van der Woude introduces the Land Soil Crop Hubs (LSC Hubs) prototype.

[Take the tour ](https://www.youtube.com/watch?v=ux4JV1efDDA)
