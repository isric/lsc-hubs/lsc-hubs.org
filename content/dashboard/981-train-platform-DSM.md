---
title: "New online training platform for DSM training"
weight: 981
background:
author: Betony Colman
media: image
value: /images/ISRIC_Academy_screenshot.jpg
featured: true
date: 2024-11-05
---

In October, during the Digital Soil Mapping (DSM) workshop in Ethiopia, we piloted an online training platform hosted by ISRIC - World Soil Information. This platform will allow those unable to attend in person to complete or review the training materials at their convenience. The platform enriched the training experience, offering access to various teaching tools in one centralised location, allowing attendees to follow along on their screens in a more engaging and effective learning experience. 

Although some fine-tuning is needed, we believe the online learning platform can greatly improve knowledge sharing and training accessibility.
