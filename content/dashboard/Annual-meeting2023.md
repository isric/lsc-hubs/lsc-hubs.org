---
title: "Team connects at Annual Project Meeting in The Netherlands"
weight: 1005
background: 
media: image
value: /images/news9RAB-IUCN.webp
featured: true
date: 2023-06-08
---

Land Soil Crop Hubs (LSC Hubs) project partners gathered in Wageningen, The Netherlands from May 9 - 13, 2023 for the 'Annual Project Meeting'. Partners travelled from Ethiopia, Kenya, Rwanda and Uganda to be present and the group followed an interactive program to dig in on the next steps for each of the project's work packages.

[Read more about the meeting.](https://lsc-hubs.org/2023/05/25/project-partners-gathered-in-the-netherlands-for-2023-annual-project-meeting/)
