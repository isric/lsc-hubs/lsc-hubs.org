---
title: "KENYA: National training workshop on LSC hub"
weight: 992
background: 
author: Silvana Summa
media: image
value: /images/2023-12-Kenya-WP4-Workshop.webp
featured: true
date: 2023-12-20
---

The Land, Soil, and Crop Information Services (LSC-IS) to support Climate Smart Agriculture (CSA) project organised a national workshop in Kenya on 18-19 December 2023. 30 participants received hands-on training on how to access Kenya’s LSC hub and how to use the information contained in the hub.

[Read more](https://lsc-hubs.org/2023/12/20/national-training-workshop-on-lsc-hub-held-in-kenya/)
