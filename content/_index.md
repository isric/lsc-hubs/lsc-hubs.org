---
title: 'Home'
date: 2018-02-12T15:37:57+07:00
heroHeading: 'Data for land decisions'
heroSubHeading: 'New land, soil and crop (LSC) information services in East Africa'
heroBackground: 'images/lsc-front.jpg'
---

