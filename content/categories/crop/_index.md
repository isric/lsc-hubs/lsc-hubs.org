---
title : 'Crop' 
headless : true
weight: 3
---

# Crop

A crop is any cultivated plant, fungus, or alga that is harvested for food, clothing, livestock,fodder, biofuel, medicine, or other uses

See also [definition of crops](http://aims.fao.org/aos/agrovoc/c_1972) in Agrovoc