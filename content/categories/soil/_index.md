---
title : 'Soil' 
weight: 2
---

Upper layer of the earth in which plants grow.

See also definition of soil in [Agrovoc]](http://aims.fao.org/aos/agrovoc/c_7156) or [wikipedia](https://en.wikipedia.org/wiki/Soil)