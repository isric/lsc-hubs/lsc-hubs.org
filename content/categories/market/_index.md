---
title : 'Market access' 
weight: 5
---

# Market access

is part of trade policies (economic policies)

See also [definition of market access](https://agrovoc.fao.org/browse/agrovoc/en/page/c_1312459073954) in Agrovoc