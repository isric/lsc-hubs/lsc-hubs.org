---
title : 'Remote/Proximal Sensing' 
headless : true
weight: 5
---

# Remote/Proximal Sensing

The gathering and analysis of data from the study area or organism that is physically removed from the sensing equipment, e.g. sub-water surface detection instrument, aircraft or satellite

See also [definition of remote sensing](https://agrovoc.fao.org/browse/agrovoc/en/page/c_6498) in Agrovoc