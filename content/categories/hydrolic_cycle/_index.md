---
title : 'Hydrolic cycle' 
weight: 1
---

# Hydrolic cycle

The circuit of water movement from the atmosphere through various stages or processes on the ground (such as precipitation, interception, runoff, infiltration, percolation, storage) and then back to the atmosphere again by evaporation, and transpiration.

See also [definition of Hydrolic cycle](http://aims.fao.org/aos/agrovoc/c_11670) in Agrovoc