---
title : 'Climate' 
headless : true
weight: 4
---

# Climate

Climate in a narrow sense is usually defined as the average weather, or more rigorously, as the statistical description in terms of the mean and variability of relevant quantities over a period of time ranging from months to thousands or millions of years.

See also [definition of climate](https://agrovoc.fao.org/browse/agrovoc/en/page/c_1665) in Agrovoc