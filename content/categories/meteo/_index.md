---
title : 'meteo' 
weight: 1
---

# Agrometeorology

Agrometeorology deals with all the weather-sensitive elements of agriculture production: pollination, animal migration, human and animal health, transport of pathogens by wind, irrigation, micro-climate manipulation and artificial climates, weather risk assessments, the use of weather forecasts in farming, crop yield and phenology forecasts and particularly advice to farmers.

See also [definition of agrometeorology](http://aims.fao.org/aos/agrovoc/c_8689) in Agrovoc