---
title : 'Land' 
weight: 1
---

# Land

The surface of the Earth, the materials beneath, the air above and all things to the soil

See also [definition of land](http://aims.fao.org/aos/agrovoc/c_4172) in Agrovoc