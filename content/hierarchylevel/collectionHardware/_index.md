---
title : 'Collection Hardware' 
headless : true
---

A device to collect data

See also [definition in iso19139:2007](https://schemas.opengis.net/iso/19139/20070417/resources/Codelist/gmxCodelists.xml#MD_ScopeCode_collectionHardware) 