---
title : 'Document' 
headless : true
---

A document is a written, drawn, presented, or memorialized representation of thought, often the manifestation of non-fictional, as well as fictional, content.

See also [definition in Agrovoc](http://aims.fao.org/aos/agrovoc/c_4f435f4f) 