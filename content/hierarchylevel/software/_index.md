---
title : 'Software' 
headless : true
---

Software or computer applications to facilitate LSC processes

See also [Computer applications definition in Agrovoc](http://aims.fao.org/aos/agrovoc/c_24009) 