---
title : 'Model' 
headless : true
---

A model is a selected simplified representation of the essential or relevant entities (modules) of some specific reality and their characteristics (fields, factors, features).

See also [definition in Agrovoc](http://aims.fao.org/aos/agrovoc/c_4881) 