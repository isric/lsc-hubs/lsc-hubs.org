---
title : 'Service' 
headless : true
---

Services are provided by organisations to facilitate the LSC processes. Services include 
financial services, climate services, cooperative services, ecosystem services, food services, information services, professional services, public services, social services, watershed services. 

See also [definition in Agrovoc](http://aims.fao.org/aos/agrovoc/c_6989) 