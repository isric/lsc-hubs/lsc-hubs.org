---
title : 'Series' 
headless : true
---

Provenance in archival science refers to the "origin or source of something; information regarding the origins, custody, and ownership of an item or collection". As a fundamental principle of archives, provenance refers to the individual, family, or organization that created or received the items in a collection. In practice, provenance dictates that records of different origins should be kept separate to preserve their context. As a methodology, provenance becomes a means of describing records at the *series* level.

See also [definition in wikipedia](https://en.wikipedia.org/wiki/Archival_science#Provenance_in_archival_science) 