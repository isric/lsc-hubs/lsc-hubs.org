---
title : 'Global' 
headless : true
---

Global means of or referring to earth or of some other celestial body

See also [definition in Wikipedia](https://en.wikipedia.org/wiki/Global) 