---
title : 'Local' 
headless : true
---

Administration and politics at the level below national government, e.g. county and borough councils

See also [local government definition in Agrovoc](http://aims.fao.org/aos/agrovoc/c_4412) 