---
title : 'Continental' 
headless : true
---

A continent is any of several large geographical regions. Continents are generally identified by convention rather than any strict criteria. A continent could be a single landmass or a part of a very large landmass, as in the case of Asia or Europe

See also [definition in Wikipedia](https://en.wikipedia.org/wiki/Continent) 