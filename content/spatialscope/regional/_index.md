---
title : 'Regional' 
headless : true
---

A form of local government which covers a wide area and groups together smaller units of administration.

See also [definition in Agrovoc](http://aims.fao.org/aos/agrovoc/c_6489) 