---
title: 'LSC Information Hub Rwanda'
date: 2023-07-05
type: rwanda
---

This is the homepage of the Land, Soil, Crop (LSC) Information Hub - Rwanda [**prototype**]. 

The LSC hub is currently under development by the LSC-Hub project team. Check back regularly to find new content arriving.

You can provide feedback by contributing to hub discussions at the [github repository](https://github.com/lsc-hubs/rwanda-hub/discussions)


{{% rw %}}
{{% cl w="2" class="text-center text-center border rounded bg-light" %}} 
<i class="fa fa-database fa-2xl pb-4"></i>

## Data

Find data in the [catalogue](https://rwanda.lsc-hubs.org/) or
go to the [mapviewer](https://maps.lsc-hubs.org/#lsc-rwanda)

{{% /cl %}}
{{% cl w="2" class="text-center text-center border rounded bg-light" %}} 
<i class="fa fa-gear fa-2xl pb-4"></i>

## Predictive modelling

Find modelling software in the [hub catalogue](https://rwanda.lsc-hubs.org/collections/metadata:main/items?type=model).

{{% /cl %}}

{{% cl w="2" class="text-center text-center border rounded bg-light" %}} 
<i class="fa fa-solid fa-balance-scale fa-2xl pb-4"></i>

## Policy

Which policies are relevant to the hub, and can the hub support policy development?

[Read more](policy)

{{% /cl %}}

{{% cl w="2" class="text-center text-center border rounded bg-light" %}} 
<i class="fa fa-solid fa-shapes fa-2xl pb-4"></i>

## Information services

Current [agricultural knowledge and innovation systems](akis) in Rwanda.

{{% /cl %}}

{{% cl w="2" class="text-center text-center border rounded bg-light" %}} 
<i class="fa fa-solid fa-diagram-project fa-2xl pb-4"></i>

## Use cases

Explore [LSC Use Cases](/hubs/rwanda/usecases)


{{% /cl %}}

{{% cl w="2" class="text-center text-center border rounded bg-light" %}} 
<i class="fa fa-users fa-2xl pb-4"></i>

## Hub community

Find other users in the [Stakeholder list](/hubs/rwanda/stakeholders/)


{{% /cl %}}

{{% /rw %}}


