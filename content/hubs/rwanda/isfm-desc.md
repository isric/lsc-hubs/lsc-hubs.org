---
title: 'Rwanda Integrated Soil Fertility Management description​'
date: 2023-07-05
type: rwanda
---

## Problem

Current fertilizer and soil fertility recommendations are not linked to local context, conditions and climate risks. Too general information may lead to declining soil fertility and soil health, low and uncertain productivity, fertilizer wastage, loss of organic matter, and environmental risks. Integrated Soil Fertility Management (ISFM) has the potential to improve effectivity and efficiency of agronomic practices including both fertilizer recommendations and organic matter management and thereby boost crop production and farm income. Also, ISFM has the potential to deliver multiple co-benefits including climate adaptation (through improved water holding capacity and soil cover) as well as climate mitigation (through carbon sequestration).​

## Goal

Improved agronomic management advisories including crop type and soil fertility management to farmers via agricultural extension staff or directly.​

## Scope

Land users and their intermediaries can use the system to obtain information on the soil fertility and alternatives to improve soil fertility. It is based on existing data (soil, terrain, crop, climate), apps (decision support tools, etc.) and models (WOFOST, DSSAT etc) accessible in the LSC-hub. These will include risk assessment of weather and other external factors.​


## Scale 

- Field/farm, 
- Sub-national (county/district/woreda)
- National levels​

## Primary actors

- national: Extension staff, agricultural planner, agro-input provider 
- district: District extension officer, District Agric & Livestock officers and planners
- local: Local extension officer, local agro-input provider, lead farmer.​     
​

## Secondary actors
- Hub-hosts and administrator: EIAR, KALRO, RAB; related data providers.​

## Beneficiary

- Farmers.​

## Scenario ​

The intermediary user accesses the LSC-Hub’s web-based data system or portal, and finds existing information on Land, Soil and crops. The intermediary actor – extension agent/ lead farmer/ agricultural input provider - converts this information into an agronomic and fertilizer recommendation and provides advise to end users, in particular farmers.​

​
## User story soil acidity

Problem: `soil acidity` 

Solution option: targeting lime 

HOW: Use spatial datasets from the LSC-IS hub Crop land and from the land cover map. Soil pH < 5 from the pH map SOC < 20g/kg from the SOC map Identify crop lands that require pH and SOC improvements

We've prepared a map of landcover and pH in Rwanda at
[Map of landcover and pH](https://maps.lsc-hubs.org/#start=%7B%22version%22%3A%228.0.0%22%2C%22initSources%22%3A%5B%7B%22stratum%22%3A%22user%22%2C%22models%22%3A%7B%22__User-Added_Data__%22%3A%7B%22isOpen%22%3Atrue%2C%22members%22%3A%5B%22%2Fhttps%3A%2F%2Fmaps.lsc-hubs.org%2Fows%2Frwanda%2FPHH2O%3Fservice%3DWMS%26version%3D1.3.0%26request%3DGetCapabilities%22%2C%22%2Fhttps%3A%2F%2Fworldcover2020.esa.int%2Fgeoserver%2Fgwc%2Fservice%2Fwms%3FSERVICE%3DWMS%26VERSION%3D1.1.1%22%5D%2C%22knownContainerUniqueIds%22%3A%5B%22%2F%22%5D%2C%22type%22%3A%22group%22%7D%2C%22%2Fhttps%3A%2F%2Fmaps.lsc-hubs.org%2Fows%2Frwanda%2FPHH2O%3Fservice%3DWMS%26version%3D1.3.0%26request%3DGetCapabilities%22%3A%7B%22isOpen%22%3Afalse%2C%22url%22%3A%22https%3A%2F%2Fmaps.lsc-hubs.org%2Fows%2Frwanda%2FPHH2O%3Fservice%3DWMS%26version%3D1.3.0%26request%3DGetCapabilities%22%2C%22knownContainerUniqueIds%22%3A%5B%22__User-Added_Data__%22%5D%2C%22type%22%3A%22wms-group%22%7D%2C%22%2Fhttps%3A%2F%2Fworldcover2020.esa.int%2Fgeoserver%2Fgwc%2Fservice%2Fwms%3FSERVICE%3DWMS%26VERSION%3D1.1.1%22%3A%7B%22isOpen%22%3Atrue%2C%22url%22%3A%22https%3A%2F%2Fworldcover2020.esa.int%2Fgeoserver%2Fgwc%2Fservice%2Fwms%3FSERVICE%3DWMS%26VERSION%3D1.1.1%22%2C%22knownContainerUniqueIds%22%3A%5B%22__User-Added_Data__%22%5D%2C%22type%22%3A%22wms-group%22%7D%2C%22%2Fhttps%3A%2F%2Fmaps.lsc-hubs.org%2Fows%2Frwanda%2FPHH2O%3Fservice%3DWMS%26version%3D1.3.0%26request%3DGetCapabilities%2FPHH2O_mean_0-20cm%22%3A%7B%22show%22%3Atrue%2C%22splitDirection%22%3A1%2C%22knownContainerUniqueIds%22%3A%5B%22%2Fhttps%3A%2F%2Fmaps.lsc-hubs.org%2Fows%2Frwanda%2FPHH2O%3Fservice%3DWMS%26version%3D1.3.0%26request%3DGetCapabilities%22%5D%2C%22type%22%3A%22wms%22%7D%2C%22%2Fhttps%3A%2F%2Fworldcover2020.esa.int%2Fgeoserver%2Fgwc%2Fservice%2Fwms%3FSERVICE%3DWMS%26VERSION%3D1.1.1%2Fesa_worldcover2021%3Aesa_worldcover2021%22%3A%7B%22show%22%3Atrue%2C%22splitDirection%22%3A-1%2C%22knownContainerUniqueIds%22%3A%5B%22%2Fhttps%3A%2F%2Fworldcover2020.esa.int%2Fgeoserver%2Fgwc%2Fservice%2Fwms%3FSERVICE%3DWMS%26VERSION%3D1.1.1%22%5D%2C%22type%22%3A%22wms%22%7D%2C%22%2F%22%3A%7B%22type%22%3A%22group%22%7D%7D%2C%22workbench%22%3A%5B%22%2Fhttps%3A%2F%2Fworldcover2020.esa.int%2Fgeoserver%2Fgwc%2Fservice%2Fwms%3FSERVICE%3DWMS%26VERSION%3D1.1.1%2Fesa_worldcover2021%3Aesa_worldcover2021%22%2C%22%2Fhttps%3A%2F%2Fmaps.lsc-hubs.org%2Fows%2Frwanda%2FPHH2O%3Fservice%3DWMS%26version%3D1.3.0%26request%3DGetCapabilities%2FPHH2O_mean_0-20cm%22%5D%2C%22timeline%22%3A%5B%22%2Fhttps%3A%2F%2Fworldcover2020.esa.int%2Fgeoserver%2Fgwc%2Fservice%2Fwms%3FSERVICE%3DWMS%26VERSION%3D1.1.1%2Fesa_worldcover2021%3Aesa_worldcover2021%22%2C%22%2Fhttps%3A%2F%2Fmaps.lsc-hubs.org%2Fows%2Frwanda%2FPHH2O%3Fservice%3DWMS%26version%3D1.3.0%26request%3DGetCapabilities%2FPHH2O_mean_0-20cm%22%5D%2C%22initialCamera%22%3A%7B%22west%22%3A29.113769531250004%2C%22south%22%3A-2.3888339781603642%2C%22east%22%3A30.731506347656254%2C%22north%22%3A-1.3594305392513697%7D%2C%22homeCamera%22%3A%7B%22west%22%3A-180%2C%22south%22%3A-90%2C%22east%22%3A180%2C%22north%22%3A90%7D%2C%22viewerMode%22%3A%222d%22%2C%22showSplitter%22%3Atrue%2C%22splitPosition%22%3A0.5%2C%22settings%22%3A%7B%22baseMaximumScreenSpaceError%22%3A2%2C%22useNativeResolution%22%3Afalse%2C%22alwaysShowTimeline%22%3Afalse%2C%22baseMapId%22%3A%22basemap-positron%22%2C%22terrainSplitDirection%22%3A0%2C%22depthTestAgainstTerrainEnabled%22%3Afalse%7D%2C%22stories%22%3A%5B%5D%7D%5D%7D)