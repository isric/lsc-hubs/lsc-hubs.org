---
title: 'List of AKIS initiatives in Rwanda'
date: 2023-08-30
type: rwanda
---

Initial list of agricultural knowledge and innovation systems has been compiled as part of "D2.3 LSC-IS Needs Assessment and LSC-hub Design Requirements (Work Package 2): Workshops and Key Informant Interviews regional report".

{{< bs-table "table table-striped table-bordered" >}}
| Initiative | Objective | Implementing organization | Targeted stakeholders | Link |
| --- | --- | --- | --- | --- |
| Smart Nkunganire system | Input subsidy management,m onitor the distribution of subsidised inputs | BK tech house | Farmers; Farmer organization; agro-dealers; fertiliser companies; local government; BK Tech House; RAB; OAF; MINALOC | [SNS](https://www.minagri.gov.rw/updates/news-details/smart-nkunganire-system-to-enhance-access-to-agriculture-inputs) |
|  Smart Kungahara System | Digitise cash crop value chain (coffee, tea, horticultural crops | NAEB | Farmers; Farmer organization; agro-dealers; fertiliser companies; local government | [SKS](https://smartkungahara.rw/#/) |
| Land ownership through the land administration information system (LAIS) |   |   |   | [Land administration](https://landportal.org/node/38457) |
| CROM-DSS (catchment-based land restoration opportunity mapping decision support system) | Tools to help field operators to identify priority sites for soil erosion and landslide mitigationRestoration measures | Rwanda water board | RAB; Local government | [CROM DSS](https://docplayer.net/203761268-Catchment-restoration-in-rwanda-crom-dss-a-gis-model-to-support-decision-making-on-catchment-restoration-africagis-2019.html) |
| YEAN platform | Providing technical information on crop and livestock | YEAN | Value chain actors; RAB; MINAGRI | [YEAN](https://yeanagro.org/) |
| RwaSIS | To provide information on soil erosion hot spots, To develop a tool where the information can be channelled, To develop lime and fertilizer site-specific recommendations | RAB | Farmers; National land authority; Financial institutions; RISA; Water use associations; Investors; Exporters; Producers; Processors |   |
| E-soko | To provide market information for different commodities | MINAGRI and MINICOM | Farmers; Financial institutions; Water use associations; Investors, Exporters; Producers; Processors | [e-Soko](https://esoko.com/) |
| Maproom | To collect maps and data for climate and environmental monitoring | Meteo Rwanda | Decision makers; Agricultural extensionists; Health institutions; Disaster management entities; Academics | [Meteo Rwanda Map Room](http://maproom.meteorwanda.gov.rw/maproom/index.html) |
| Carbon footprint | Carbon credit calculation to reduce CO
# 2
emissions | REMA | RICA; REMA; RSB; Manufacturers; NGOs |   |
| National agriculture uses insurance | Decrease the risk in agriculture, To develop farmers to access loans. | MINAGRI, BK Company Insurance |   |   |
| MOPA System |   |   | SAIP |   |
| Tekana Muhinzi Mworozi |   |   |   |   |
{{< /bs-table >}}