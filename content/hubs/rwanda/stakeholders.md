---
title: 'Rwanda stakeholders'
date: 2023-03-22
type: rwanda
---

In Rwanda, Land Soil Crop- Information Services’s (LSC Hubs) lead Rwanda project partner RAB co-hosted a national and regional stakeholders’ gatherings on the topic of how information hubs for climate-smart land, soil and crop data can contribute to rural transformation. The event attracted more than 60 governmental, private business, and development partners with a vested interest in creating, sharing and disseminating agricultural knowledge and innovation. See the list of stakeholders below. 

*This list is in progress and new stakeholder are still being added. If you are engaged with the project and would like to be added, please email Emily Toner at emily.toner@isric.org.* 

Stakeholders are categorised by:

- [Development Partners](#development-partners)
- [Farmer Organisations](#farmer-organisations)
- [Knowledge Institutions](#knowledge-institutions)
- [Private sector](#private-sector)
- [Public sector](#public-sector)

&nbsp;

## Development Partners

{{% acc  id="dev" %}} 



{{% acc-item name="Environnement sans FrontièreFrontire" url="" %}}{{% /acc-item %}}
{{% acc-item name="Hydraulic Program of Ruhengeri Diocese" url="" %}}{{% /acc-item %}}
{{% acc-item name="Projet de Gestion IntégréeIntegree des eaux de pluie-Gire/IWRM zone des laves" url="" %}}{{% /acc-item %}}
{{% acc-item name="AGRI RESEARCHAGRIRESEARCH" url="https://agriresearchunguka.rw/" %}}Vision: To be youth leading innovation hub in Rwanda that empowers farmers to be healthy and wealthy
Mission: Promoting research culture and green extension{{% /acc-item %}}
{{% acc-item name="Eucord" url="https://eucord.org/projects/create-rwanda/" %}}Goal: To reduce poverty in Rwanda through increasing agricultural capacity in rural households and limiting the dependency on imported commodities. The project aims to rebuild agricultural production and increase food security by linking smallholder farmers to urban markets, restoring essential social services, and rebuilding local community cohesion and infrastructure.{{% /acc-item %}}
{{% acc-item name="One Acre Fund" url="https://oneacrefund.org/" %}}Aim: Tree program is to provide not only short-term income opportunities but also long-term benefits, such as soil health improvements and erosion control, so that farmers build resilience over time. Working also on: 1) Building a unique model for quality community-based crop seed production and multiplication. 2) Equipping private entrepreneurs and government nurseries to grow coffee seedlings for sale to farmers. They expect this to benefit women in particular. 
3) Testing new fruit tree offerings (such as avocado, moringa, coffee, neem, papaya, mango) in our network of agroforestry research sites. 4) Engaging with a malt factory and two cooperatives to kick start quality malt barley production in Amhara. 5) Exploring carbon sales as a way to cover program costs, add value to farmers’ tree planting and support local government initiatives{{% /acc-item %}}
{{% acc-item name="Rural Development Initiative" url="https://ruraldevelopmentinitiative.org/" %}}Mission: Empowering youth with opportunities to become active citizens and creating sustainable incomes and livelihood opportunities for the rural poor households.
Vision: To Lead the way in creating self-reliant and empowered  communities.{{% /acc-item %}}
{{% acc-item name="RWARRI" url="https://rwarri.com/" %}}The mission of RWARRI is to be one of the most valued and respected NGOs in Rwanda and beyond, working towards achieving sustainable livelihoods among the rural poor, contributing towards shaping and implementation of national and global policies that lead to positive economic, social and technological transformation of the citizens. The vision of RWARRI is the ultimate realization of a rural and vulnerable community that is economically sustainable, socially transformed, and food security. Fundamental to our vision is the belief that the above vision will be realized when community members themselves take ownership and responsibility for the entire development process.{{% /acc-item %}}
{{% acc-item name="AGRITERRA" url="https://www.agriterra.org/rwanda-eng/" %}}How: As an international specialist in cooperative development, we work by using a three-track approach. We make cooperatives bankable and create real farmer-led businesses. We improve extension services to members and enhance farmer-government dialogues. We use Agripool, our knowledge broker agency which is a unique pool of hundreds of agricultural experts from the Netherlands and other countries. Agriterra builds on the know-how and experience of experts in agribusiness. Agripool experts speak “the language of agribusiness” and work from farmer to farmer.{{% /acc-item %}}
{{% acc-item name="Agriterra" url="https://www.agriterra.org/rwanda-eng/" %}}How: As an international specialist in cooperative development, we work by using a three-track approach. We make cooperatives bankable and create real farmer-led businesses. We improve extension services to members and enhance farmer-government dialogues. We use Agripool, our knowledge broker agency which is a unique pool of hundreds of agricultural experts from the Netherlands and other countries. Agriterra builds on the know-how and experience of experts in agribusiness. Agripool experts speak “the language of agribusiness” and work from farmer to farmer.{{% /acc-item %}}
{{% acc-item name="ASARECA" url="https://www.asareca.org/" %}}ASARECA has strategically repositioned to perform a higher level convening, coordination, facilitative, advocacy, partnership brokerage, communicative and catalytic role to maximize economies of scale, reduce duplication and misalignment of resources in the sub-region’s Agriculture Research for Development (AR4D). This is aimed at improving effectiveness and efficiency, leading to significant improvement in the delivery of inclusive and sustainable agricultural transformation and development.{{% /acc-item %}}
{{% acc-item name="CGIAR" url="https://www.cgiar.org/" %}}{{% /acc-item %}}
{{% acc-item name="ICRAF/World Agroforestry" url="https://www.cifor-icraf.org/" %}}CIFOR–ICRAF addresses local challenges and opportunities while providing solutions to global problems for forests, landscapes, people and the planet. We deliver actionable evidence and solutions to transform how land is used and how food is produced: conserving and restoring ecosystems, responding to the global climate, malnutrition, biodiversity and desertification crises. In short, improving people’s lives, while preserving the environment. In this context, they work closely with farmers, forest communities and a wide range of partners. In addition, they also drive the international dialogue on sustainable land use by convening large-scale events and establishing communities of practice through the Global Landscapes Forum{{% /acc-item %}}
{{% acc-item name="CLINTON FOUNDATION (CDI)" url="https://www.clintonfoundation.org/programs/economic-inclusion-development/clinton-development-initiative/" %}}{{% /acc-item %}}
{{% acc-item name="CNFA" url="https://www.cnfa.org/program/feed-the-future-rwanda-hinga-weze-activity/" %}}Mission: To stimulate economic growth and improve livelihoods by cultivating entrepreneurship.
Vision: A prosperous world without hunger.{{% /acc-item %}}
{{% acc-item name="RDDP" url="https://www.ifad.org/en/web/operations/-/project/2000001195" %}}The project aims to raise rural incomes by intensifying dairy production and improving market access.{{% /acc-item %}}
{{% acc-item name="ILRI" url="https://www.ilri.org/ " %}}Mission: to improve food and nutritional security and to reduce poverty in developing countries through research for efficient, safe and sustainable use of livestock—ensuring better lives through livestock. Strategic objectives, with partners 1) to develop, test, adapt and promote science-based practices that—being sustainable and scalable—achieve better lives through livestock. 2) to provide compelling scientific evidence in ways that persuade decision makers—from farms to boardrooms and parliaments—that smarter policies and bigger livestock investments can deliver significant socio-economic, health and environmental dividends to both poor nations and households. 3) to increase capacity among ILRI’s key stakeholders to make better use of livestock science and investments for better lives through livestock.{{% /acc-item %}}
{{% acc-item name="IUCN" url="https://www.iucn.org/" %}}Vision: Influence, encourage and assist societies to conserve the integrity and diversity of nature and ensure that any use of natural resources is equitable and ecologically sustainable. How: Broad membership also means they can incubate ideas and are a trusted repository of best practices, tools and international standards. They provide a neutral space in which governments, NGOs, scientists, businesses, local communities, indigenous peoples’ organisations and others can work together to solve environmental challenges and achieve sustainable development.
Working with many partners and supporters, IUCN implements a large and diverse portfolio of conservation projects worldwide. These projects combine the latest science with traditional knowledge of local communities to work to reverse habitat loss, restore ecosystems and improve people’s well-being.{{% /acc-item %}}
{{% acc-item name="WAMCAB" url="https://www.jica.go.jp/project/english/rwanda/008/news/general/190731.html" %}}The Project purpose is "the capacity of irrigation scheme management improves in the model sites{{% /acc-item %}}
{{% acc-item name="MERRY YEAR" url="https://www.jobinrwanda.com/employer/merry-year-international" %}}Merry Year International is an international self-reliance development NGO that provides constant opportunities to the neglected neighbors around the world to become independent, Merry Year International seeks ways for people in need to stand on their own feet and create sustainable economic communities.{{% /acc-item %}}
{{% acc-item name="Land O Lakes" url="https://www.landolakesventure37.org/" %}}Our Mission: Striving to help global communities thrive through agriculture. We are focused on long-term, human-centered impact.{{% /acc-item %}}
{{% acc-item name="Nile Basin Initiative" url="https://www.nilebasin.org/index.php/rwanda" %}}Goals:
1. Enhance availability and sustainable utilization and management of transboundary water resources of the Nile Basin
2. Enhance hydropower development in the basin and increase interconnectivity of electric grids and power trade.
3. Enhance efficient agricultural water use and  promote a basin approach to address the linkages between water and food security
4. Protect, restore and promote sustainable use of water related ecosystems across the basin
5. Improve basin resilience to climate change impacts
6. Strengthen transboundary water governance in the Nile Basin{{% /acc-item %}}
{{% acc-item name="Partners for Conservation" url="https://www.notrebio.org/" %}}Our overall mission is to save a biodiversity legacy through the promotion, protection and conservation. Partners For Conservation serves as a platform that provides opportunities to everyone from farmers to businessmen; from children to adults; from illiterates to academics; from low-income people to donors; from volunteers to professionals, to appreciate and celebrate their contributions towards the conservation of the biodiversity.{{% /acc-item %}}
{{% acc-item name="World Vision" url="https://www.wvi.org/rwanda" %}}Vision: Our vision for every child, life in all its fullness; Our prayer for every heart, the will to make it so. Mission: World Vision is an international partnership of Christians, whose mission is to follow our Lord and Saviour Jesus Christ, in working with the poor and oppressed to promote human transformation, seek justice and bear witness to the good news of the Kingdom of God.{{% /acc-item %}}
{{% acc-item name="COCURIB COOPERATIVE" url="?" %}}{{% /acc-item %}}
{{% acc-item name="Coop. Enviro Bugesera" url="?" %}}{{% /acc-item %}}
{{% acc-item name="GWIZARW.34" url="?" %}}{{% /acc-item %}}



{{% /acc %}}

&nbsp;


## Farmer Organisations

{{% acc id="farm" %}} 


{{% acc-item name="EJO HEZA COOPERATIVE" url="https://ejoheza.gov.rw/ltss-registration-ui/landing.xhtml" %}}The objective of this scheme is to help all Rwandans and foreigners living in Rwanda have a pension saving scheme and retire with dignity. It will give an additional opportunity to even formal employed Rwandans to complement their existing RSSB pension scheme.{{% /acc-item %}}
{{% acc-item name="HORECO" url="https://horeco.rw/" %}}Mission: The largest transformative Company in Rwanda's agriculture which applies modern technology with the main focus on horticulture, irrigation, and export. Vision: to assist local farmers to transform subsistence agriculature into a mdoern agri-business enterprise by applying extensions that integrate sustainable crop production and modern irrigation technology. Objectives: The overall objective of HoReCo is to improve Rwanda's food security by applying modern framing and innovations in research and technology to agriculture, resulting in real and tangible long-term income.{{% /acc-item %}}
{{% acc-item name="Imbaraga Urugaga" url="https://imbaraga.org/" %}}{{% /acc-item %}}
{{% acc-item name="URUGAGA IMBARAGA" url="https://imbaraga.org/ " %}}{{% /acc-item %}}
{{% acc-item name="RYAF" url="https://ryaf.rw/home" %}}Our Mission: To empower, promote, mobilize, advocate and inform young individuals and youth organizations to fully contribute to agriculture transformation through improved agribusiness, job creation, stable food market and inclusive food system. Our Vision: To be a leading agribusiness platform empowering young people to contribute in job creation and food security for the country prosperity.{{% /acc-item %}}
{{% acc-item name="AEE/Ev Hort.proj" url="https://snv.org/project/hortinvest-rwanda" %}}Our mission: Together, we want to inspire action locally, nationally, and globally, elevating the voices of the people and communities with whom we work. We want to see a world where across every society, all people live with dignity and have equitable opportunities to thrive sustainably.

That’s why our mission is to strengthen capacities and catalyse partnerships that transform the agri-food, energy, and water systems, which enable sustainable and more equitable lives for all.{{% /acc-item %}}
{{% acc-item name="Burera District" url="https://www.burera.gov.rw/ahabanza" %}}{{% /acc-item %}}


{{% /acc %}}

&nbsp;


## Knowledge Institutions

{{% acc  id="ki" %}} 



{{% acc-item name="AGRIFOP" url="https://agriprofocus.com/organisation/agribusiness-focused-partnership-organisation-agrifop" %}}Complex challenges, like transforming food systems, ask for innovative approaches as traditional solutions often do not solve the underlying systemic aspects of a problem. By taking a food systems approach, Netherlands Food Partnership’s contributions will help Dutch initiatives to increase synergies and reduce negative trade-offs between social, economic and ecological food system outcomes. We focus on four relevant food system challenges: Equal access and distribution of food, Healthy diets, Resilient ecosystems and Peace, justice and stability.{{% /acc-item %}}

{{% acc-item name="UR-CAVM" url="https://cavm.ur.ac.rw/" %}}Complex challenges, like transforming food systems, ask for innovative approaches as traditional solutions often do not solve the underlying systemic aspects of a problem. By taking a food systems approach, Netherlands Food Partnership’s contributions will help Dutch initiatives to increase synergies and reduce negative trade-offs between social, economic and ecological food system outcomes. We focus on four relevant food system challenges: Equal access and distribution of food, Healthy diets, Resilient ecosystems and Peace, justice and stability.{{% /acc-item %}}
{{% acc-item name="Muhabura Integrated Polytechnic   College." url="https://mipc.ac.rw/" %}}Vision: To be the Premier Source for education, workforce training, partnerships and economic development.
Mission: To provide innovative educational environments, opportunities, and experiences that enable individuals, communities, and the region to grow, thrive, and prosper.{{% /acc-item %}}
{{% acc-item name="UNILAK" url="https://site.unilak.ac.rw/" %}}The vision of UNILAK is to become a leading University in Africa, grounded in Christian values, we strive for excellence in education, research, and community service.
The mission of UNILAK is to promote integral education, scientific and technological research, community service and any other thing that may directly or indirectly contribute to the real development of Rwanda.{{% /acc-item %}}
{{% acc-item name="INES University" url="https://www.ines.ac.rw/" %}}The mission is to contribute through the interactive conjunction between civil society, private sector and public sector to the national and regional development, by providing specialized university education enhanced by research, in order to create competitive enterprises and well paid employment.
And the vision is universality in each individual; Knowledge in order to unite and better serve the world.{{% /acc-item %}}
{{% acc-item name="IPRC" url="https://www.iprckigali.rp.ac.rw/" %}}Vision: RP IPRC Kigali aspires to be a leading world class institution in the provision  of  producing graduates capable of developing and implementing creative technical solutions to social and industrial needs of Rwanda, the region and International society.
Mission: To provide Technical and vocational training at all levels in order to empower students and enhance their opportunities for career advancement and success in a global economy.{{% /acc-item %}}
{{% acc-item name="IPRC Musanze" url="https://www.iprcmusanze.rp.ac.rw/" %}}Our Vision is to provide quality education that complies with applicable standards through vocational education that enables beneficiary to acquire skills required to create jobs and compete in the labour market.{{% /acc-item %}}
{{% acc-item name="ISRIC " url="https://www.isric.org/" %}}Mission: serve the international community as a custodian of global soil information. We support soil data, information and knowledge provisioning at global, national and sub-national levels for application into sustainable management of soil and land.{{% /acc-item %}}
{{% acc-item name="WUR" url="https://www.wur.nl/" %}}Mission: "To explore the potential of nature to improve the quality of life". The strength of Wageningen University & Research lies in its ability to join the forces of specialised research institutes and the university. It also lies in the combined efforts of the various fields of natural and social sciences. This union of expertise leads to scientific breakthroughs that can quickly be put into practice and be incorporated into education. Domains: Food, feed & biobased production, Natural resources & living environment, Society & well-being.{{% /acc-item %}}



{{% /acc %}}

&nbsp;


## Private sector

{{% acc id="private" %}} 



{{% acc-item name="MUDATSIKIRA Valens Ltd" url="?" %}}{{% /acc-item %}}
{{% acc-item name="SAVANA MUSANZE LTD" url="?" %}}{{% /acc-item %}}
{{% acc-item name="Yara Ltd" url="https://agriprofocus.com/organisation/yara-rwanda-ltd" %}}Complex challenges, like transforming food systems, ask for innovative approaches as traditional solutions often do not solve the underlying systemic aspects of a problem. By taking a food systems approach, Netherlands Food Partnership’s contributions will help Dutch initiatives to increase synergies and reduce negative trade-offs between social, economic and ecological food system outcomes. We focus on four relevant food system challenges: Equal access and distribution of food, Healthy diets, Resilient ecosystems and Peace, justice and stability.{{% /acc-item %}}
{{% acc-item name="Alicomec" url="https://alicomec.rw/" %}}ALICOMEC Is leader in the production of crushed limestone, quicklime and hydrated lime in Rwanda. Our factory supplies reliable, high-quality limestone products.

We produces a wide range of lime based products, high quality Limestone Powder that is widely used for commercial as well as industrial, agricultural , pastoral, and construction applications. Our Fine Limestone Powder is prepared from the natural limestone as per the strict quality norms. Natural Limestone Powder produced by us is in huge demand in the Rwandan market for purity.{{% /acc-item %}}
{{% acc-item name="BK TechHouse" url="https://bktechouse.rw/" %}}Vision: Become the regional number one customer choice for Innovative Technology Solutions that delight and empower our customers and communities to reach their highest potential.
Mission: Passionately and professionally providing the most delightful, empowering, innovative , customer-centric Digital solutions.{{% /acc-item %}}
{{% acc-item name="FORWARD INVESTMENT GROUP" url="https://rig.co.rw/" %}}Vision: We intend to direct our investments into high impact areas with the aim of making profit while accelerating social-economic development as well as generating attractive returns for our shareholders; and stimulating private sector confidence to invest in Rwanda.
Mission: We envisage becoming the leading investment company in Rwanda while achieving the highest ethical and performance standards.{{% /acc-item %}}
{{% acc-item name="Hollanda Fairfood/ big" url="https://winnazworld.com/" %}}A key pillar in our doing business is to improve the yield and quality of Rwandan smallholder farmers. Our dedicated team of agronomists visits them during the crop cycle and regularly give the farmers advice, follow-ups, and training. Farmers who work with us get access to a premium market and weekly advice on improved ways of growing potatoes to have high-quality yields.{{% /acc-item %}}
{{% acc-item name="APTC" url="https://www.aptc.rw/" %}}Vision: To become a world class supplier of top quality agro-processed products that leads the growth of agro-processing industry in Rwanda by procuring, processing, and marketing varieties of specialty food technologies and employing safe globally certified environmental practices/codes.
Mission: To develop and sustain a strong Agro Processing Company using modern technologies related to agricultural mechanization, modern livestock farming and agro processing as well as develop a strong research and development unit that promotes other industrial byproducts.{{% /acc-item %}}
{{% acc-item name="RFC/OCP" url="https://www.ocpafrica.com/en/rwanda" %}}Vision: Africa can become a world leader in sustainable farming, using local resources to realize the continent’s vast agricultural potential and help feed its growing population.
We are committed to working hand-in-hand with the people who will make this a reality - African smallholder farmers – enabling them to move from subsistence to a more modern way of farming.{{% /acc-item %}}

{{% /acc %}}

&nbsp;


## Public sector

{{% acc id="public" %}} 


{{% acc-item name="Gatsibo" url="?" %}}{{% /acc-item %}}
{{% acc-item name="SAIP" url="https://lwh-rssp.minagri.gov.rw/index.php?id=4" %}}The project objective is to increase agricultural productivity, market access, and food security of the targeted beneficiaries in the project intervention areas.{{% /acc-item %}}
{{% acc-item name="Trii Seed Co Ltd" url="https://triseeds.rw/" %}}Mission:
- to implement national policies, laws, strategies, regulations and Government resolutions related to the management and use of land;
- to provide advice to the Government, monitor and coordinate the implementation of strategies related to the management and use of land;
- to promote activities relating to investment and value addition in the activities related to the use and exploitation of land resources in Rwanda;
- to register land, issue and keep land authentic deeds and any other information relating to land of Rwanda;
- to supervise all land-related matters and represent the State for supervision and monitoring of land management and use;
- to execute or cause to be executed geodetic, topographic, hydrographic and cadastral surveys in relation to land resources;
- to initiate research and study on land, publish the results of the research and disseminate them;
- to prepare, disseminate and publish various maps and master plans relating to land management using the most appropriate scales;
- to establish and update basic topographic maps and thematic maps; 10° to define standards for:
          a. land administration;
          b. land surveys;
          c. the geo-information, spatial information and land information data collection;
          d. cartographic representations of geographic features and national spatial data infrastructure;
- to set up principles and guidelines related to use of land;
- to organise, coordinate and monitor collection use and dissemination of geo information in the country under the National Spatial data Infrastructure Framework;
- to issue technical instructions related to land management and use to district land bureaux and follow up their implementation;
- to receive and evaluate proposals to purchase or lease private state-owned land and to issue, on behalf of Government, long term leases and permits to occupy such lands in accordance with the Law governing land in Rwanda;
- to monitor and to enforce the execution of terms of land lease contracts and to advise on their amendment;
- to undertake or cause to be undertaken all State land valuation for the purposes of its classification for sale, lease, taxation and cession;
- to carry out an inventory of all land resources in the country, their quality and their use, and act as the keeper and custodian of all national maps, aerial photomaps collections and their database;
- to resolve conflicts relating to land use and management which were not resolved at the District or City of Kigali levels;
- to establish cooperation and collaboration with other regional and international institutions with an aim of harmonising the performance and relations on matters relating to management of land;{{% /acc-item %}}
{{% acc-item name="Bugesera " url="https://www.bugesera.gov.rw/" %}}{{% /acc-item %}}
{{% acc-item name="Kayonza" url="https://www.kayonza.gov.rw/" %}}{{% /acc-item %}}
{{% acc-item name="Kirehe" url="https://www.kirehe.gov.rw/" %}}{{% /acc-item %}}
{{% acc-item name="National Land Authority" url="https://www.lands.rw/" %}}Mission:
- to implement national policies, laws, strategies, regulations and Government resolutions related to the management and use of land;
- to provide advice to the Government, monitor and coordinate the implementation of strategies related to the management and use of land;
- to promote activities relating to investment and value addition in the activities related to the use and exploitation of land resources in Rwanda;
- to register land, issue and keep land authentic deeds and any other information relating to land of Rwanda;
- to supervise all land-related matters and represent the State for supervision and monitoring of land management and use;
- to execute or cause to be executed geodetic, topographic, hydrographic and cadastral surveys in relation to land resources;
- to initiate research and study on land, publish the results of the research and disseminate them;
- to prepare, disseminate and publish various maps and master plans relating to land management using the most appropriate scales;
- to establish and update basic topographic maps and thematic maps; 10° to define standards for:
          a. land administration;
          b. land surveys;
          c. the geo-information, spatial information and land information data collection;
          d. cartographic representations of geographic features and national spatial data infrastructure;
- to set up principles and guidelines related to use of land;
- to organise, coordinate and monitor collection use and dissemination of geo information in the country under the National Spatial data Infrastructure Framework;
- to issue technical instructions related to land management and use to district land bureaux and follow up their implementation;
- to receive and evaluate proposals to purchase or lease private state-owned land and to issue, on behalf of Government, long term leases and permits to occupy such lands in accordance with the Law governing land in Rwanda;
- to monitor and to enforce the execution of terms of land lease contracts and to advise on their amendment;
- to undertake or cause to be undertaken all State land valuation for the purposes of its classification for sale, lease, taxation and cession;
- to carry out an inventory of all land resources in the country, their quality and their use, and act as the keeper and custodian of all national maps, aerial photomaps collections and their database;
- to resolve conflicts relating to land use and management which were not resolved at the District or City of Kigali levels;
- to establish cooperation and collaboration with other regional and international institutions with an aim of harmonising the performance and relations on matters relating to management of land;{{% /acc-item %}}
{{% acc-item name="Meteo" url="https://www.meteorwanda.gov.rw/index.php?id=2" %}}Our vision: To be a Meteorological Service that is highly efficient and effective, customer and employee focused.{{% /acc-item %}}
{{% acc-item name="MINAGRI" url="https://www.minagri.gov.rw/" %}}The Ministry of Agriculture and Animal Resources (MINAGRI) has the mission of promoting the sustainable development of a modern, efficient and competitive agriculture and livestock sector, in order to ensure food security, agriculture export and diversification of the productions for the benefit of the farmer and the economy of the Country.
The vision of the Ministry, as defined by the National Agricultural Policy (2018), is for Rwanda to become “a nation that enjoys food security, nutritional health and sustainable agricultural growth from a productive, green and market-led agricultural sector.”{{% /acc-item %}}
{{% acc-item name="NAEB" url="https://www.naeb.gov.rw/index.php?id=1" %}}Mission:
- To advise on the development of policy and strategies for  developing exports of agricultural and livestock products meeting international market requirements;
- To implement policy and strategies for developing exports of agricultural and livestock products meeting international market requirements;
- To work with stakeholders’ networks and coordinate their activities in relation to the processing and export of agricultural and livestock products;
- To provide timely and cost-effective support services required for enhanced international competitiveness of the private sector in agricultural and livestock exports;
- To identify and diversify agricultural and livestock exports to sustain growth of foreign currency revenues;
- To identify and support research activities on agricultural and livestock extension regarding exports of agricultural and livestock products;
- To identify places for installation of factories and other activities meant for processing agricultural and livestock products for export;
- To participate in the setting and checking of quality standards for agriculture and livestock export commodities in collaboration with other relevant institutions;
- To issue certificates of authenticity and origin of agricultural and livestock export commodities;
- To put in place strategies designed to provide support and train private operators and cooperatives involved in export of agricultural and livestock products;
- To contribute to investments aimed at increasing production, industries and infrastructure for adding value to agricultural and livestock products for export;
- To collect and analyse information on national, regional and international markets and disseminate that information to the concerned stakeholders for use;
- To participate in international negotiations and forums in order to ensure the protection and extension of agricultural and livestock products export market;
- To participate in various national, regional and international trade fairs, in order to promote agricultural and livestock products for export;
- To facilitate negotiations for setting and publishing minimum farm gate prices for agricultural and livestock export commodities in collaboration with stakeholders;
- To establish relations and cooperation with regional and international organisations with the aim of improving operations and collaboration with   regard to exports of agricultural and livestock products.{{% /acc-item %}}
{{% acc-item name="Nyabihu District" url="https://www.nyabihu.gov.rw/" %}}{{% /acc-item %}}
{{% acc-item name="Nyagatare" url="https://www.nyagatare.gov.rw/" %}}{{% /acc-item %}}
{{% acc-item name="RAB" url="https://www.rab.gov.rw" %}}Vision: Improved food security and livelihoods of all Rwandans by transforming agriculture from subsistence into modern farming through generating research and extension innovations that generate sustainable crop, animal husbandry and natural resource management.
Mission: RAB has the general mission of developing agriculture and animal resources through research, agricultural and animal resources extension in order to increase agricultural and animal productivity as well as their derived.{{% /acc-item %}}
{{% acc-item name="RISA" url="https://www.risa.rw/home/" %}}RISA has the mission of digitizing the Rwandan society through increased usage of Information and Communication Technologies and innovation technology as a cross-cutting enabler for the development of other sectors spearheading Rwanda’s digital and social economic transformation.{{% /acc-item %}}
{{% acc-item name="Rwamagana" url="https://www.rwamagana.gov.rw/" %}}{{% /acc-item %}}
{{% acc-item name="RWB" url="https://www.rwb.rw/" %}}Mission:
- To implement national policies, laws and strategies related to water resources;   
- To advise the Government on matters related to water resources;   
- To establish strategies aimed at knowledge based on research on water resources knowledge, forecasting on water availability, quality and demand;  
- To establish strategies related to the protection of catchments and coordinate the implementation of erosion control plans;   
- To establish floods management strategies;  
- To establish water storage infrastructure;
- To establish water resources allocation plans;  
- To establish water resources quality and quantity preservation strategies;   
- To control and enforce water resources use efficiency;  
- To examine the preparation of roads, bridges, dams and settlements designs in order to ensure flood mitigation and water storage standards;  
- To monitor the implementation of flood mitigation measures and water storage during the implementation of roads, bridges and settlements’ plans;   
- To cooperate and collaborate with other regional and international institutions with a similar mission.{{% /acc-item %}}
{{% acc-item name="NISR" url="https://www.statistics.gov.rw/" %}}Mission: To assume the leading role in improving capacity to use information for evidence based decision-making by coordinating national effort to collect and archive reliable data, to analyze, document and disseminate data within an integrated and sustainable framework.
Vision: To develop and sustain a culture of excellence in statistical production and management of national development{{% /acc-item %}}
{{% acc-item name="Tear fund/AEE" url="https://www.tearfund.org/campaigns/world-of-difference/rwanda" %}}Mission: Our mission is to follow Jesus where the need is greatest, responding to crises and partnering with local churches and organisations to help people lift themselves out of poverty.
Vision: Our vision is to see people freed from poverty, living transformed lives, and fulfilling their God-given potential.{{% /acc-item %}}


{{% /acc %}}

&nbsp;