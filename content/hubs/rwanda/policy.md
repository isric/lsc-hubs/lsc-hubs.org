---
title: 'List of relevant policies in Rwanda'
date: 2023-08-30
type: rwanda
---

The initial list of relevant policies has been compiled as part of "D2.3 LSC-IS Needs Assessment and LSC-hub Design Requirements (Work Package 2): Workshops and Key Informant Interviews regional report".

{{< bs-table "table table-striped table-bordered" >}}
| Policy/Framework | Objectives | Implementing organization | Targeted stakeholders | Link |
| --- | --- | --- | --- | --- |
| National Agriculture policy | Increased contribution to wealth creation; Economic opportunities & prosperity; Improved food security and nutrition; Increased resilience | RAB | NAEB | [NAP](https://www.minagri.gov.rw/fileadmin/user_upload/Minagri/Publications/Policies_and_strategies/National_Agriculture_Policy_-_2018___Approved_by_Cabinet.pdf) |
| Land consolidation policy | Land ownership and tenure security; Equitable allocation; Increase productivity per unit area- Effective use of inputs | MINAGRI, Ministry of Environment, Local government, RAB, NAEB |   | [Land Use Consolidation Act (LUC)](https://www.landportal.org/library/resources/rwanda-land-research-136/final-report-land-use-consolidation-and-crop) |
| National data revolution policy | Establish stand and principles for data management; Establish a framework to develop human resources; Establish a framework for data creation, realization; Derive development; Establish a data framework |   |   | [Data Revolution](https://statistics.gov.rw/file/5410/download?token=r0nXaTAv) |
| Biodiversity conservation policy |   |   |   | [Rwanda Biodiversity Policy](https://rema.gov.rw/rema_doc/pab/RWANDA%20BIODIVERSITY%20POLICY.pdf) |
| Fertilizer use guidelines | Increase productivity; Effectiveness and national fertiliser use |  MINAGRI, RAB |   | [National Fertilizer Policy](https://faolex.fao.org/docs/pdf/rwa174364.pdf) |
| Agricultural extension policy |   |   |   | [National Agricultural Extension Strategy](https://faolex.fao.org/docs/pdf/rwa149678.pdf) |
| Irrigation guidelines |   |   |   | [Irrigation Master Plan](https://www.minagri.gov.rw/fileadmin/user_upload/Minagri/Publications/Policies_and_strategies/Rwanda_Irrigation_Master_Plan.pdf) |
| National Environmental and Climate Change Policy | To protect national resources from environmental hazards and weather; Resilience to climate change |  RAB, MoE, FONERWA, REMA  |   | [NECCP](https://plasticsdb.surrey.ac.uk/documents/Rwanda/Ministry%20of%20Enviornment%20(2019)%20Rwanda%20National%20Environment%20and%20Climate%20Change%20Policy,%20Rwanda.pdf) |
| PST4 |  Sustainable productivity- Market-oriented agriculture; Improved technical innovations in Agriculture; Smart agriculture | MINAGRI; RAB | Farmers; private sector;  |   |
| National Forestry Policy | Conservation, protection, management and utilisation of national forest resources |   |   | [Rwanda National Forestry Policy](https://www.environment.gov.rw/fileadmin/user_upload/Moe/Publications/Policies/Rwanda_National_Forestry_Policy_2018__1_.pdf) |
| Rwanda water policy | Effective use of water | Rwanda Water Board, RAB, MoE | All farmers, Private sector |   |
| Mining policy | Sustainable use of available mining | REMA, MoE, Rwanda mining petroleum and Gas | All farmers, private sector |   |
| Crop intensification program | Increase productivity per unit area- Effective use of inputs | RAB, MINAGRI | Farmers, agro-dealers | [CIP](http://197.243.22.137/gakenke/fileadmin/templates/DOCUMENT_Z_ABAKOZI/abakozi/MORE_INFORMATION_ABOUT_CROP_INTENSIFICATION_PROGRAM.pdf) |
| Quality and food safety policy |   |   |   |   |
| Seed multiplication policy |   |   |   |   |
| Paddy Rice Policy |   |   |   | [National Rice Development Strategy](https://riceforafrica.net/wp-content/uploads/2021/09/rwanda_en.pdf) [Rice Strategy 2021-2030](https://riceforafrica.net/wp-content/uploads/2022/02/nigeria_nrds2.pdf) |
| Inspection policy |   |   |   | [Rwanda Quality Policy](https://rwandatrade.rw/media/2010%20MINICOM%20National%20Quality%20Policy.pdf) |
| Livestock policy |   |   |   |   |
| Nutrition-sensitive agriculture mainstreaming guideline | Innovation and extension, productivity and market value chain, enabling government and responsible institution  | RAB, NAEB, RICA, RSB | RAB, NAEB, RSB, RICA, NIRDA, Local Government, Research institution, Private sector, NGO  |   |
{{< /bs-table >}}