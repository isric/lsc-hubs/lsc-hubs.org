

---
title: 'Rwanda Methodologies for LSC optimisation​'
date: 2023-08-22
type: kenya
---

This page provides a listing of resources on LSC optimization

<hr>

## Rwanda Agriculture and Animal Resources Development Board (RAB)

The Rwanda Agriculture and Animal Resources Development Board (RAB) is an autonomous body established by LAW N°38/2010 OF 25/11/2010 and Currently, RAB is governed by Presidential Order N° 074/01 OF 09/12/2022 Governing Rwanda Agriculture and Animal Resources Development Board (RAB), determining its mission, organization, and functioning. The law specifies that: The mission of RAB is to develop agriculture and animal resources through research, agricultural extension and animal resources extension in order to increase agricultural and animal resources productivity and quality, as well as their derived products.

- Various [online services](https://www.rab.gov.rw/online-services) is available to support land owners

<hr>

## FAO

The Food and Agriculture Organization (FAO) is a specialized agency of the United Nations that leads international efforts to defeat hunger. FAO provides educational materials:

- [The international Code of Conduct for the sustainable use and management of fertilizers](https://www.fao.org/documents/card/en/c/ca5253en/) provides guidance on fertilization
- The [PIP approach](https://www.fao.org/policy-support/tools-and-publications/resources-details/en/c/1027927/) is an inclusive bottom-up approach that engages people in environmental stewardship and sustainable change. 

Relevant PIP video's:

- [Gallery](https://www.wur.nl/en/research-results/research-institutes/environmental-research/programmes/sustainable-land-use/sustainable-agricultural-production-systems/the-pip-approach-building-a-foundation-for-sustainable-change/the-pip-approach-gallery.htm)
- [Empowering communities through the PIP approach: impact of the MWARES project](https://www.youtube.com/watch?v=lXPowU33E90)
- [PIP Approach in Burundi: PAPAB / WEnR (Alterra)](https://www.youtube.com/watch?v=S-gauxffsA8)

<hr>

## WOCAT

[WOCAT](https://www.wocat.net/en/) is a global network on Sustainable Land Management (SLM) that promotes the documentation, sharing and use of knowledge to support adaptation, innovation and decision-making in SLM.

- The [Global SLM Database](https://qcat.wocat.net/en/wocat/) contains over 1500 SLM practices from all over the world.

<hr>

## ISRIC

[ISRIC - World Soil Information](https://www.isric.org)  is an independent foundation with a mission to serve the international community as a custodian of global soil information.

- The [ISRIC resource library](https://resources.isric.org) contains a number of training materials and tools to improve data sharing in the Soil and SLM domain

<hr>

## MetaMeta

[MetaMeta](https://metameta.nl/) is a social enterprise based in the Netherlands, deeply engaged in water and natural resources management. They provide a set of training materials, free apon registration.

- [Meta meta Resource library](https://metameta.nl/resources)

<hr>

## Landportal

The [Land Portal Foundation](https://landportal.org) creates, curates and disseminates land governance information by fostering an inclusive and accessible data landscape.

- The foundation has published a number of [state of land information reports](https://landportal.org/library/state-of-land-information) and [impact stories](https://landportal.org/impact).
- [landvoc.org](https://explore.landvoc.org/landvoc/en/) is a universal vocabulary for land related terms, maintained by landportal as part of [FAO Agrovoc](https://www.fao.org/agrovoc/).

<hr>

## DigiFarm	

Increase an enabling environment for the farmers production goals	

- [Safaricom](https://www.safaricom.co.ke/media-center-landing/frequently-asked-questions/digifarm)
- [Mobile app](https://play.google.com/store/apps/details?id=com.safaricom.digifarm&hl=en&gl=US)

<hr>

## Shamba Shape-up	

Advice on Good Agricultural Practices (GAP)	by [The Mediae Company](https://mediae.org/)

- More then 100 episodes of farming advice [television](https://shambashapeup.com/series) and [leaflets](https://shambashapeup.com/leaflets/)
- [iShamba](https://ishamba.com/) A free SMS service with agronomy Q&A

<hr>


## One Acre Fund	

The [One Acre Fund](https://oneacrefund.org) empowers farmers for sustainable agricultural production		

- Various reports and datasets from regional studies and interventions are available at the [Insights and data library](https://oneacrefund.org/our-impact/how-we-measure-impact/impact-studies] 
- [Agronomic Survey data (2016-2022)](https://docs.google.com/spreadsheets/d/11_nzUxtpwV1nWaNYjDPcCPLUtbUsiuV9/edit?usp=drive_link&ouid=101538385841085303640&rtpof=true&sd=true) in Kenya, Rwanda, Tanzania, Uganda, Burundi, Malawi, Zambia.

<hr>

## Satelligence	

Combine local knowledge, field trips, AI-powered predictive modelling and remote sensing to monitor what’s happening on the ground.		

- [Satelligence](https://satelligence.com/)

<hr>

## e Farmers Hub	

Link farmer to market advisory, inputs access and mechanization, powered by [Syngenta Foundation](https://www.syngentafoundation.org)		

- [e-farmershub](https://www.syngentafoundation.org/agriservices/whatwedo/digitalsolutions/e-farmershub)

<hr>

## Plant Village Nuru	

Help farmers diagnose crop diseases	by 	[Penn State University](https://psu.edu)

- [PlantVillage](https://plantvillage.psu.edu/)

<hr>

## AgriBORA	

Improve farmers resilience to agricultural risks		

- [AgriBORA](https://agribora.com)

<hr>

## Tahmo	

Trans-African Hydro-Meteorological Observatory (Tahmo) in Nairobi aims to develop a vast network of weather stations across Africa. Current and historic weather data is important for agricultural, climate monitoring, and many hydro-meteorological applications.
Originates from the [H2020 research project Twiga](https://cordis.europa.eu/project/id/776691).

- [Tahmo](https://tahmo.org/)

<hr>

## Farm to Market Alliance	

FtMA is an Alliance of six agri-focused organizations (agra, bayer, yara, syngenta, rabobank, wfp). It's aim is to Empower farmers for increased farm productivity, market access, networking and digitization of services, hosted by the [Cereal Growers Association](https://cga.co.ke/).

- [Farm to Market Alliance](https://ftma.org/)

<hr>

## The Nutrition in City Ecosystems (NICE)

Agro-ecological production & market linkages for Poultry and Sweet potato value chains by [Syngenta Foundation](https://www.syngentafoundation.org) and [Sustainable Agroecosystems Group](https://sae.ethz.ch/).	

- [Nice](https://nice.ethz.ch/)


<hr>

## AgroCARES	

Provide timely location specific advice on soil nutrient management throughout the crops growth		

- [AgroCARES Netherlands](https://www.agrocares.com/)

<hr>


## eLeaf	

Provide satellite-based data layers at scale relevant for water management, climate analysis and agriculture. 
Contributed to the [WAPOR database](https://www.fao.org/in-action/remote-sensing-for-water-productivity/en/) hosted by FAO.		

- [eLeaf Company - Netherlands](https://eleaf.com/)

<hr>

## RATIN	

Make the grain sub-sector more transparent in terms of supply and demand		

- [East African Grain Council](https://ratin.net/)

<hr>

## iCow

Support farmers with livestock and crop production by connecting them to input providers		

- [Green Dreams Tech company Ltd](https://icow.co.ke/)

<hr>

## UjuziKilimo	

Complete soil & agronomic data pool that is both field specific and highly accurate - to support farm level decision making processes.		

- [Ujuzi Kilimo company](https://www.ujuzikilimo.com/)

<hr>

## LandPKS	

Allows users to make decisions on land suitability for crop production, the soil types and soil erosion risks, and supports decision making on sustainable land management practices.		

- [via RCMRD](https://apps.rcmrd.org/agricultural/land-potential-knowledge-system)
- [Landpotential.org](https://landpotential.org)
- [Mobile app](https://play.google.com/store/apps/details?id=org.landpotential.lpks.landcover&hl=en&gl=US)
- [1000 landscapes](https://landscapes.global/)
- [Terraso](https://landscapes.global/wp-content/uploads/2023/03/Terraso-.pdf)
- [Commonland](https://commonland.com)

<hr>

## mTela	

Aims to help small agro dealers/retailers for agro inputs to run the shop more efficient by allowing them to manage their sales, prices and stocks 100% on the phone		

- [Mtela](https://www.mtela.com/)

<hr>


## CABI Plantwise 	

Practical plant health and pest control information		

- [CABI](https://www.cabi.org/products-and-services/)


<hr>

Are you aware of additional materials on SLM approaches, you are invited to add them below.