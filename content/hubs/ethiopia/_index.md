---
title: 'LSC Information Hub Ethiopia'
date: 2023-07-05
type: ethiopia
---

This is the homepage of the Land, Soil, Crop (LSC) Information Hub - Ethiopia [**prototype**]. 

The LSC hub is currently under development by the LSC-Hub project team. Check back regularly to find new content arriving.

You can provide feedback by contributing to hub discussions at the [github repository](https://github.com/lsc-hubs/ethiopia-hub/discussions)

You can add and desribe new information by using the ODK form. You can access it by: [LSC ODK form](https://odk.isric.org/-/single/n2Sosp1gxbXLUcOelRBWsMru72DSLFb?st=pi9NCIyCBZIeYuVU0lb812NtQfOucM34dS04qF6GFaao2FB!c3DJDl10TTRTeOUJ)


{{% rw %}}
{{% cl w="2" class="text-center text-center border rounded bg-light" %}} 
<i class="fa fa-database fa-2xl pb-4"></i>

## Data

Data is the heart of the hub. It provides evidence to trigger discussion or predictive models.   

[Read more about data in the hub](data)

{{% /cl %}}
{{% cl w="2" class="text-center text-center border rounded bg-light" %}} 
<i class="fa fa-gear fa-2xl pb-4"></i>

## Predictive modelling

Distribution of environmental phenomena in time and space can be obtained using predictive modelling. 

[Read more about predictive modelling](models)


{{% /cl %}}
{{% cl w="2" class="text-center text-center border rounded bg-light" %}} 
<i class="fa fa-solid fa-shapes fa-2xl pb-4"></i>

## Information services

Information services provide dedicated information derived from relevant data to a targeted audience.

[Read more about information services](services).

{{% /cl %}}

{{% cl w="2" class="text-center text-center border rounded bg-light" %}} 
<i class="fa fa-solid fa-diagram-project fa-2xl pb-4"></i>

## Use cases

Stories highlight the capabilities of the hub for a number of use cases.

[Read the LSC Use Cases](usecases)

{{% /cl %}}

{{% cl w="2" class="text-center text-center border rounded bg-light" %}} 
<i class="fa fa-solid fa-balance-scale fa-2xl pb-4"></i>

## Policy

Which policies are relevant to the hub, and can the hub support policy development?

[Read more](policy)

{{% /cl %}}

{{% cl w="2" class="text-center text-center border rounded bg-light" %}} 
<i class="fa fa-users fa-2xl pb-4"></i>

## Hub community

Find out who participates in the [hub community](community) and what the community has to offer.


{{% /cl %}}

{{% /rw %}}


