---
title: 'Data publication in Ethiopia​'
date: 2023-10-13
type: ethiopia
---

In your work you may create data and information which is usefull to share with stakeholders. Various platforms exist for you to share your data. After sharing the data, we welcome you to reference the data from the LSC-Hub platform, so it becomes available to the LSC stakeholders.

## Share data

Various platforms exist to share your datasets.

- [Zenodo](https://zenodo.org) is a science oriented platform
- [Open Science Foundtaion](https://osf.io) is another science oriented platform
- [Google Drive](https://drive.google.com) allows to store files, including excel sheets, with option to collaborate on a resource

## Register dataset in LSC hub

For each dataset a metadata record should be made available. This metadata record can then be registered in the LSC catalogue. Some platforms, such as Zenodo and OSF create a metadata record as part of the dataset registration process. In other cases you can use Exel or MDME to create a record. Submit your list of records to the hub catalogue to have them included.

Read more on data publication at [EJPSoil](https://ejpsoil.github.io/soildata-assimilation-guidance/cookbook/zenodo.html)
