---
title: 'The Ethiopian Hub Community​'
date: 2023-08-09
type: ethiopia
---



{{% rw %}}
{{% cl class="text-center  border rounded bg-light" %}} 
<i class="fa-solid fa-users fa-2xl pb-4"></i>

## Stakeholders

A list of [Stakeholders](https://lsc-hubs.org/hubs/ethiopia/stakeholders/)

{{% /cl %}}

{{% cl  class="text-center  border rounded bg-light" %}} 
<i class="fa-solid fa-comments fa-2xl pb-4"></i>

## Discussion

Join discussions at the [Github repository](https://github.com/lsc-hubs/ethiopia-hub/discussions).
{{% /cl %}}

{{% cl class="text-center  border rounded bg-light" %}} 
<i class="fa-solid fa-calendar-days fa-2xl pb-4"></i>

## Events

Check the [calendar](#) for relevant events.
{{% /cl %}}

{{% cl class="text-center  border rounded bg-light" %}} 
<i class="fa-solid fa-envelope fa-2xl pb-4"></i>

## Newsletter

Join the [mailinglist](#) to receive our newsletter.
{{% /cl %}}


{{% /rw %}}
