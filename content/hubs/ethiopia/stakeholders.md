---
title: 'Ethiopian stakeholders'
date: 2023-03-21T15:37:57+07:00
type: ethiopia
---

On January 23, 2023 in Adama, Ethiopia, Land Soil Crop- Information Services’s (LSC Hubs) lead Ethiopian project partner Ethiopian Institute for Agriculture Research (EIAR) co-hosted a national stakeholders’ gathering on the topic of how information hubs for climate-smart land, soil and crop data can contribute to rural transformation. The event attracted more than 60 governmental, private business, and development partners with a vested interest in creating, sharing and disseminating agricultural knowledge and innovation. See the list of stakeholders below. 

*This list is in progress and new stakeholder are still being added. If you are engaged with the project and would like to be added, please email Emily Toner at emily.toner@isric.org.* 

Stakeholders are categorised by:

- [Development Partners](#development-partners)
- [Farmer Organisations](#farmer-organisations)
- [Knowledge Institutions](#knowledge-institutions)
- [Private sector](#private-sector)
- [Public sector](#public-sector)

&nbsp;

## Development Partners


{{% acc  id="dev" %}} 

{{% acc-item name="GIZ" url="https://www.giz.de/en" %}}Vision: We work to shape a future worth living around the world.
Acts: manage change, provide know-how, develop solutions, act as an intermediary, are value-driven, advise policymakers, secure results, are a global player, corporate Values (sustainability){{% /acc-item %}}
{{% acc-item name="CIAT " url="https://alliancebioversityciat.org/" %}}Goal: Towards food systems and landscapes that sustain the planet, drive prosperity, and nourish people.​ How: We deliver research-based solutions that harness agricultural biodiversity and sustainably transform food systems to improve people's lives in a climate crisis.{{% /acc-item %}}
{{% acc-item name="IFAD" url="https://www.ifad.org/en/web/operations/w/country/ethiopia" %}}The IFAD country programme has two main objectives:
1) improved resilience and productivity of ecosystems and livelihoods through improved management of natural resources, particularly water; 2) expanded linkages with the private sector to ensure increased and sustained access to markets, finance and agricultural technology. Key areas of investment include: 1) small-scale irrigation development; 2) sustainable natural resource management; 3) rural financial inclusion; 4) community-driven development among pastoral groups; 5) knowledge exchange through partnerships with the private sector, research institutions and other developing countries.{{% /acc-item %}}
{{% acc-item name="ILRI" url="https://www.ilri.org/ " %}}Mission: to improve food and nutritional security and to reduce poverty in developing countries through research for efficient, safe and sustainable use of livestock—ensuring better lives through livestock. Strategic objectives, with partners 1) to develop, test, adapt and promote science-based practices that—being sustainable and scalable—achieve better lives through livestock. 2) to provide compelling scientific evidence in ways that persuade decision makers—from farms to boardrooms and parliaments—that smarter policies and bigger livestock investments can deliver significant socio-economic, health and environmental dividends to both poor nations and households. 3) to increase capacity among ILRI’s key stakeholders to make better use of livestock science and investments for better lives through livestock.{{% /acc-item %}}
{{% acc-item name="IUCN" url="https://www.iucn.org/" %}}Vision: Influence, encourage and assist societies to conserve the integrity and diversity of nature and ensure that any use of natural resources is equitable and ecologically sustainable. How: Broad membership also means they can incubate ideas and are a trusted repository of best practices, tools and international standards. They provide a neutral space in which governments, NGOs, scientists, businesses, local communities, indigenous peoples’ organisations and others can work together to solve environmental challenges and achieve sustainable development.
Working with many partners and supporters, IUCN implements a large and diverse portfolio of conservation projects worldwide. These projects combine the latest science with traditional knowledge of local communities to work to reverse habitat loss, restore ecosystems and improve people’s well-being.{{% /acc-item %}}
{{% acc-item name="One Acer Fund" url="https://oneacrefund.org/" %}}Aim: Tree program is to provide not only short-term income opportunities but also long-term benefits, such as soil health improvements and erosion control, so that farmers build resilience over time. Working also on: 1) Building a unique model for quality community-based crop seed production and multiplication. 2) Equipping private entrepreneurs and government nurseries to grow coffee seedlings for sale to farmers. They expect this to benefit women in particular. 
3) Testing new fruit tree offerings (such as avocado, moringa, coffee, neem, papaya, mango) in our network of agroforestry research sites. 4) Engaging with a malt factory and two cooperatives to kick start quality malt barley production in Amhara. 5) Exploring carbon sales as a way to cover program costs, add value to farmers’ tree planting and support local government initiatives{{% /acc-item %}}
{{% acc-item name="USAID" url="https://www.usaid.gov/" %}}Mission: to support partners to become self-reliant and capable of leading their own development journeys. We make progress toward this by reducing the reach of conflict, preventing the spread of pandemic disease, and counteracting the drivers of violence, instability, transnational crime and other security threats{{% /acc-item %}}
{{% acc-item name="ICRAF" url="https://www.cifor-icraf.org/" %}}CIFOR–ICRAF addresses local challenges and opportunities while providing solutions to global problems for forests, landscapes, people and the planet. We deliver actionable evidence and solutions to transform how land is used and how food is produced: conserving and restoring ecosystems, responding to the global climate, malnutrition, biodiversity and desertification crises. In short, improving people’s lives, while preserving the environment. In this context, they work closely with farmers, forest communities and a wide range of partners. In addition, they also drive the international dialogue on sustainable land use by convening large-scale events and establishing communities of practice through the Global Landscapes Forum{{% /acc-item %}}
{{% acc-item name="SAA" url="https://www.saa-safe.org/www/ethiopia.html" %}}Works in close collaboration with national agricultural extension services in Ethiopia, Mali, Nigeria and Uganda to support smallholder farmers along agricultural value chain. SAA aims to increase farmers’ income and food and nutrition security through promoting market-oriented, sustainable, resilient, and regenerative and nutrition-sensitive agricultural innovations and building the capacity of Extension Agents and farmers.{{% /acc-item %}}
{{% acc-item name="FAO" url="https://www.fao.org/" %}}Mission: to achieve food security for all and make sure that people have regular access to enough high-quality food to lead active, healthy lives. {{% /acc-item %}}
{{% acc-item name="Green climate Fund" url="https://www.greenclimate.fund/" %}}The Green Climate Fund (GCF) – a critical element of the historic Paris Agreement - is the world’s largest climate fund, mandated to support developing countries raise and realize their Nationally Determined Contributions (NDC) ambitions towards low-emissions, climate-resilient pathways. Investing across four transitions – built environment; energy & industry; human security, livelihoods and wellbeing; and land-use, forests and ecosystems – and employing a four-pronged approach:  Transformational planning and programming, catalysing climate innovation, de-risking investment to mobilize finance at scale, mainstreaming climate risks and opportunities into investment decision-making to align finance with sustainable development{{% /acc-item %}}
{{% acc-item name="Welthungerhilfe" url="https://www.welthungerhilfe.org/our-work/countries/ethiopia" %}}Vision: A world in which all people can exercise their right to lead a self-determined life in dignity and justice, free of hunger and poverty. How: fights against global hunger and for sustainable food security. This includes promoting site-oriented agriculture, access to clean water, environmentally friendly energy supplies and improving health and education:
- Clean Water - Accessible through Boreholes
- More Girls Graduating
- Defying the Droughts
- First Things First: Water, Food, and Shelter
- Building the Capacity of Local Organisations to Provide Humanitarian Aid
- Maintaining Livelihoods by Protecting Livestock
- Protecting Forests to Stop Desertification
- Dietary Abundance and Diversity
- Lobbying for Land Rights
- Land for Life: Lobbying for Land Rights and Reforms
- Implementing Fair Land Ownership{{% /acc-item %}}
{{% acc-item name="UN-REDDS" url="https://www.un-redd.org/about/about-redd" %}}UN-REDD is the flagship UN knowledge and advisory partnership on forests and climate to reduce forest emissions and enhance forest carbon stocks. It is the largest international provider of REDD+ assistance, supporting its 65 partner countries to protect their forests and achieve their climate and sustainable development goals.{{% /acc-item %}}
{{% acc-item name="CMP" url="https://www.cmpethiopia.org/page/1011" %}}The Community Managed Project (CMP) approach is one of the four service delivery models for the development and use of rural WaSH infrastructure in Ethiopia. Effectively it is an implementation modality which helps communities realise their WaSH projects. The CMP approach decentralizes service delivery by making communities responsible for the planning, implementation and maintenance of new water schemes, whilst district authorities provide capacity building and external support{{% /acc-item %}}
{{% acc-item name="SNV" url="https://snv.org/" %}}At SNV, we want to see a world where across every society, all people live with dignity and have equitable opportunities to thrive sustainably.

Our world is changing with dramatic speed: from pandemics to conflicts to environmental crises, hundreds of millions of people still lack access to energy, food, and water. And we know that development progress is not yet reaching everyone.

That’s why, driven by the Sustainable Development Goals, we aim to strengthen capacities and catalyse partnerships that can transform the agri-food, energy, and water systems. Our projects directly benefit millions of people across Africa and Asia. They contribute to true systems transformation, while promoting gender equality and social inclusion, facilitating climate adaptation and mitigation, and strengthening institutions and effective governance.{{% /acc-item %}}
{{% acc-item name="SOS Sahel Ethiopia" url="https://sossahelethiopia.org/" %}}SOS Sahel Ethiopia envisions a world without poverty and It is dedicated to improve the living standards of smallholder farmers and marginalized pastoralists through better management of their environment. The work of SOS Sahel focuses on community-based natural resources management, food security, agriculture, policy analysis, value chain analysis and development, pro-poor value chain development.{{% /acc-item %}}
{{% acc-item name="WetLand International" url="https://www.wetlands.org/" %}}Their vision is a world where wetlands are treasured and nurtured for their beauty, the life they support and the resources they provide. Their mission is to inspire and mobilise society to safeguard and restore wetlands for people and nature.
Their strategic framework guides us towards achieving healthy wetlands, resilient wetland communities, and reduced climate risks across three “streams” of work or wetland landscapes: coasts and deltas, rivers and lakes, and peatlands.  They  measure their progress across these three streams according to the following six types of desired outcomes:  
- Wetland habitats and functions safeguarded and restored: 
- Wetland species recovered
- Water and food secured for wetland communities
- Reduced societal conflict and displacement from wetlands
- Wetland carbon stores secured and enhanced
- Wetland nature-based solutions integrated into infrastructure{{% /acc-item %}}

{{% /acc %}}

&nbsp;


## Farmer Organisations

{{% acc id="farm" %}} 

{{% acc-item name="Bulbula Integrated Agro industry PLC" url="www.oipdc.org.et" %}}It is administered under the Oromia Industrial Parks Development Corporation (www.oipdc.org.et/), which has as mission: Developing and managing a world class standardized industrial parks and special manufacturing industry clusters that enhance revenue to the government.{{% /acc-item %}}
{{% acc-item name="Debut irrigation union" url="" %}}{{% /acc-item %}}
{{% acc-item name="Debut kebele cooperative association" url="" %}}{{% /acc-item %}}
{{% acc-item name="Erer Farmers Cooperation Union" url="https://ererunion.com/" %}}Mission: Providing better services: at fair price, required time and amount in which members competent enough in free market with providing better price to their products. How:
- Purchase Agricultural inputs & distribute to its members. (improved seeds, Agro-chemicals & farm machineries)
- Marketing of members product
- Credit service
- Training and education for members BOD & staff
- Tractor rental service
- Audit service to members
- Distributing consumer goods
- Crop insurance coverage
- Participating on other social affairs
- Storage and marketing information service
- Transportation service
- Seed cleaning
- Marketing of members seed product
- To generate revenue for members{{% /acc-item %}}
{{% acc-item name="Kebele Cooperative" url="" %}}{{% /acc-item %}}
{{% acc-item name="Lume Adama Farmers’ cooperative" url="" %}}Mission: 1) Ensuring efficient and effective supply of input; 2)Achieving better market price through increasing bargaining power and implementing value adding activities; 3) Providing different services which are important for farming and better-off 
livelihood; 4) Creating a self reliant cooperative organization 
for surrounding farmers.
Activities: Distribution of agricultural input, Marketing of farmers produce, Import and export, Processing, Seed multiplication and preparation, Tractor rental service, Training services, Credit service, Transport service, Meeting hall services, Secretarial service, Crop insurance service, Distribution of chicken to the Farmers {{% /acc-item %}}
{{% acc-item name="North Shewa zone cooperative office" url="" %}}{{% /acc-item %}}
{{% acc-item name="Wito, Chiraro Debir, Bas Dengura kebele" url="" %}}{{% /acc-item %}}
{{% acc-item name="Bora Union " url="" %}}{{% /acc-item %}}

{{% /acc %}}

&nbsp;


## Knowledge Institutions

{{% acc  id="ki" %}} 

{{% acc-item name="Debre Berhan University (DBU)" url="https://www.dbu.edu.et/" %}}{{% /acc-item %}}
{{% acc-item name="Oromia Agricultural Research Institute" url="https://iqqo.org/" %}}Support the agricultural development in Oromia Regional State through adapting, generating, multiplying, and disseminating appropriate agricultural technologies.{{% /acc-item %}}
{{% acc-item name="Wageningen University and Research" url="https://www.wur.nl/" %}}Mission: "To explore the potential of nature to improve the quality of life". The strength of Wageningen University & Research lies in its ability to join the forces of specialised research institutes and the university. It also lies in the combined efforts of the various fields of natural and social sciences. This union of expertise leads to scientific breakthroughs that can quickly be put into practice and be incorporated into education. Domains: Food, feed & biobased production, Natural resources & living environment, Society & well-being{{% /acc-item %}}
{{% acc-item name="Debre Berhan Agricultural Research centre (DBARC)" url="" %}}{{% /acc-item %}}
{{% acc-item name="Artificial Intelligence Institute" url="http://www.aii.et/" %}}Mission: Fostering the Development of a Nationally Recognized AI Ecosystem to Empower and Inspire a Nation for Peace and Prosperity with the most Trusted Analytics.
Duties:
- Provide research-based artificial intelligence services and products
- Set national infrastructure that enables artificial intelligence research and development programs.
- Formulate national Artificial intelligence related policies, legislation, and regulatory frameworks
- Create an eco-system that enables public-private interconnectivity and co addition,
- Support private initiatives on artificial intelligence research, development, and deployment
- Make sure that artificial intelligence service the defense and national security-related decision-making process,
- Ensure that artificial intelligence support socio-economic programs such as health, education, agriculture, and utilities.
- Enables artificial intelligence to support decision making process on urban administration, land administration, national disaster - prevention and environmental hazards.
- Facilitate national database development and utilization
- Help human resource development on the area in collaboration higher institutions
- Own property, enter into contracts, sue and be sued in its own name
- Performs other duties related to its objectives{{% /acc-item %}}
{{% acc-item name="Adami tulu ARC" url="https://iqqo.org/?q=atac" %}}Objectives of the center: To generate adapt and transfer feasible technologies that could improve agricultural productivity and contribute to economic and socio development of East Shoa and West Arsi zone{{% /acc-item %}}
{{% acc-item name="Arsi University" url="https://arsiun.edu.et/" %}}Mission: Arsi University strives to bring sustainable development through practice-based teaching-learning process and conducting problem-solving research in collaboration with industries.
Vision: Becoming one of the most known and chosen Applied Science universities in East Africa by 2030{{% /acc-item %}}
{{% acc-item name="Batu ARC" url="" %}}{{% /acc-item %}}
{{% acc-item name="Biotechnology Institute" url="https://www.ebti.gov.et/" %}}{{% /acc-item %}}
{{% acc-item name="Water and Land Resource Centre" url="https://www.wlrc-eth.org/" %}}Objective: Establish a full-fledged and modern resource database for water and land management in Ethiopia building on accumulated data and knowledge of the University of Bern, its learning watershed, observatories and of other similar undertakings in the country. It will generate appropriate information on processes of hydro-sedimentology, meteorology and land management and serve as a knowledge hub in the country and the Eastern Nile Region at large. It aims at helping decision makers, planners and the scientific community to make informed decision and planning for reducing degradation and poverty in the country and the Easter Nile Region, for exploring alternative potential uses of natural resources, and for developing benefit sharing mechanisms.{{% /acc-item %}}
{{% acc-item name="SWR" url="Stichting Wageningen Research Ethiopia launches new project portfolio - WUR" %}}Stichting Wageningen Research Ethiopia (SWR Ethiopia) is an international NGO that has been registered in Ethiopia since March 2021. Where projects in Ethiopia were bilateral undertakings until now, SWR Ethiopia will serve as a hub for projects that WUR undertakes with all kinds of partners in Ethiopia.{{% /acc-item %}}
{{% acc-item name="ISRIC – World Soil Information" url="https://www.isric.org/" %}}Mission: serve the international community as a custodian of global soil information. We support soil data, information and knowledge provisioning at global, national and sub-national levels for application into sustainable management of soil and land.{{% /acc-item %}}

{{% /acc %}}

&nbsp;


## Private sector

{{% acc id="private" %}} 

{{% acc-item name="Kacha Digital Financial Service " url="https://www.kacha.et/" %}}Kacha Digital Financial Service S.C is a mission to grow, first private payment instrument issuer in Ethiopia. Under the payment instrument issuer directive ONPS/01/2020, a licensed payment instrument issuer is allowed to provide micro saving, micro credit, micro insurance, pension products and international remittances in collaboration with reputable financial institutions.{{% /acc-item %}}
{{% acc-item name="Kifiya Financial Tec. PLC" url="https://kifiya.com/" %}}Kifiya is a technology and services company developing scalable and secured technology platforms over the past twelve years. Over the years, Kifiya has gained a strong market understanding and expertise in distribution, microinsurance, digital agricultural services, and delivery of financial technology products and services{{% /acc-item %}}
{{% acc-item name="Leresha" url="https://www.lersha.com/" %}}Lersha’ (ለእርሻ) is an Amharic equivalent to the phrase 'for agriculture’. It represents the desire to fulfill the agricultural needs of small holder farmers through innovation. Lersha provides a one-stop digital service to small holder farmers and enables them to access farm inputs, hire mechanization services, and request dynamic agro-climate advice using technology. The platform combines a mobile application, a call center and lersha agents to facilitate transactions with farmers.{{% /acc-item %}}
{{% acc-item name="Midroc Investment Group" url="https://www.midrocinvestmentgroup.com/" %}}Company is divided in multiple clusters. I have focused on Agriculture and Agro-processing Cluster. They focus on Coffee
Tea, Medicinal and Aromatic Plants, Crops, Fruits and Vegetables, Cut Flowers, Forest Honey, Spices, Poultry Products, Meat Products{{% /acc-item %}}
{{% acc-item name="Nyala Insurance" url="https://www.nyalainsurancesc.com/" %}}Private insurance companie. Specialised a.o. in agricultural micro insurance, multi- peril crop insurance, coffee plantation insurance, indemnity livestock insurance.{{% /acc-item %}}
{{% acc-item name="Mekobo Enterprise" url="" %}}{{% /acc-item %}}
{{% acc-item name="MAKOBU" url="" %}}{{% /acc-item %}}

{{% /acc %}}

&nbsp;


## Public sector

{{% acc id="public" %}} 
{{% acc-item name="Ethiopian Institute of Agricultural Research" url="http://www.eiar.gov.et/" %}}Mission is to conduct research that will provide market competitive agricultural technologies that will contribute to increased agricultural productivity and nutrition quality, sustainable food security, economic development, and conservation of the integrity of natural resources and the environment. Vision: EIAR aspires to see improved livelihood of all Ethiopians engaged in agriculture, agro-pastoralism, and pastoralism through market competitive agricultural technologies. {{% /acc-item %}}
{{% acc-item name="Space Science and GIO special Institute " url="https://etssti.org/" %}}Mission: Contribute to the development of the national economy by providing creative and social services to our people and improving their living conditions in the field of space science and technology, In astronomy and astrophysics, on earth view global and to provide competitive research in aeronautics and astronomy, manpower training and international relations.{{% /acc-item %}}
{{% acc-item name="Ethiopian Agriculture business corporation" url="https://ethioagri.com/" %}}Mission: “Provide Agricultural inputs and technologies that should considerably improve production and productivity, boost modern farms and Agro-Industries at very competitive price to make the economic development of the nation grow fast.” The purpose for  are:
1.    to buy from domestic and international markets and supply agricultural inputs, process same; undertake agricultural inputs market price stabilization activities;
2.    to render agricultural mechanization services, agricultural and construction equipment repair services and provide rental services of agricultural machineries and transport vehicles;
3.    to buy from domestic and international markets and supply agricultural machineries and spare parts, construction equipment and agro-chemicals;
4.    to provide necessary education for promotion of the use of modern agricultural machineries; provide consultancy service in handling and use of agricultural machineries and provide on the job technical trainings;
5.    to harvest, buy, value add to and process natural gum, produce other forest products, and supply to domestic and international markets;{{% /acc-item %}}
{{% acc-item name="Bako enterprise" url="" %}}{{% /acc-item %}}
{{% acc-item name="Batu Soil research Centre" url="https://iqqo.org/?q=bsrc" %}}Research Focus: Soil fertility and productivity improvement is the research focus of the center{{% /acc-item %}}
{{% acc-item name="CSA" url="http://www.csa.gov.et/" %}}The major mandates and responsibilities of the CSA, among others, are: Collect statistical data through censuses, sample surveys, administrative records and registrations as well as process, evaluate, analyze, publish and thereby disseminate the results and also serve as the country’s information center; Prepare short, medium or long-term national statistical program for the collection, processing, evaluation and analysis of data required for socio-economic development planning and upon approval execute the program and projects within the given budget{{% /acc-item %}}
{{% acc-item name="East Shewa Agriculture Bureau" url="" %}}{{% /acc-item %}}
{{% acc-item name="Etiopian Environment Protection Authority" url="https://www.epa.gov.et/" %}}Actions:
- Coordinate activities to ensure that the environmental objectives provided under the Constitution and the basic principles set out in the Environmental Policy of the Country are realized;
- Establish a system and follow up implementation for undertaking environmental impact assessment or strategic environmental assessment on social and economic development polices, strategies, laws, programs and project set by the government or Privet;
- Prepare a mechanism that promotes social, economic and environmental justice and channel the major part of benefit derived thereof to the affected communities to reduce emissions of greenhouse gases that would otherwise have resulted from deforestation and forest degradation;
- Coordinate actions on soliciting the resources required for building a climate resilient green economy in all sectors and at all Regional levels; as well as provide capacity building support and advisory services;
- Establish a system for evaluating and decision making, in accordance with the Environmental Impact Assessment Proclamation, the impacts of implementation of investment programs and projects on environment prior to approvals of their implementation by the concerned sectoral licensing organ or the concerned regional organ;
- Prepare programmes and directives for the synergistic implementation and follow up of environmental agreements ratified by Ethiopia pertaining to the natural resources base, desertification, forests, hazardous chemicals, industrial wastes and anthropogenic environmental hazards with the objective of avoiding overlaps, wastage of resources and gaps during their implementation in all sectors and at all governance levels;
- Take part in the negotiations of international environmental and climate change agreements and, as appropriate, initiate a process of their ratification; play key role in coordinating the nationwide responses to the agreements;
- coordinate, and as may be appropriate, carry out research and technology transfer activities that promotes the sustainability of the environment and the conservation and use of forest as well as the equitable sharing of benefits accruing from them while creating opportunities for green jobs;
- Establish a system for development and utilization of small and large scale forest including bamboo on private, communal and watershed areas, and ensure implementation of same;
- Establish a system to rehabilitate degraded forest lands and ensure its implementation to enhance their environmental and economic benefits. {{% /acc-item %}}
{{% acc-item name="Ethiopian Bio Diversity Institute " url="https://ebi.gov.et/" %}}Objective: To ensure that the country’s biodiversity and the associated community knowledge are properly conserved and sustainably utilized, and the country and its communities get fair and equitable share of the benefits arising from their utilization.{{% /acc-item %}}
{{% acc-item name="Ethiopian Geological Survey" url="" %}}Vision: Facilitate the utilization of the geoscience data of Ethiopia for developing the country’s mineral resources, so as to contribute as much as possible to its economic growth.
Goal: Improve the quality and coverage of the geoscience data of the country.{{% /acc-item %}}
{{% acc-item name="Ethiopian Public Health Institute" url="https://ephi.gov.et/" %}}Mission: To protect and promote the health of the Ethiopian people by addressing priority public health and nutrition problems through problem-focused research, public health emergency management, establishing and maintaining  a quality laboratory system. How: 
Objective 1: Research and technology transfer
Objective 2: Increase the availability of vaccines, plant-
based  medicines, complementary foods and other
biological products 
Objective 3: Public Health Emergency Management
Objective 4: Public Health Laboratory Service
Objective 5 Research and Technology Transfer
Objective 6: Increase problem solving research on
disease, nutrition, traditional medicine and modern
drugs 
Objective 7: Improve research on health system and intervention evaluation
Objective 8: Enhance the production of vaccines, standardized  plant based medicine and food product 
Objective-9 Public Health Emergency Management
Objective-10. Improve risk identification and PHE preparedness
Objective 11: Public Health Laboratory Service
Objective  12: Improve the diagnostic  capacity of laboratories Finance
Objective 13 Management and Leadership
Objective 14: Improve human resource management
Objective 15: Improve project management system
Objective-16: Strengthen the capacity of technical
facilities 
Objective-17. Enhance coordination and collaboration{{% /acc-item %}}
{{% acc-item name="Ethiopian Statistics Service " url="http://www.statsethiopia.gov.et/" %}}Mission: To establish an integrated and well-coordinated National Statistical System under the guidance and leadership of the Ethiopian Statistics Service, professionally producing official statistics that meet the current and evolving needs of users in a transparent and timely fashion, using standards and best statistical practices. How: Collect and compile data from sample surveys, censuses and administrative records, and also analyze data and deliver report for various sectors and users. Additionally, the ESS provides technical guidance and assistance to government agencies, institutions and individuals to help them to establish administrative recording, registration and reporting systems and to build the capacity required for providing directives and consultations on the development of administrative records and registration systems.{{% /acc-item %}}
{{% acc-item name="MHARA FOREST ENTERPRISE" url="https://www.amharaforest.com/" %}}Mission: To accelerate the economic and social development of the region by extensively developing and producing forest and forest products by developing environmental friendly forests{{% /acc-item %}}
{{% acc-item name="Sinqe Bank" url="https://siinqeebank.com/" %}}Vision: To be the Leading Bank in Financial inclusion and Transformation. Mission: Siinqee Bank is dedicated to provide integrated, inclusive, and innovative banking and micro-finance services through its committed, dynamic and disciplined employees by deploying state-of-the art information technology to empower the society and maximize stakeholders’ value with a special focus to be the financial alternative and sources of area knowledge with adherence to the spirit of law.{{% /acc-item %}}
{{% acc-item name="Sugar Industry" url="https://etsugar.com/esig/" %}}Mission: Building modern technology and capable human resource so as to develop the nation’s potential of the sector and hence produce sugar, sugar bi-products and co-products and satisfy the domestic market beyond taking foreign market share and thereby support the nation’s economy. How: 
- Follow up and monitor the execution of new as well as existing feasible public sugar projects and commission such projects as independently – operating sugar mill;
- Create a modern and efficient distribution system for locally manufactured and imported sugar, sugar by-products and co-products and ensure effective distribution of such products in the domestic market as well as export such products where appropriate;
- Approve the operating budget, the investment plan and budget of state owned sugar mills as well as monitor, follow-up and evaluate their periodic performance;
- Support trade and investment activities of state owned sugar factories and collaborate with same in the building of sugar byproduct and co-product processing plants;
- Submit proposal to the Minister of Finance on amount of annual income from sugar sale that state owned sugar mills are allowed to retain for the purpose of determining their contribution to the sugar fund in accordance with Article 4 of the Sugar Industry Development Fund and implement same upon approval;
- Facilitate capacity building activities in favor of state owned sugar factories, by strengthening university industry linkages for training and research as well as provide same with business advice and other technical support;
- Manufacture factory components and spare parts independently or in partnership with local or foreign companies and carryout from inside or outside Ethiopia, bulk procurements of large machinery, spare parts and various inputs that are of common requirement to sugar factories under a framework contract and supply same to state owned sugar mills;
- Raise finance capital by selling bonds or negotiating and entering into a loan agreement with local or foreign financial institutions in accordance with the policy direction and directives issued by the Ministry of Finance;{{% /acc-item %}}
{{% acc-item name="Woreda Agricultural Bureau" url="" %}}{{% /acc-item %}}
{{% acc-item name="Ministery of Health - Ethiopia" url="https://www.moh.gov.et/site/THE_SEQOTA_DECLARATION" %}}Vision: To see Ethiopian children being free from malnutrition. Goal: To end stunting in children under two by 2030. 
Mission: Seqota Declaration works to end stunting in Ethiopia for children less than two years through effective coordination and collaboration of sectors, communities and development partners, focusing on high impact nutrition specific and nutrition sensitive interventions and social behavior change communications with special consideration for cross-cutting issues such as gender mainstreaming, environment and integrated community development approaches.{{% /acc-item %}}
{{% acc-item name="Ethiopian Meteorology Institute" url="http://www.ethiomet.gov.et/" %}}Objective: investigate and study the weather and climatic condition of Ethiopia in order to exploit the beneficial effects for economic and social development. How:
1. To carry out the tasks of fulfilling these national needs and discharging international obligations regarding meteorology.
To establish and operate a national network of meteorological stations.
2. Disseminate advice and educational information to public on weather on regarding meteorological issues.
3. Provide meteorological services to all stakeholders. Collaborators and the general public.
4. Provide advice and early warnings regarding the  adverse effects weather and climate to pertinent institutions and the general public.
5. Fulfill international treaties that Ethiopia had ratified in the area of meteorology.{{% /acc-item %}}
{{% acc-item name="ATI" url="" %}}{{% /acc-item %}}
{{% acc-item name="EMI" url="" %}}{{% /acc-item %}}
{{% acc-item name="OBA" url="" %}}{{% /acc-item %}}


{{% /acc %}}
