---
title: 'Information services in Ethiopia'
date: 2023-07-05
type: ethiopia
---

Information services provide dedicated information derived from relevant data to a targeted audience.

Processing raw data in many cases requires expert knowledge. To bring the value of the data to a wider audience, it is of interest
to set up information services derived from that data to targeted audiences.

{{% rw %}}
{{% cl w="3" class="text-center  border rounded bg-light" %}} 
<i class="fa-solid fa-magnifying-glass fa-2xl pb-4"></i>

## Catalogue

Find information services in the [catalog component](https://ethiopia.lsc-hubs.org/collections/metadata:main/items?type=service)

{{% /cl %}}

{{% cl class="text-center  border rounded bg-light" %}} 
<i class="fa-solid fa-book fa-2xl pb-4"></i>

## Documentation 

Evaluate [Tools and Guidance](#) on how to set up data services.
{{% /cl %}}
{{% cl  class="text-center  border rounded bg-light" %}} 
<i class="fa-solid fa-comments fa-2xl pb-4"></i>

## Discussion

Ask questions about use or creation of services in [the hub community](#).

{{% /cl %}}

{{% cl class="text-center  border rounded bg-light" %}} 
<i class="fa-solid fa-diagram-project fa-2xl pb-4"></i>

## Use cases

Read more about services related to specific [use cases](usecases).

{{% /cl %}}
{{% /rw %}}