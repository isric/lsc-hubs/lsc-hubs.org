---
title: 'Predictive modelling in Ethiopia'
date: 2023-07-05
type: ethiopia
---

Distribution of environmental phenomena in time and space as well as future yields at various scenario's can be obtained using predictive modelling.


{{% rw %}}
{{% cl w="3" class="text-center  border rounded bg-light" %}} 
<i class="fa-solid fa-magnifying-glass fa-2xl pb-4"></i>

## Catalogue

Locate modelling software in the  [catalog component](https://ethiopia.lsc-hubs.org/collections/metadata:main/items?type=software)

{{% /cl %}}

{{% cl class="text-center  border rounded bg-light" %}} 
<i class="fa-solid fa-book fa-2xl pb-4"></i>

## Documentation 

Evaluate [Tools and Guidance](#) about predictive modelling.
{{% /cl %}}

{{% cl  class="text-center  border rounded bg-light" %}} 
<i class="fa-solid fa-comments fa-2xl pb-4"></i>

## Discussion

Ask questions about predictive modelling in [the hub community](#).

{{% /cl %}}

{{% cl class="text-center  border rounded bg-light" %}} 
<i class="fa-solid fa-diagram-project fa-2xl pb-4"></i>

## Use cases

Read more about predictive modelling related to specific [use cases](usecases).

{{% /cl %}}
{{% /rw %}}

