---
title: 'Ethiopia LSC Use Cases'
date: 2023-07-05
type: ethiopia
---

This page describes a number of use cases relevant to LSC hubs. 

The two ‘Use Cases’ are brief descriptions of key applications for which the DeSIRA LSC-IS hubs can be used, including the main stakeholders, the main issues and the models through which ‘LSC data’ is converted into information services that support informed decision making.



{{% rw %}}
{{% cl w="4" class="text-center text-center border rounded bg-light" %}} 
<i class="fa fa-tractor fa-2xl pb-4"></i>

##  Integrated Soil Fertility management​

[Read more](../isfm) about this use case.

{{% /cl %}}
{{% cl w="4" class="text-center text-center border rounded bg-light" %}} 
<i class="fa fa-hand-holding-droplet fa-2xl pb-4"></i>

## Soil water conservation

[Read more](../swc) about this use case.

{{% /cl %}}
{{% /rw %}}
