---
title: 'Find or contribute data in the Ethiopia LSC Hub components'
date: 2023-08-09
type: ethiopia
---

This hub around Land Soil and Crop information facilitates effective access to data in various ways. It aims to make existing resources better findable, accessible, interoperable and reusable ([FAIR](https://www.go-fair.org/fair-principles/)) as well as identify and fill gaps of data, required for LSC use cases. 


{{% rw %}}
{{% cl w="3" class="text-center  border rounded bg-light" %}} 
<i class="fa-solid fa-magnifying-glass fa-2xl pb-4"></i>

## Findable

Find LSC datasets in the [catalog component](https://ethiopia.lsc-hubs.org)

{{% /cl %}}
{{% cl w="3" class="text-center border rounded bg-light " %}} 
<i class="fa-solid fa-download fa-2xl pb-4"></i>

## Accessible

Access to datasets is facilitated by the [LSC terriaMap Data viewer](https://maps.lsc-hubs.org/#lsc-ethiopia)

The hub provides guidance and tools on [how to archive and share datasets](data-publication.md). 

{{% /cl %}}
{{% cl w="3" class="text-center  border rounded  bg-light" %}} 
<i class="fa-solid fa-handshake fa-2xl pb-4"></i>

## Interoperable

The hub provides guidance and tools for [adoption of standardised formats and interfaces](#).

{{% /cl %}}
{{% cl w="3" class="text-center  border rounded bg-light " %}} 
<i class="fa-solid fa-users fa-2xl pb-4"></i>

### Reusable

The platform provides guidance and tools for:

- [licensing](#)
- [standardisation](#)
- [aggregation](#)
- build [services](#) 

{{% /cl %}}
{{% /rw %}}

## What types of data are expected

There are various types of data in the LSC hubs. At the moment, these types of data are in the LSC hub:

- Observation data from field studies, such as soil/water samples, crop yields 
- Observation data from air or space
- delineation of administrative boundaries, transport networks
- Economic data on price development
- Outputs of predictive models (soil, weather, yields)
- Statistical data on population
