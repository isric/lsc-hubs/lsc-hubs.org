---
title: 'Kenya Agricultural & Livestock Research Organization (KALRO)'
date: 2018-11-28T15:14:54+10:00
icon: '/images/KALRO_website_LogoCard.png'
featured: true
weight: 5
draft: false
heroHeading: 'Kenya Agricultural & Livestock Research Organization (KALRO)'
heroSubHeading: 'Kenya Agricultural & Livestock Research Organization (KALRO) is a corporate body created under the Kenya Agricultural and Livestock Research Act of 2013 to establish a framework for coordination of agricultural research in Kenya that is dynamic, innovative, responsive and well-coordinated.'
heroBackground: 'services/service1.jpg'
country: Kenya
link: https://www.kalro.org
role: team
---

{{% staff name="Michael Okoti" role="Senior Researcher at KALRO" img="/images/michael.jpg" %}}
Environment and Sustainability
{{% /staff %}}

{{% staff name="Elizabeth Adobi Okwuosa" role="Soil Scientist at KALRO" img="/images/elizabeth.jpg" %}}
{{% /staff %}}

{{% staff name="Boniface Akuku" role="CTO/IT Director at KALRO" img="/images/boniface.jpg" %}}
Information systems, open data, open science and knowledge management
{{% /staff %}}

