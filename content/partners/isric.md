---
title: 'ISRIC - World Soil Information'
date: 2018-11-28T15:14:39+10:00
icon: '/images/ISRIC_Website_LogoCard.png'
featured: true
weight: 2
draft: false
heroHeading: 'ISRIC - World Soil Information'
heroSubHeading: 'ISRIC - World Soil Information is an independent foundation and a custodian of global soil information which they produce, gather, compile and serve together with our partners at global, national and regional levels.'
heroBackground: 'services/service2.jpg'
country: Netherlands
link: https://www.isric.org
role: team
---
 


{{% staff name="André Kooiman" role="Senior sustainable land management expert at ISRIC" img="/images/andre.jpg" %}}
LSC Hubs project role: Scientific coordinator
{{% /staff %}}
{{% staff name="Zhanguo Bai" role="Senior soil and land degradation assessment expert at ISRIC" img="/images/Zhanguo_Bai500_0.jpg" %}}
LSC Hubs role: Input in soil and water conservation use case
{{% /staff %}}
{{% staff name="Luis Calisto" role="Database development expert at ISRIC" img="/images/Luis_Calisto500b.jpg" %}}
LSC Hubs role: Support for spatial data infrastructure, in task 3.4 and Task 3.5.
{{% /staff %}}
{{% staff name="Betony Colman" role="Junior GIS Technician at ISRIC" img="/images/betony500b.jpg" %}}
LSC Hubs role: Support in LSC mapping in task 3.2 and 3.3
{{% /staff %}}
{{% staff name="Giulio Genova" role="Digital soil mapping expert at ISRIC" img="/images/giulio.jpg" %}}
LSC Hubs role: Task leader 3.1 Data compilation, support mapping in task 3.2
{{% /staff %}}
{{% staff name="Paul van Genuchten" role="Spatial data infrastructures expert at ISRIC" img="/images/Paul_van_Genuchten.jpg" %}}
LSC Hubs project role: Support for spatial data infrastructure, PMEAL dashboard in Task 3.1, Task 3.4 and Task 3.5.
{{% /staff %}}
{{% staff name="Bas Kempen" role="Senior digital soil mapping expert at ISRIC" img="/images/Bas_Kempen500b.jpg" %}}
LSC Hubs project role: Digital Soil Mapping, input in Task 3.2, Task 3.3, Task 3.4 and Task 3.6
{{% /staff %}}
{{% staff name="Johan Leenaars" role="Soil science expert at ISRIC" img="/images/Johan_Leenaarsb500.jpg" %}}
LSC Hubs project role: Input in soil fertility use case
{{% /staff %}}
{{% staff name="Laura Poggio" role="Senior digital soil mapping and remote sensing expert at ISRIC" img="/images/laura.jpg" %}}
LSC Hubs project role: Task leader 3.2 LSC mapping and task leader 3.3 Soil Functional mapping
{{% /staff %}}
{{% staff name="Maria Ruperez Gonzalez" role="Digital soil mapping and GIS expert at ISRIC" img="/images/Maria_Ruiperez_Gonzalez.jpg" %}}
LSC Hubs project role: Support in LSC mapping in task 3.1, 3.2 and 3.3
{{% /staff %}}
{{% staff name="Silvana Summa" role="Communications manager at ISRIC" img="/images/silvana-summa.webp" %}}
LSC Hubs project role: Communications coordinator
{{% /staff %}}
{{% staff name="Ulan Turdukulov" role="Senior spatial data infrastructures expert at ISRIC" img="/images/ulan.jpg" %}}
LSC Hubs project role: Task leader 3.4 Architectural Design, and task leader 3.5 System building
{{% /staff %}}
{{% staff name="Judy Willems" role="Secretary at ISRIC" img="/images/Judy_Willem.jpg" %}}
LSC Hubs project role: Secrectary, coordination support
{{% /staff %}}
{{% staff name="Thaïsa van der Woude" role="Project coordinator at ISRIC" img="/images/thaisa.png" %}}
LSC Hubs project role: Work package 3 coordinator/lead
{{% /staff %}}


