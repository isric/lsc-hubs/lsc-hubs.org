---
title: 'World Agroforestry (ICRAF)'
date: 2018-11-28T15:14:54+10:00
icon: '/images/icraf.png'
featured: true
weight: 8
draft: false
heroHeading: 'World Agroforestry (ICRAF)'
heroSubHeading: 'World Agroforestry is a centre of science and development excellence that harnesses the benefits of trees for people and the environment.'
heroBackground: 'services/service1.jpg'
country: Kenya
link: https://worldagroforestry.org/
role: partner
---

{{% staff name="Ermias Betemariam " role="Land Health Scientist"  img="/images/Ermias_ICRAF.jpg"%}}
LSC Hubs project role: Project lead
{{% /staff %}}
{{% staff name="Weullow, Elvis" role="Senior Soil laboratory Enterprise Manager" img="/images/elvis.jpg" %}}
LSC Hubs project role: Support project implementation particularly in the capacity building 
{{% /staff %}}

{{% staff name="Ann Wavinya " role="Communication assistant at ICRAF" img="/images/ann.jpg" %}}
LSC Hubs project role: Communicate project activities
{{% /staff %}}
 {{% staff name="Leigh Winowiecki" role="Leader, Land Health Decisions" img="/images/Leigh_ICRAF.jpg" %}}
 LSC Hubs project role: Technical advisor
{{% /staff %}}

