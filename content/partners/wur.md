---
title: 'Wageningen University & Research'
date: 2018-11-18T12:33:46+10:00
icon: '/images/WUR_Website_LogoCard.png'
draft: false
featured: true
weight: 1
heroHeading: 'Wageningen University & Research'
heroSubHeading: 'Wageningen University & Research is a collaboration between Wageningen University and Wageningen Research with three core areas of research carried out internationally: 1) food, feed & biobased production, 2) natural resources & living environment and 3) society & well-being.'
heroBackground: 'services/service1.jpg'
country: Netherlands
link: https://www.wur.nl/en/about-wur.htm
role: team
---


{{% staff name="Frank van Weert" role="Project manager at Wageningen Centre for Development Innovation" img="/images/frank-vanweert.jpg" %}}
LSC Hubs project role: Project manager, Advisor institutional embedding of hub in AKIS in Rwanda
{{% /staff %}}
{{% staff name="Pascal Debons" role="Project manager at Wageningen Centre for Development Innovation" img="/images/pascal_WUR.jfif" %}}
LSC Hubs project role: Project manager
{{% /staff %}}
{{% staff name="Herman Snel" role="Rural Innovation and Livelihoods Advisor at Wageningen Centre for Development Innovation" img="/images/herman.jpg" %}}
LSC Hubs project role: Advisor institutional embedding of hub in AKIS in Ethiopia
{{% /staff %}}
{{% staff name="Esther Koopmanschap" role="Monitoring, Evaluation and Learning (MEL) Advisor at Wageningen Centre for Development Innovation" img="/images/esther.jpg" %}}
LSC Hubs project role: Strategic advisor for Communications and PMEAL
{{% /staff %}}
{{% staff name="Eunice Likoko" role="Policy advisor Gender and Nutrition at Wageningen Centre for Development Innovation" img="/images/eunice.jpg" %}}
LSC Hubs project role: Advisor institutional embedding of hub in AKIS in Kenya
{{% /staff %}}
{{% staff name="Flo Dirks" role="Agro-economic Advisor at Wageningen Centre for Development Innovation"  img="/images/no-photo.png" %}}
LSC Hubs project role: Advisor financial sustainability hubs
{{% /staff %}}
{{% staff name="Hermine ten Hove" role="Advisor Seed Sector and digitalization in agriculture" img="/images/Hermine_WCDI.jpg" %}}
LSC Hubs project role: Responsible for PMEAL
{{% /staff %}}
{{% staff name="Arnab Gupta " role="Advisor Seed Sector and digitalization in agriculture" img="/images/no-photo.png" %}}
LSC Hubs project role: Strategic advisor on digitalization in AKIS
{{% /staff %}}
{{% staff name="Aad Kessler" role="Researcher sustainable land management at Wageningen Environmental Research" img="/images/aad.jpg" %}}
LSC Hubs project role: Work Package 5 researcher 
{{% /staff %}}
{{% staff name="Hanneke Heesmans" role="Scientific researcher at Wageningen Environmental Research" img="/images/hanneke.jpg" %}}
LSC Hubs project role: Work package 5 leader
{{% /staff %}}
{{% staff name="Carlos Brazao Vieira Alho" role="Researcher in Sustainable Land Use at Wageningen Environmental Research" img="/images/Carlos_WENR.jpg" %}}
LSC Hubs project role: WP5 focal point Ethiopia 
{{% /staff %}}
{{% staff name="Mark van der Poel" role="Sustainable Farming Systems Researcher at Wageningen Environmental Research"  img="/images/Mark_WENR.jpg" %}}
LSC Hubs project role: WP5 focal point Kenya
{{% /staff %}} 


