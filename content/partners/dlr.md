---
title: 'Deutsches Zentrum für Luft- und Raumfahrt (DLR)'
date: 2018-11-28T15:14:54+10:00
icon: '/images/dlr.jpg'
featured: true
weight: 5
draft: false
heroHeading: "Deutsches Zentrum für Luft- und Raumfahrt (DLR)"
heroSubHeading: "The German Space Agency undertakes statutory tasks in the space sector on behalf of the German Federal Government. Within the scope of the tasks effectively assigned, they implement the space strategy of the Federal Government, develop and manage the national space programme, and represent the interests of the Federal Republic of Germany in space-related international bodies in accordance with the tasks assigned."
heroBackground: 'services/service1.jpg'
country: Germany
link: https://www.dlr.de
role: partner
---
