---
title: 'International Livestock Research Institute (ILRI)'
date: 2018-11-28T15:15:26+10:00
icon: '/images/ILRI_Website_LogoCard.png'
featured: true
weight: 3
draft: false
heroHeading: 'International Livestock Research Institute (ILRI)'
heroSubHeading: 'International Livestock Research Institute (ILRI) works for better lives through livestock in developing countries. ILRI is a CGIAR research centre, co-hosted by Kenya and Ethiopia, and provides global research partnership for a food-secure future.'
heroBackground: 'services/service1.jpg'
country: The Netherlands
link: https://www.ilri.org
role: team
---



{{% staff name="John Recha" role="Participatory Action Research" img="/images/john.jpg" %}}
LSC Hubs project role: Project manager at ILRI 
{{% /staff %}}

{{% staff name="Dawit Solomon" role="Regional program leader" img="/images/dawit.png" %}}
LSC Hubs project role: Work-packages scientific coordinator
{{% /staff %}}
{{% staff name="Brook Tesfaye Makonnen" role="Communications and Knowledge Management Lead" img="/images/no-photo.png" %}}
LSC Hubs project role: Communication support 
{{% /staff %}}
{{% staff name="Maren Radeny " role="Science Officer" img="/images/no-photo.png" %}}
LSC Hubs project role: Needs assessment and LSC-hub design and use at national level in Rwanda
{{% /staff %}}
{{% staff name="Abonesh Tesfaye" role="Senior Research Associate" img="/images/no-photo.png" %}}
LSC Hubs project role: Needs assessment and LSC-hub design and use at national level in Ethiopia 
{{% /staff %}}
{{% staff name="Gebermedihin Ambaw" role="Research Associate" img="/images/no-photo.png" %}}
LSC Hubs project role: Needs assessment and LSC-hub design and use at national level in Kenya 
{{% /staff %}}