---
title: 'Ethiopian Institute of Agricultural Research (EIAR)'
date: 2018-11-28T15:15:34+10:00
icon: '/images/EIAR_website_LogoCard.png'
featured: true
weight: 4
draft: false
heroHeading: 'Ethiopian Institute of Agricultural Research (EIAR)'
heroSubHeading: 'As a national research institute, Ethiopian Institute of Agricultural Research (EIAR) aspires to see improved livelihood of all Ethiopians engaged in agriculture, agro-pastoralism, and pastoralism through market-competitive agricultural technologies.'
heroBackground: 'services/service2.jpg'
country: Ethiopia
link: http://www.eiar.gov.et/
role: team
---


{{% staff name="Girma Mamo" role="Principal investigator" img="/images/girma.png" %}}{{% /staff %}}

{{% staff name="Dejene Abera" role="Researcher" img="/images/dejene.jpg" %}}
Agronomy and soil fertility
{{% /staff %}}
{{% staff name="Temesgen Desalegn" role="" img="/images/desalegn.jpg" %}}{{% /staff %}}