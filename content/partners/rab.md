---
title: 'Rwanda Agriculture and Animal Resources Development Board (RAB)'
date: 2018-11-18T12:33:46+10:00
icon: '/images/RAB_logo.jpg'
draft: false
weight: 6
featured: true
heroHeading: 'Rwanda Agriculture and Animal Resources Development Board (RAB)'
heroSubHeading: 'The Rwanda Agriculture and Animal Resources Development Board (RAB) champions the Rwandan agriculture sector into a knowledge-based, technology-driven and market-oriented industry, using modern methods in crop, animal, fisheries, forestry and soil and water management.'
heroBackground: 'services/service2.jpg'
country: Rwanda
link: http://www.rab.gov.rw/index.php?id=180
role: team
---



{{% staff name="Dr Pierre Celestin Ndayisaba" role="Soil health and Nutrition senior research fellow" img="/images/Pierre-celestin-ndayisaba.jpg" %}}
LSC Hubs project role: Rwanda coordinator
{{% /staff %}}
{{% staff name="Dr Florence Uwamahoro" role="Deputy Director General in Charge of Agriculture Development" img="/images/Florence_RAB.jpg" %}}
LSC Hubs project role: Overseeing Manager at RAB Senior Management level
{{% /staff %}}
