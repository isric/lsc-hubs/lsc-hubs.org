---
title: 'International Union for Conservation of Nature (IUCN)'
date: 2018-11-28T15:14:54+10:00
icon: '/images/iucn.png'
featured: true
weight: 3
draft: false
heroHeading: 'International Union for Conservation of Nature (IUCN)'
heroSubHeading: 'The International Union for Conservation of Nature (IUCN) is a membership Union uniquely composed of both government and civil society organisations. By harnessing the experience, resources and reach of its more than 1,400 Member organisations and the input of some 15,000 experts, IUCN is the global authority on the status of the natural world and the measures needed to safeguard it.'
heroBackground: 'services/service1.jpg'
country: Switzerland
link: https://www.iucn.org
role: partner
---

{{% staff name="Charles Karangwa" role="" img="/images/karangwa.jpg" %}}{{% /staff %}}
{{% staff name="Jules Rutebuka" role="" img="/images/jules.jpg" %}}{{% /staff %}}