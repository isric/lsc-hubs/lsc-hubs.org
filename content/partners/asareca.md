---
title: 'Association for Strengthening Agricultural Research in Eastern and Central Africa (ASARECA)'
date: 2018-11-28T15:14:54+10:00
icon: '/images/asareca.png'
featured: true
weight: 7
draft: false
heroHeading: 'Association for Strengthening Agricultural Research in Eastern and Central Africa (ASARECA)'
heroSubHeading: 'ASARECA is a not-for-profit inter-governmental sub-regional organization. ASARECA comprises 14 member countries: Burundi, Cameroon, Central African Republic, Democratic Republic of Congo, Eritrea, Ethiopia, Kenya, Madagascar, Republic of the Congo, Rwanda, South Sudan, Sudan, Tanzania and Uganda.'
heroBackground: 'services/service1.jpg'
country: Africa
link: https://www.asareca.org
role: partner
---


{{% staff name="Blaise Amony" role="Program Officer, Capacity Development at ASARECA" img="/images/Blaise_ASARECA.jpeg" %}}
LSC Hubs project role: Coordination and Capacity Development support
{{% /staff %}}
{{% staff name="Genevieve Apio" role="Communication Officer at ASARECA" img="/images/genevieve.jpg" %}}
LSC Hubs project role: Communication support
{{% /staff %}}
{{% staff name="Annet Wanyana " role="Interim Head of Finance" img="/images/Annet_ASARECA.jpeg" %}}
LSC Hubs project role: Finance suppor
{{% /staff %}}
{{% staff name="Moses Odeke " role="Interim Head of Programs  " img="/images/Moses_ASARECA.jpeg" %}}
LSC Hubs project role: Technical Support 
{{% /staff %}}

