---

title: Rwanda workshop series
date: 2022-12-20
author: Abraham Abhishek
weight: 110
image: /images/kigaliwshop.png
subtitle: "A data hub has recently been launched in Rwanda"
teaser: "Consultation workshops held in Rwanda at national and regional levels during the month of November 2022"
---
A series of consultation workshops were held by the project team in Rwanda during the month of November 2022. The workshops served to identify the needs and demands among stakeholders in the country with regards to land, soil and crop information. 

![](/images/kigaliwshop.png)

The series started with a national-level workshop in capital Kigali on November 10th-11th, 2022. This was followed by subnational-level workshops in Musanze (November 14th -15th) and Rwamagana (Nov 17th-18th) districts respectively. The subnational workshops helped engage stakeholders operating at a different level of governance, as well as in different agro-ecological zones where different sets of data needs might arise. 

A take-off point for project activities in Rwanda will be the ongoing RwaSIS Project, which is working on setting up an interactive soil information system for Rwanda.

A report on the workshop series is under preparation. A set of key informants will also be identified and interviewed soon to fill the gaps in the needs assessment that came out of the workshop.

The workshops were coordinated by the [International Livestock Research Institute](https://www.ilri.org), as the leader of work package 2 within the project which pertains specifically to the assessment of data needs among stakeholders in the three countries the project focuses on. They were led by the [Rwanda Agriculture and Animal Resources Development board](https://rab.gov.rw/index.php?id=180), the national project partner. A scientist from [Kenya Agricultural and Livestock Research Organisation](https://www.kalro.org/) helped co-facilitate and shared experiences from [workshops and activities in Kenya](https://lsc-hubs.org/2022/09/19/desira-lsc-is-project-engages-more-than-130-land-soil-crop-data-users-in-kenya) that had taken place prior to the Rwanda workshops.

The Kigali workshop was covered by the Rwandese national TV show 'Le Journal.' Here is a clip (aired November 10, 2022):

<iframe width="560" height="315" src="https://www.youtube.com/embed/a3kQ7JWBYXg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
