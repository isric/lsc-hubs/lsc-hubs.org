---
title: LSC Hubs to benefit different agroecological zones in Ethiopia 
date: 2023-02-13
author: Genevieve Apio (ASARECA), Brook Makonnen (ILRI), Emily Toner (ISRIC)
weight: 108
image: /images/ziwaybanner.jpg
subtitle: "Ethiopia Workshop Series"
teaser: "Regional workshops held in Ethiopia in January 2023"
---

{{% news-img title="Participants at the LSC Hubs regional workshop in Debre Birhan" img="/images/EthiopiaWorkshop_LSC-Hubs_Credit_BrookMakonnen.jpg" %}} 

In January 2023, the "Land, Soil, Crop Information Services for Climate-smart Agriculture in East Africa" (LSC Hubs) project held two sub-national Ethiopian stakeholders' workshops to gather inputs from local agricultural data providers and users in specific agroecological zones.

{{% news-img title="LSC Hubs Ethiopia Regions of Focus" img="/images/_Ethiopia_2023.png" row-class="w-75 mx-auto" %}}

{{% news-img title="Map of the two focal districts in Amhara and Oromia regions indicating the agroecological zones" img="/images/ziwayaemap.jpg" row-class="w-75 mx-auto" %}}

On January 25-26, LSC Hubs visited Adami Tulu Jido Kombolcha district in the central rift valley area of the Oromia regional state, which is classified as semi-arid agroecology with an annual rainfall ranging between 670 and 1000 mm. The farming system is typified by mixed crop-livestock system. Maize is the dominant crop, while wheat, teff and lowland legumes are also produced. Onion, tomato, pepper, cabbages, papaya are the most dominant crops grown under irrigation, mainly during the dry season. 

{{% news-img title="Typical landscape in Oromia regional state semi-arid area" img="/images/ziwaylandscape.jpg" row-class="w-75 mx-auto" %}}

This first subnational workshop attracted  57 participants from the Adami Tulu Jido Kombolcha district, ranging from farmers’ associations, the public sector, business companies, and development organizations, and staff from the local Ethiopian Institute of Agricultural Research (EIAR) station.

{{% news-img title="Workshop particpiants in Adami Tulu Jido Kombolcha" img="/images/ziwaygroup.jpg" row-class="w-75 mx-auto" %}}

Dr. Girma Mamo Director of Climate and Geospatial Biometrics Research at the Ethiopian Institute of Agricultural Research, while addressing stakeholders in Batu (Ziway), gave an overview and background of land, soil, and crop information services in Ethiopia. 

{{% quote-img title="Dr. Girma Mammo, Ethiopian Institute of Agricultural Research" img="/images/girmaziway.jpg" %}} 
“Ethiopian agriculture has demonstrated remarkable resilience over the centuries, despite facing a wide range of challenges related to land, soil, water, and climate that make farmers and livestock keepers more vulnerable due to their low adaptive capacity,” said Dr. Mamo.
{{% / quote-img %}}


On January 30-31, LSC Hubs held the second subnational workshop in Basona Werena, North Shewa zone of Amhara regional state, the eastern edge of the Ethiopian Highlands. Agroecologically, this area comprises of cool, moist mid-highlands (M4), Tepid sub-moist mid-highlands (SM3), and cool, sub-moist mid-highlands (SM4). The average annual rainfall of the area is 1013 mm. The farmers grow a variety of crops like wheat, barley, teff, fava bean, sweet potatoes and cassava. Napier grass is also widely produced to sustain dairy farming in the district. 

The workshop here attracted 74 participants from business institutions including banks, farmers associations, cooperatives, Debre Birhan University, the public sector, agricultural extension staff, development organizations, and staff from the local Ethiopian Institute of Agricultural Research (EIAR) station.

These workshops, along with a national gathering held on January 23 in Adama, contributed to the needs assessment for a climate-smart Land Soil and Crop Information Hub. In the workshops, agriculture stakeholders who work locally provided inputs on what is needed for new information services to serve this sector. Their inputs will help shape the design of the hub.

{{% news-img title="Participants at the workshop in Debre Birhan" img="/images/LSC_Hubs_Debre_Birhan_Workshop.jpg" row-class="w-75 mx-auto" %}}

Workshop participants discussed and identified various challenges, the most prominent of which was the need for more technical capacity at institutional and community levels regarding agricultural data accretion, access, dissemination, and usage. Other challenges included a lack of enabling infrastructure, such as modern soil and crop labs, and limited financial resources to support, maintain and run big data platforms. 

{{% news-img title="Participants at the workshop in Adami Tulu Jido Kombolcha" img="/images/ziwaywshoptwo.png" row-class="w-75 mx-auto" %}}

The Land Soil Crop Hubs project is expected to demonstrate the impact of improved data and information availability in informing decision-making processes of specific user groups such as national and local decisionmakers and farmers – and in doing so, to contribute to climate-smart agricultural transformation in rural East Africa.


**How will the climate-smart Land Soil Crop (LSC) Hub Benefit End users?**

This information hub will:

- support diverse practices which help the agricultural sector adapt and build resilience to climate change.
- account for changing environmental conditions and facilitate relevant climate change mitigation
- take advantage of the latest data technology so that people who need knowledge about land, soil and crops have the best information possible
- link with existing data sources and portals to facilitate easier access while maintaining national data ownership.

**More about this project:**

LSC Hubs is a four-year project (2021-2024) supported through funding from the [European Union’s Development of Smart Innovation through Research in Agriculture (DeSIRA) program](https://europa.eu/capacity4dev/desira/wiki/desira-projects), the Dutch Ministry of Foreign Affairs, and a contribution from ISRIC - World Soil Information with the aim to develop sustainable land, soil, and crop information hubs. The project's objective is to develop sustainable land, soil, and crop information hubs in national agricultural research organizations in East Africa. Ethiopia, Kenya and Rwanda  will host the information hubs to enhance the effectiveness of national Agricultural Knowledge and Innovation Systems (AKIS) and contribute to rural transformation and climate-smart agriculture.

