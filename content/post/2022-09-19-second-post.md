---

title: DeSIRA LSC-IS project engages more than 130 land-soil-crop data users in Kenya
date: 2022-09-19
author: ISRIC
weight: 111
image: /images/desira-workshop-kenya.jpg
subtitle: "the Land, Soil, and Crop Information Services (LSC-IS) to support Climate-Smart Agriculture project held a national stakeholders' workshop in Nairobi, Kenya."
teaser: "On 19-20 September 2022, the Land, Soil, and Crop Information Services (LSC-IS) to support Climate-Smart Agriculture project held a national stakeholders' workshop in Nairobi, Kenya. Over 40 participants from farmers' associations, public, private, and development organizations attended the two-day event.  "
---

Relevant county government officials from Busia and Taita Taveta counties, and officials from the Kenya Agricultural and Livestock Research Organization (KALRO), led the interaction, discussion, and debates on needs and usage of agricultural data and information at the grassroots- and national level.

{{% news-img title="Participants of the Nairobi workshop" img="/images/desira-workshop-kenya.jpg" %}} 

The national workshop in Nairobi was followed by two workshops in Busia and Taita Taveta counties, each attended by over 50 county and local stakeholders on respectively 19-20 September and 13-14 October 2022.

The workshops brought together national and local users of LSC data hubs in the agricultural knowledge and innovation system (AKIS) in Kenya. They were grouped into farmer representatives, public and private sectors, development partners, and knowledge institutes. Each group discussed current challenges in data access and use and specified data and training requirements. Also, the workshops discussed how scientific information provided by the hub needs to be translated to be useful for extension services, farmers, and others to improve decision making.

In his opening speech, Stephen Wathome – the European Union representative to Kenya – highlighted we are now witnessing a global food crisis that warrants prioritisation of food production. Improving soil fertility management and soil and water conservation continue to be a priority for the EU. He emphasized the need for simple, credible information that can be assimilated by a range of users, ranging from scientists and policymakers to farmers.

{{% news-img title="Stephen Wathome, the European Union representative to Kenya, giving the opening speech" img="/images/wathome-desira.jpg" %}} 

Mr Wathome:

“Information dissemination in any sector remains a vital attribute in ensuring that meaningful results are achieved. Generation and appropriate dissemination of land, soil, and crop information is crucial for effective decision-making and innovation.”    
“It is vital that national organisations take ownership and responsibility of this programme and we are happy to see that KALRO is taking up its role”.

The workshops were organised as part of the DeSIRA LSC-IS project by the International Livestock Research Institute (ILRI) and KALRO and supported by staff of ISRIC World Soil Information, Wageningen Centre for Development innovation (WCDI), Wageningen Environmental Research, Rwanda Agriculture Board (RAB), Ethiopian Institute of Agricultural Research (EIAR), World Agroforestry (ICRAF) and the Association for Strengthening Agricultural Research in Eastern and Central Africa (ASARECA)

Workshop host KALRO, Dr Michael Okoti of KALRO, explained in his keynote speech that in the three target countries, climate change has led to drastic land degradation, increased greenhouse gas emissions, and food and nutrition insecurity. In response, national governments have included climate-smart agriculture (CSA) in their policies. This project proposes an organized and accessible LSC information system for effective and well-informed decision-making by all users which will facilitate implementation of these policies.

{{% news-img title="Michael Okoti, Assistant Director for KALRO’s natural resource management division" img="/images/okoti-desira.jpg" %}}

Mr Okoti:

"The Kenyan hub will be hosted in KALRO and will focus on including soil productivity and soil and water management for climate smart agriculture (CSA). Farmers and other stakeholders will co-develop and use information with which they can improve land management practices and improve production amid the changing climatic condition in Kenya."
"This workshop is about creating a hub – a one-shop system. All the information that we need – soil nutrients, pH, weather forecast – will be found in the hub, thereby supporting our extension services … [and] farmers will have area-specific information all in one place."

The workshops received valuable contributions from county officials and farmer representatives. They observed that despite all information and data available at global and national levels, very little relevant data is accessible to users at county and local level and applauded efforts of the projects to engage them in the development of the hubs.

Florence Kigunzu, Deputy County Director for Agriculture at Busia County Government stated:

"The project adds a lot of value in what we [at the county level] are doing … In our sustainable use of land and agricultural resource base, we have soil and water management. [LSC-IS Project] will marry very well with the county's Kenya Climate-Smart Agriculture project (KCSAP) component."

{{% news-img title="Florence Kigunzu, Deputy Director for Agriculture, Busia County and her staff" img="/images/kigunzu-desira.jpg" %}} 
