---

title: "LSC catalogue prototype developed"
date: 2023-09-11
author: Thaïsa van der Woude and Emily Toner (ISRIC)
weight: 101
image: /images/AboutTheCatalogue.png
subtitle: ""
teaser: "Behind the scenes on a data and information hub, the catalogue is the principal component of the platform's architecture. In this blog, ISRIC project coordinator Thaïsa van der Woude who is guiding the multipartner team designing and building the hubs introduces the catalogue prototype."
---  

{{% news-img title="The catalogue prototype for the Rwanda Land Soil Crop Hub" img="/images/Rwanda-catalogue-screenshot.webp" %}}

The Land Soil Crop Hubs catalogue is a software component that allows [FAIR](https://www.go-fair.org/fair-principles/) (meta)data provisioning and creates an overview of the available land soil and crop (LSC) data, information, apps, and models in the project countries. To achieve the goal of having complete information accessible via the catalogue, the metadata for each resource included is described in a standardised format. This facilitates uploading the data, models, etc. into the catalogue.

<iframe width="560" height="315" src="https://www.youtube.com/embed/biIvlJpcxDk?si=E2hvGUuJixvsu09n" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

[To learn more about the metadata template use for this project, read this earlier project blog.](https://lsc-hubs.org/2023/03/23/process-established-for-metadata-inventory/)

## Digging into to the catalogue contents

At the moment, the LSC catalogue contains a compilation of land, soil, and crop information for each project country--Ethiopia, Kenya, and Rwanda. This data consists mostly of national and global spatial vector data (point, line, polygon), gridded maps and related metadata, and is collected from various sources as i) third parties (online sources), ii) ISRIC data holding, iii) country project partners (Ethiopia Institute of Agricultural Research (EIAR), Kenya Agricultural and Livestock Research Organization (KALRO), Rwanda Agriculture and Animal Resources Development Board (RAB)) and iv) project partners (ICRAF World Agroforestry, International Union for Conservation of Nature (IUCN)).

{{% news-img title="An example of one resource described in the Rwanda Land Soil Crop Hub's catalogue" img="/images/Catalogue-item-example.webp" %}}

The collected metadata are filtered on spatial scope. Table 1 shows the summary of the collected metadata. It shows that most collected data are on global or continental scale. These records may not be usable in this project and therefore, in the next step, the relevant records should be selected. In addition, for the hub to be relevant for its purpose, more regional (meta)data is needed. The national agriculture research system (NARS) partners  are working on gathering more (meta)data at this level.

*Table 1 Summary collected metadata\**
{{< table "table table-striped table-bordered" "border:1px solid red" >}}
| |N° collected |
|----|----|---|---|
|Spatial scope|Rwanda|Ethiopia|Kenya|
|Global|127|127|127|
|Continental| 115|117|116|
|National (country)|68|51|10|
|Regional (District /province/ subnational)|20|3|6|
|Local (plot, catchment)|11|4|0|
{{</table>}}
*\*The work is ongoing, and the numbers are changing regularly. The presented numbers may differ from the latest numbers of collected metadata.*

The collected metadata can also be filtered on number of datasets per LSC category. Figure 1 shows the number of datasets per LSC category. As the figure shows, soil holds the most gathered datasets which largely come from the ISRIC holding. Relevant (meta)data on crop and land are needed to cover all aspects of the LSC hub. The NARS are working on gathering more metadata on these categories as well.

{{% img-flex img-class="image-smaller" title="Figure 1 Datasets per LSC category" img="/images/LSC_Hubs_Catalogue_charts.webp"  %}}

The current developed LSC catalogue is a prototype and may not not represent the final version as this depends on the design by the NARS. The records and the layout are, therefore, subjected to constant changes. Any feedback on the LSC catalogue is highly appreciated to create a LSC catalogue that fits to country needs. The feedback can be send to Thaïsa van der Woude [(thaisa.vanderwoude@isric.org)](mailto:thaisa.vanderwoude@isric.org) or Paul van Genuchten [(paul.vangenuchten@isric.org)](mailto:paul.vangenuchten@isric.org).

The LSC catalogue for each of the project countries can be accessed by

- [Rwanda: LSC Hub Rwanda - Catalogue - Home (lsc-hubs.org)](https://rwanda.lsc-hubs.org/)
- [Ethiopia: LSC Hub Ethiopia - Catalogue - Home (lsc-hubs.org)](https://ethiopia.lsc-hubs.org/)
- [Kenya: LSC Hub Kenya - Catalogue - Home (lsc-hubs.org)](https://kenya.lsc-hubs.org/)

A factsheet on the LSC catalogue was created to describe its functionalities. The factsheet can be [downloaded here.](/documents/Factsheet-LSC-hub-v06.pdf)
