---

title: Project partners gathered in The Netherlands for 2023 Annual Project Meeting
date: 2023-05-25
author: Emily Toner (ISRIC)
weight: 1044
image: /images/news9-group-photo.jpg
subtitle: ""
teaser: "Land Soil Crop Hubs (LSC Hubs) project partners gathered in Wageningen, The Netherlands from May 9 - 13, 2023 for the Annual Project Meeting."
---  

{{% news-img title="Project partners gathered in The Netherlands for the Annual Project Meeting" img="/images/news9-group-photo.jpg" %}} 

Land Soil Crop Hubs (LSC Hubs) project partners gathered in Wageningen, The Netherlands from May 9 - 13, 2023 for the "Annual Project Meeting". Partners travelled from Ethiopia, Kenya, Rwanda and Uganda to be present and the group followed an interactive program to dig in on the next steps for each of the project's work packages. 

Joining the meeting's local hosts Wageningen Center for Development Innovation, ISRIC - World Soil Information and Wageningen Environmental Research were team members from International Livestock Research Institute, Ethiopia Institute of Agricultural Research (EIAR), Kenya Agricultural and Livestock Research Organization (KALRO), Rwanda Agriculture and Animal Resources Development Board (RAB), International Union for Conservation of Nature (IUCN), Association for Strengthening Agricultural Research in Eastern and Central Africa (ASARECA), and ICRAF World Agroforestry.

{{% news-img title="RAB and IUCN" img="/images/news9RAB-IUCN.webp" %}}

{{% news-img title="" img="/images/news9-notebook.webp" %}} 
{{% news-img title="" img="/images/news9-discussion.webp" %}}

Partners gave valuable input to help clarify the concept of the hub for each country building one and also to identify what has gone well and what areas can be improved for the project moving forward. 

In addition to meeting in Fletcher Hotel Wageningse Berg, the second day of the meeting took place in ISRIC's World Soil Museum. The group also took an outing to a floodplain nature reserve along the Rhine river and heard about The Netherland's environmental challenges and solutions for water management over recent decades. 

{{% news-img title="" img="/images/news9-discussion2.webp" %}}

{{% news-img title="" img="/images/news9-museum-tour.webp" %}}
{{% news-img title="" img="/images/news9-wild-horses.webp" %}}

{{% news-img title="" img="/images/news9-blauwe-kamer.webp" %}}

View [more images from the project meeting on Flickr.](https://www.flickr.com/photos/197148133@N03/albums/72177720308169031/)




