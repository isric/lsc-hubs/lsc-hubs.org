---

title: Launch event recap
date: 2022-02-11
author: Emily Toner
weight: 112
image: /images/NorthernKarongi_Rwanda_Photo_credit_Elie_Ntirengaya.jpg
subtitle: "A data hub has recently been launched in Rwanda"
teaser: "On Tuesday, January 25 a new project, Land Soil Crop Information Services (LSC Hubs), to support climate-smart agriculture in Ethiopia, Kenya and Rwanda held its public launch event online. "
---

Land Soil Crop Hubs brings together six key partners:

- Ethiopian Institute of Agricultural Research (EIAR)
- Kenya Agriculture and Livestock Research Organisation (KALRO)
- Rwanda Agriculture and Animal Resources Development Board (RAB)
- ISRIC - World Soil Information
- Wageningen University & Research
- International Livestock Research Institute (ILRI)

{{% news-img title="Image credit: unsplash.com" img="/images/NorthernKarongi_Rwanda_Photo_credit_Elie_Ntirengaya.jpg" %}} 

The work will be supported by collaboration with the Association for Strengthening Agricultural Research in Eastern and Central Africa (ASARECA), ICRAF World Agroforestry, the International Union for Conservation of Nature (IUCN) and German Aerospace Center (DLR).

The goal of the collaboration is to develop sustainable land, soil, crop information hubs in national agricultural research organizations to enhance the effectiveness of national agricultural knowledge and innovation systems and contribute to rural transformation and climate-smart agriculture in East Africa."

{{% news-img title="Image credit: isric.org" img="/images/LCS-hubs-small.png" %}} 

On Tuesday, January 25 a new project, Land Soil Crop Information Services (LSC Hubs), to support climate-smart agriculture in Ethiopia, Kenya and Rwanda held its public launch event online.




