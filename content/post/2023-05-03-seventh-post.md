---
title: LSC Information Services for Climate-Resilient Food Systems at ASARECA conference
date: 2023-05-25
author: Ermias Betemariam (CIFOR-ICRAF), Thaïsa van der Woude (ISRIC), Emily Toner (ISRIC)
weight: 106
image: /images/52651607146_286bd81a32_c.jpg
subtitle: ""
teaser: "A delegation from the Land Soil Crop Hubs project will present at the ASARECA Agriculture Ministerial Conference in Uganda from 17-19 May, 2023. "
---

{{% news-img title="Land, Soil, Crop Information Services for Climate-Resilient Food Systems in Africa at ASARECA conference" img="/images/52651607146_286bd81a32_c.jpg" %}} 

A delegation from the Land Soil Crop Hubs project will attend the 
[Association for Strengthening Agricultural Research in Eastern and Central Africa (ASARECA) Agriculture Ministerial Conference](https://www.asareca.org/summit/) in Uganda from 17-19 May, 2023. The theme of this gathering, which will be attended by the 14 ASARECA member states in East and Central Africa, is “Building Resilient Food Systems to Feed Africa for Generations.”



LSC Hubs project team members will present at 16:00 on Thursday, 18 May as part of the session on “Partnership for Inclusive and Sustainable Agricultural Transformation.” 

The presentations at the ASARECA conference will:
- Share lessons learned from LSC Hub development in Kenya, Ethiopia and Rwanda.
- Discuss how an LSC Hub system contributes to building resilient food systems and opportunities to scale it up to other ASARECA member countries.

The following people and institutions will be part of the LSC Hubs presentation:
<table><tr><th style="border-bottom: 1px solid black;padding: 10px 0;bg-color:var(--bs-primary) valign=center">Topic </th><th style="border-bottom: 1px solid black;">Speaker</th></tr>
<tr ><td rowspan=2 valign='center' width=30% >Overview of the LSC-IS  project </td><td>{{% speaker name="Thaïsa van der Woude MSc" role="Project coordinator" organisation="ISRIC - World Soil Information" img="/images/thaisa.png" %}}{{% /speaker %}}</td></tr>
<tr ><td> </td></tr>
<tr><td rowspan=2 valign='center'>Opportunity for LSC Information Services for scaling CSA in ASARECA region </td><td> {{% speaker name="Dr. John Recha" role="Scientist, Climate-Smart Agriculture and Policy"  organisation="International Livestock Research Institute (ILRI)" img="/images/john.jpg" %}}{{% /speaker %}}</td></tr>
<tr><td> </td></tr>
<tr><td rowspan=2 valign='center'>Lessons from Kenya </td><td>{{% speaker name="Dr. Kennedy Were" role="Research Geographer (spatial Scientist)"  organisation="Kenya Agricultural and Livestock Research Organization (KALRO)" img="/images/Kenedy-were.jpg" %}}{{% /speaker %}}</td></tr>
<tr><td><p></p></td></tr>
<tr><td rowspan=2 valign='center'>Lessons from Rwanda </td><td> {{% speaker name="Eric Nsabimana MSc" role="Data-GIS specialist" organisation="Rwanda Agriculture and Animal Resources Development Board (RAB)" img="/images/nsabimana.jpg" %}}{{% /speaker %}}</td></tr><tr><td> </td></tr>
<tr><td rowspan=2 valign='center'>Lessons from Ethiopia</td><td>{{% speaker name="Dr. Girma Mamo" role="Director, Climate,  Geospatial and Biometrics Research Directorate" organisation="Ethiopian Institute of Agricultural Research (EIAR)" img="/images/mamo.jpg" %}}{{% /speaker %}}</td> </tr><tr><td> 
</td></tr>
</table>



### Moderators

<table><tr><td valign="top">
{{% moderator name="Dr. Ermias Betemariam"  img="/images/Ermias.jpg" role="Land health scientist" description="Center for International Forestry Research –  World Agroforestry (CIFOR-ICRAF)"%}}</td><td valign="top"> 
{{% moderator name="Blaise Amony MSc " img="/images/Blaise_ASARECA.jpeg" role="Program officer Capacity Development" description="Association for Strengthening Agricultural Research in Eastern and Central Africa (ASARECA)" %}}</td valign="top"><td>
{{% moderator name="Dr. Jules Rutebuka"  img="/images/Rutebuka.png" role="Regional Program Officer, Sustainable Agriculture" description="The International Union for Conservation of Nature (IUCN)" %}} </td></tr>
</table>

See the [concept note](/documents/ASARECA-Agriculture-Ministerial-Conference-concept-note.pdf) for this session




