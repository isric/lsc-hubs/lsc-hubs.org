---

title: "Fieldwork underway to support climate-smart fertiliser recommendations in Ethiopia"
date: 2023-10-23
author: Musefa Redi Abegaz (WUR/ISRIC/EIAR), Emily Toner (ISRIC)
weight: 100
image: /images/jambo-m-pic-01-cropped.webp
subtitle: ""
teaser: "The recent fieldwork takes place in the study area which covers 92,000 km2 in central Ethiopia, focusing on maize and wheat crops."
---  

{{% news-img title="Musefa Redi Abegaze (right) assessing the root depth and density of a maize plant after carefully detaching the soil from the roots" img="/images/taye-pic-03.webp" %}}

During the 2023 main cropping season, research fieldwork was carried out in Ethiopia by PhD candidate Musefa Redi Abegaz whose research contributes to the Land Soil Crop Information Services (LSC Hubs) project. The goal of this fieldwork research is to validate the soil rootable depth map and the individual rootability-limiting soil factors (constraints), as well as the Root Zone Plant Available Water Holding Capacity (RZ-PAWHC) map for central Ethiopia, developed in the project. The fieldwork results will support climate smart agriculture, specifically to derive climate-smart fertilizer recommendations in Ethiopia.

“Understanding the soils beyond topsoil fertility such as root zone plant available water holding capacity is important for optimizing crop productivity and sustainable agriculture,” said Musefa.
{{% news-img title=" Counting visible maize roots using a metal frame of of 5 cm by 5 cm square grids in a soil pit located in Dugda District, East Shewa Zone of Ethiopia " img="/images/jambo-m-pic01.webp" %}}

Musefa is carrying out his PhD-research at Wageningen University under the supervision of Prof. dr. Gerard Heuvelink (Wageningen University, ISRIC), Johan Leenaars (ISRIC), Dr. Temesgen Desalegn (Ethiopian Institute of Agricultural Research,EIAR), and Dr. Mezegebu Getnet (Stichting Wageningen Research Ethiopia, SWR).

“Musefa’s PhD-research is novel in that it goes beyond mapping only basic soil properties but extends this to more complex soil properties, such as the root zone plant available water holding capacity. This is highly relevant information for farmers and land managers. The fieldwork provides valuable independent validation data needed to assess the accuracy of the maps produced,” explained Prof. dr. Gerard Heuvelink.

The fieldwork is conducted in a study area covering 92,000 km2 in central Ethiopia, focusing on maize and wheat crops. The measurements taken in the field include rootable soil depth and plant-available water holding capacity, which will be used to enhance location-specific and climate-smart fertilizer recommendations in a later stage of the project. In total, data will be collected from 50 sites across the study area.

{{% news-img title=" Location of the study area in Ethiopia" img="/images/location-of-study-area-ethiopia.webp" %}}
{{% img-flex img-class="img-portrait" title=" Musefa and fieldwork assistant taking measurements in a wheat field located in Dugda District, East Shewa Zone of Ethiopia" img="/images/jumbo-wheat-pic-05.webp" %}}

While the main emphasis of the LSC Hubs project is to bring together existing data, information and resources, through this research the project is also generating new, quality-assessed soil and crop information in Ethiopia. These data will facilitate informed decision-making for sustainable agricultural development at the national level.

“Ethiopia has relatively rich soil data and information holdings. Soil data are only relevant, though, if adequately interpreted and properly used,” stated Johan Leenaars. “Musefa will first validate soil rootability, which is the result of an interpretation of primary soil properties, and then use that information, besides other LSC information, to estimate the yearly variation in crop yield potentials, the associated crop nutrient requirements and the corresponding soil nutrient deficiencies.”

Interested to know more about this research? Contact Musefa with any follow up questions at musefa.abegaz@wur.nl.

[View more photos from the fieldwork in Ethiopia on Flickr.](https://www.flickr.com/photos/197148133@N03/albums/72177720312041831)


{{% img-flex img-class="img-portrait" title="Musefa and his colleague describing a soil profile and collecting soil samples at a maize field in Nedi Gibe District, Jimma Zone of Ethiopia" img="/images/kemer-pic-03.webp" %}}
