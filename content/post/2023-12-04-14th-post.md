---

title: "Digital soil mapping training completed in Rwanda "
date: 2023-12-04
author: Emily Toner (ISRIC)
weight: 99
image: /images/rwanda-20231121-093651.webp
subtitle: ""
teaser: "The primary focus of the training was joint development of a new set of high-resolution digital soil maps of Rwanda"
---  

{{% news-img title="" img="/images/rwanda-training-4887.webp" %}}

From November 21-23, the Land Soil Crop Hubs project carried out training on digital soil mapping in Musanze, Rwanda. Project team members from [ISRIC - World Soil Information](https://www.isric.org) delivered the training to Rwandan professionals with most coming from LSC Hubs project partner Rwanda Agriculture and Animal Resources Development Board (RAB). The primary focus of the training was joint development of a new set of high-resolution digital soil maps of Rwanda. 

This was the fourth training provided by ISRIC - World Soil Information in Rwanda. The effort is a continuation of a series of trainings on soil data standardization and digital soil mapping begun with the  Bill and Melinda Gates Foundation-funded 'Rwanda Soil Information Services' (RwaSIS). 

{{% news-img title=" Participants LSC Rwanda training" img="/images/rwanda-training-4906.webp" %}}

Thirteen people attended the training, coming from [RAB](https://www.rab.gov.rw/), the [Rwanda Ministry of Agriculture and Animal Resources](https://www.minagri.gov.rw/), the [Rwanda Space Agency](https://space.gov.rw/) and the [International Union for Conservation of Nature](https://iucn.org/). The training took place at the RAB station in Musanze. A brief tour of the training agenda:

- Day one: a refresher on the concepts of digital soil mapping and took a closer look at the steps of the digital soil mapping workflow. Participants practised with tutorials for each workflow step. 
- Day two: Participants dug deeper into the digital soil mapping workflow and practised the process that will help them develop and update their own digital soil maps using the statistical software R.
- Day three: Participants use the procedures learned to produce new gridded soil maps for Rwanda. In addition, trainers did an overview on Google's Earth Engine Data Catalogue and demonstrated how imagery contained in that catalogue can be obtained to support digital soil mapping

{{% news-img title="" img="/images/rwanda-20231121-093651.webp" %}}

{{% news-img title="" img="/images/rwanda-training-4896.webp" %}}




