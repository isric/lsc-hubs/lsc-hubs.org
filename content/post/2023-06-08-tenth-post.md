---

title: ASARECA conference provides valuable connections for agriculture knowledge and innovation
date: 2023-06-08
author: Thaïsa van der Woude and Emily Toner (ISRIC)
weight: 103
image: /images/52936977024_a4ef10738c_o.jpg
subtitle: ""
teaser: "On 18 May 2023, the Land Soil Crop Information Services (LSC Hubs) project team presented a session at the ASARECA (Association for Strengthening Agricultural Research in Eastern and Central Africa) Agriculture Ministerial Conference in Uganda."
---  

{{% news-img title="ASARECA Agriculture Ministerial Conference in Uganda" img="/images/52936977024_a4ef10738c_o.jpg" %}} 

On 18 May 2023, the Land Soil Crop Information Services (LSC Hubs) project team presented a session at the ASARECA (Association for Strengthening Agricultural Research in Eastern and Central Africa) Agriculture Ministerial Conference in Uganda. The team's session was about Land Soil and Crop Information Services for Climate-Resilient Food Systems sharing lessons learned from building an information services hub in Ethiopia, Kenya and Rwanda.  The project also had an exhibition booth. Through the session and the booth, LSC Hubs team members shared about the project concept to Ministries of Agriculture present at the conference from the 15 ASARECA member countries: Burundi, Cameroon, Central African Republic, Democratic Republic of Congo, Eritrea, Ethiopia, Kenya, Madagascar, Republic of the Congo, Rwanda, South Sudan, Sudan, Tanzania, Uganda and Somalia.

{{% news-img title="" img="/images/52936976749_aa1e7036bf_h-4.jpg" %}} 
{{% news-img title="" img="/images/52937214635_009cdf08b5_k.jpg" %}}

**Access the presentation slides here: [LSC Hubs Presentation ASARECA Conference.pdf](/documents/LSC-Hubs-Presentation-ASARECA-Conference.pdf)**

**View the full session presented by the project at the conference:**

<iframe width="560" height="315" src="https://www.youtube.com/embed/JFR9ox4gY6s?start=25735" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</br></br> 
At the project's exhibition booth, team members made many useful connections with people across East and Central Africa who are working on innovating in the agricultural sector,  particularly organisations and stakeholders who are working on climate-smart decision making support for farmers and land managers. LSC Hubs connected with people from Global Green Growth Initiative (GGGI), Digital Earth Africa, Uganda's National Agriculture Research Organisation (NARO), Sasakawa Africa Association and many others including Ministries of Agriculture from several ASARECA countries which could provide the opportunity for upscaling LSC Hubs.
</br></br> 

**View the presentation of the exhibition booth to the conference below:**

<iframe width="560" height="315" src="https://www.youtube.com/embed/3wJnTRgwukQ?start=839" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

{{% news-img title="" img="/images/news10g.jpg" %}}

{{% news-img title="" img="/images/52936826491_e8836431a4_k.jpg" %}}

{{% news-img title="" img="/images/news10h.jpg" %}}

Team members at the conference included:
- Tha&iuml;sa van der Woude, ISRIC
- Eric Nsabimana, RAB
- John Recha, ILRI
- Michael Okoti, KALRO
- Kennedy Were, KALRO
- Girma Mamo, EIAR
- Dejene Abera, EIAR
- Blaise Amony, ASARECA
- Genevive Apio, ASARECA
- Jules Rutebuka, IUCN
- Ermias Betemariam, ICRAF
- Emily Toner, ISRIC

View [more images from the LSC Hubs time at the 2023 ASARECA Ministerial Conference on Flickr.](https://www.flickr.com/photos/197148133@N03/albums/72177720308670721)


Check out the project's [social media highlights from the conference](https://wakelet.com/wake/1Ulviw4eeQtM3FH8Ysk5b)

