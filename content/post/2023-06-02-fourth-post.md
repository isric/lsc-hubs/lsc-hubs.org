---
title: New Ethiopian Agriculture Data Hub will Support Climate-Smart Decisions
date: 2023-02-06
author: Brook Makonnen (ILRI), Genevieve Apio (ASARECA), Emily Toner (ISRIC)
weight: 1099
image: /images/ethworkshopbanner.png
subtitle: "Ethiopia Workshop Series"
teaser: "Consultation workshops held in Ethiopia at national and regional levels during January 23-31, 2023"
---

{{% news-img title="Participants of the Ethiopia national LSC Hubs workshop" img="/images/ethworkshopbanner.png" %}} 

On January 23rd in Adama, Ethiopia, Land Soil Crop- Information Services's (LSC Hubs) lead Ethiopian project partner <a href="http://www.eiar.gov.et/">Ethiopian Institute for Agriculture Research (EIAR)</a> co-hosted a national stakeholders' gathering on the topic of how information hubs for climate-smart land, soil and crop data can contribute to rural transformation. The event attracted more than 60 governmental, private business, and development partners with a vested interest in creating, sharing and disseminating agricultural knowledge and innovation. 

During his welcome remarks at the event Director of Climate and Geospatial Biometrics Research at EIAR Dr. Girma Mamo mentioned that the concept of climate-smart agriculture and its linkage with ag data hubs has been studied and practiced in parts of the country, yet requires capacity and technological boost. In line with the Ethiopian Ministry of Agriculture’s <a href="https://cgspace.cgiar.org/rest/bitstreams/15afe490-d395-4a7e-959c-f2a111757750/retrieve">Climate-Smart Agriculture Roadmap (2021-2023)</a>, LSC Hubs project co-creates a transformational approach addressing bottlenecks in digitalizing agricultural extension and advisory services.

{{% quote-img title="Dr. Girma Mamo, EIAR" img="/images/DrGirmaMammo.jpg" %}} 
"A number of institutions, including farmer-based organizations, are doing their best to really understand the concept and practices of climate-smart agriculture, which is all about moving into modern means by also incorporating indigenous knowledge. For this to play out, data must be gathered, analyzed, interpreted and shared in easy-to-use platforms," said Dr. Mamo.
{{% / quote-img %}}

With the Ethiopian population being at least 120 million people by the end of 2022, the United Nations’ Food and Agriculture Organization (FAO) predicts a 70-percent growth in agricultural output will be needed to serve the projected demand for food by the year 2050. To increase yield and profits in an environmentally sustainable manner, agribusinesses, farmers, and growers must leverage agricultural data and innovation to improve productivity. 

The Ethiopia national Land Soil Crop (LSC) Hub will target government policy-making, agriculture knowledge organizations, development partners, and farmers countrywide and across the diversity of agroecological zones pictured below.


{{% news-img title="Map of Ethiopia's major agroecological zones" img="/images/lscethmap.jpg" row-class="w-75 mx-auto" %}} 


The project's nature and timely importance attracted high-level decision-makers to the January 23 event who are keen to influence agriculture-focused digitalization, rural transformation and research and development. Gracing the event at the highest level was H.E. Mr. Anteneh Fekadu, Advisor to the State Minister at the <a href="http://www.moa.gov.et/en/">Federal Ministry of Agriculture (MoA)</a> . Other higher-level public officials included Dr. Chimdo Anchala, Senior Director for Digital Agriculture and Rural Financing at the <a href="https://www.ata.gov.et/">Ethiopian Agricultural Transformation Institute (ATI)</a> and Dr. Birru Yitaferu, Director of Soil and Water Research at EIAR.

{{% news-img title="H.E. Mr. Anteneh Fekadu, Ethiopian MoA" img="/images/ethagmin.png" row-class="w-75 mx-auto" %}} 

In his opening speech in Adama, H.E. Mr. Anteneh Fekadu addressed the government's readiness to collaborate with initiatives aligning with several countrywide agricultural growth paths focusing on digitalization and providing vast data resources accrued through the ministry and regional research institutes and universities. 

"[The] Ethiopian government is committed to supporting initiatives that build standard national data-hub infrastructure that also employs digitally-enabled data analytics, along with human capacity development to use the system sustainably. This is clearly outlined in the ten years (2021-2030) perspective development plan and sector strategies … Further, such integrated products from the land, soil and crop data, coupled with farmers' data, field sensors, and remotely sensed data, fueled by analytical capabilities will enable near real-time and predictive information dissemination to the end users," said H.E. Mr. Anteneh Fekadu, MoA
 
Local, national and international research institutes and centers are currently tasked with catalyzing the transition of agricultural knowledge and innovation systems that incorporate up-to-date, accessible and affordable data on land, soil and crop information. EIAR is the main implementing partner of the LSC Hubs in Ethiopia and through this project, EIAR researchers will be empowered to undertake routine operations, maintenance and facilitation of the use of the hub nationally. 

For a start, the two pilot districts in Ethiopia will be Adami Tulu Jido Kombolcha district in the Oromia region and Basona Werana region in the Amhara region before rolling out the project to the rest of the country. In both project districts – Adami Tulu Jido Kombolacha and Basona Werana – the presence of satellite research centers customized for the local agro-ecological and climatic situation enhance and promote the uptake of approaches and scale-out for the benefit of smallholder farmers and support actors.

{{% news-img title="Regions of focus for LSC Hubs in Ethiopia" img="/images/_Ethiopia_2023.png" %}} 

This national gathering on January 23 was one of three fora that serve as information-needs assessments of users and providers at federal and local levels, as well as an evaluation of the capacity of the hub host, seeking to provide a fundamental insight for the design of the central and regional hubs. Furthermore, the consultative workshop focused on the use, access and affordability of a national ag-data hub on land, soil, crops, and relevant climatic information, linking this hub to the overall decision-making structure. 


{{% news-img title="Participant at Ethiopia national workshop for LSC Hubs stakeholders " img="/images/Adam_Workshop_Participant.jpg" row-class="w-75 mx-auto" %}} 


The 60+ participants from across the nation also delved into group write-shop exercises unpacking topics such as the nature of data user needs and skills, the type of facilitation needed for an open data culture on ag-related information and identification of AKIS players and their potential linkage to users such as smallholder farmers. Finally, the workshop attendees unloaded policy development and implementation frameworks and business-case modeling and development relevant to climate-smart agriculture. 

The workshop was covered by Amhara Media Corporation (AMECO) English News.' Here is the clip (aired January 23, 2023):

<iframe width="560" height="315" src="https://www.youtube.com/embed/bb12NbJ5DX8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
