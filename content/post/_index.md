---
title: 'News'
date: 2018-02-10T11:52:18+07:00
weight: 1
background: 'https://source.unsplash.com/zglUlG8k47I/1600x500'
featured_image: '/images/NorthernKarongi_Rwanda_Photo_credit_Elie_Ntirengaya.jpg'
---

This page lists some news items from the project. Find more upates on the [project progress dashboard](/#dashboard).