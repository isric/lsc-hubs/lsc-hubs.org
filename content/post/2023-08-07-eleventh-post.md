---

title: "Explainer: What is a land, soil, crop 'hub'?"
date: 2023-08-07
author: Thaïsa van der Woude and Emily Toner (ISRIC)
weight: 102
image: /images/Hub-visualv04.webp
subtitle: ""
teaser: "This project is creating 'hubs' related to land, soil and crop information in order to support climate-smart agriculture in East Africa. But what is a hub? This news item explains the concept."
---  

{{% news-img title="" img="/images/Hub-visualv04.webp" %}} 


The land, soil and crop (LSC) Hubs are easy-to-access online platforms, managed by the Ethiopia Institute of Agricultural Research (EIAR), Kenya Agriculture and Livestock Research Organisation (KALRO) and Rwanda Agriculture Board (RAB), enabling stakeholders to access information on soil, crops, land and climate such as data, maps and advisory services. 

The LSC Hub is designed based on stakeholder and user needs which are gathered through local workshops and co-developed with stakeholders. Access to this type of information supports improved decision-making at national, regional and local levels, especially as people need to adjust their approaches because of climate change. The focus of the hub design is to serve two use cases: soil fertility management and soil water conservation.

## LSC Hub Users and Stakeholders

The platform that we are designing brings together data users and providers which enables the exchange of knowledge and information between farmers, knowledge institutes, development partners, the private sector and policymakers. For example, when stakeholder groups attend workshops with us, their organisation's name and website are then added to the LSC Hub so that people know they are a resource and/or interested in land, soil and crop information.

On the LSC Hub, data users can find recent data and information which should help them improve decision-making on soil fertility and soil water conservation and find answers to agriculture related questions. They can also use the hub to find other stakeholders. Data providers can use the LSC Hub to reach a large group of potential users, promote their data and have a more significant impact.

{{% news-img title="Stakeholders gathered in Batu (Ziway), Ethiopia in January 2023 to discuss needs for a Land, Soil Crop Hub" img="/images/LSC-Hub-Stakeholders-Batu-Ziway-Ethiopia-2023.webp" %}}
																																			

There are three LSC Hubs under development in three countries: Ethiopia, Kenya and Rwanda. Ownership of each LSC Hub is with the National Agriculture Research Organisation in that country: Ethiopian Institute of Agricultural Research (EIAR), Kenya Agricultural & Livestock Research Organization (KALRO) and Rwanda Agriculture and Animal Resources Development Board (RAB). These organisations will host, maintain and operate the LSC Hub for their country

## LSC Hub Functions

{{% news-img title="A screenshot of the first LSC Hub prototype" img="/images/Prototype-screenshot.webp" %}}

The LSC Hub functionalities depend on the user's skills and needs. It may contain the following functionalities:
- LSC data catalogue: catalogue to find relevant open data;
- LSC predictive model description: an overview of technical models for assessing soil fertility and soil water conservation;
- LSC information services: listing of existing projects, initiatives, apps, services and tools;
- LSC use cases: description of the use cases and how to use the available information for the two use cases;
- LSC hub community: find other stakeholders.

The project partners provided their feedback on the LSC Hub functionalities during the [2023 annual project meeting](https://lsc-hubs.org/2023/05/25/project-partners-gathered-in-the-netherlands-for-2023-annual-project-meeting/). Based on the feedback, the functionalities of the LSC Hub were updated and the first prototype of an LSC Hub was created for each pilot country. A screenshot of the hub prototype is shown in the image above.

**Download a factsheet explaining the basic components of an LSC Hubs: [pdf](/documents/Factsheet-LSC-hub-v06.pdf)**
