---
title: 'DeSIRA LIFT'
date: 2018-11-28T15:15:34+10:00
icon: '/images/desiralift.jpg'
featured: true
weight: 1
draft: false
heroBackground: 'services/service2.jpg'
link: https://www.desiralift.org
---

[DeSIRA-LIFT](https://www.desiralift.org) is a service facility supporting [DeSIRA](https://europa.eu/capacity4dev/desira), an initiative funded by the European Commission that aims to contribute to climate-relevant, productive, and sustainable transformation of agri-food systems in the global South. DeSIRA supports research and innovation projects in Africa, Asia, and Latin America and strengthens research capacities and governance involving key actors at the national, regional, and global levels.

Here you will find information on the documents DeSIRA LIFT produces, events it organises and also opportunities for shorter of longer term experts it looks for to help the project with specific assignments put forward by [DG INTPA](https://commission.europa.eu/about-european-commission/departments-and-executive-agencies/international-partnerships_en)
