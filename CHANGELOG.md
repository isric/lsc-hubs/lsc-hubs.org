## [1.44.3](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.44.2...1.44.3) (2025-03-04)


### Bug Fixes

* add images RAB partner ([fc4df59](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/fc4df593078822f0d3872d95ecd072a1fea7afb2))

## [1.44.2](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.44.1...1.44.2) (2025-03-04)


### Bug Fixes

* change RAB partner ([c79aca5](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/c79aca56e14d542d185589afae6dcb4b09a3f763))

## [1.44.1](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.44.0...1.44.1) (2025-02-18)


### Bug Fixes

* add forgotten file fo news item 18 feb 2025 ([dcfcc6c](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/dcfcc6c364efb9083ce06550667f461a2f433d5f))

## [1.44.0](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.43.3...1.44.0) (2025-02-18)


### Features

* news item 18 feb 2025 ([d2ee1b2](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/d2ee1b209497e9b457ad8bc167c21d61bf715671))

## [1.43.3](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.43.2...1.43.3) (2025-02-06)


### Bug Fixes

* wrong upload ([84e26ca](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/84e26ca69c19894f3bf680569801f54c0eaba853))

## [1.43.2](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.43.1...1.43.2) (2025-02-06)


### Bug Fixes

* replace 2nd image on 250122 ([b4fa7d0](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/b4fa7d02772cfdec01016d3efe50c0255cd1d01c))

## [1.43.1](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.43.0...1.43.1) (2025-02-06)


### Bug Fixes

* change prelaunch to etechnical experst ([8485d1b](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/8485d1b44ccc762875d615f934345155953cd4d0))

## [1.43.0](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.42.0...1.43.0) (2025-02-04)


### Features

* add Ethiopia_AgData_prelaunch ([80db99a](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/80db99aefdba88d15e1b4793e26ba8237d46a6b8))

## [1.42.0](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.41.1...1.42.0) (2025-01-28)


### Features

* add dsm india dashboard tile ([81ef0b3](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/81ef0b3aebbbb8c6686725b4bdffb72c2db55cc1))

## [1.41.1](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.41.0...1.41.1) (2025-01-20)


### Bug Fixes

* change sub title in homepage features ([c605b39](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/c605b39e6dc70370c8ef6893e5318b82d302626b))

## [1.41.0](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.40.8...1.41.0) (2025-01-20)


### Features

* add Pascal Debons to wur partners ([77a6da4](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/77a6da44623aab06bd668f1513e5a9f667313097))
* add Pascal Debons to wur partners ([c7f6605](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/c7f66055552b9044782dc04c2024b4893624476a))

## [1.40.8](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.40.7...1.40.8) (2025-01-07)


### Bug Fixes

* cleanup up harbor ([643a619](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/643a6190e89a8c64ac08f5822c1520eb0acdf97a))

## [1.40.7](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.40.6...1.40.7) (2024-12-19)


### Bug Fixes

* add news item mombassa ([983538e](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/983538e02c0d15ed6edd3156bafc81fe5101c602))

## [1.40.6](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.40.5...1.40.6) (2024-12-17)


### Bug Fixes

* test cicd ([545ab71](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/545ab71644d20065245b138e5d4aee432531e46d))

## [1.40.5](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.40.4...1.40.5) (2024-12-12)


### Bug Fixes

* use webp image type ([20297bb](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/20297bbac4cc194c783f5d619f3fc6766e9a6e15))

## [1.40.4](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.40.3...1.40.4) (2024-12-12)


### Bug Fixes

* add social media tags ([355e917](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/355e9172839d6883b089fd4afb7dce43557376eb))

## [1.40.3](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.40.2...1.40.3) (2024-12-10)


### Bug Fixes

* update version harbor ([d53cfa7](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/d53cfa7fdee593cc69ae64f43226e0dbd70c1e56))

## [1.40.2](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.40.1...1.40.2) (2024-12-10)


### Bug Fixes

* minor textual changes ([6defa47](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/6defa47f4dbded0638be01c88df2b3725fb7636a))

## [1.40.1](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.40.0...1.40.1) (2024-12-10)


### Bug Fixes

* link dashboard to newsitem and correct url to flickr ([74e7167](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/74e7167874bcc88fe3e41a2fbe9018eddf7f7ca6))

## [1.40.0](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.39.0...1.40.0) (2024-12-09)


### Features

* add annual meeting kigali ([234cc68](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/234cc6838ba318f0c38898e7b7811945d3ecdf84))

## [1.39.0](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.38.0...1.39.0) (2024-11-26)


### Features

* remove asareca members ([6156bc3](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/6156bc34b22939284c2fb9dc1af62a3d9b13404e))

## [1.38.0](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.37.1...1.38.0) (2024-11-18)


### Features

* add november items betonny ([ec10cc5](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/ec10cc560a0cd87d35e9612a7971680d1affb578))

## [1.37.1](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.37.0...1.37.1) (2024-10-22)


### Bug Fixes

* test ([c819d63](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/c819d630a6b6f443acb37df15954adef1c078fbe))

## [1.37.0](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.36.4...1.37.0) (2024-10-16)


### Features

* add Et-DSM training ([b67e92f](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/b67e92f715d9da7fa2b01a87d60bb853e5cd8714))

## [1.36.4](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.36.3...1.36.4) (2024-09-25)


### Bug Fixes

* update x logo ([d4f71d3](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/d4f71d3340d917edc6be1b1c87707c9778b9dc77))

## [1.36.3](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.36.2...1.36.3) (2024-09-24)


### Bug Fixes

* smaller buttons ([5a8f9c4](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/5a8f9c4663f4b146f82caeaf248b9305f2714de4))

## [1.36.2](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.36.1...1.36.2) (2024-09-23)


### Bug Fixes

* add social media icons ([7cf8d06](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/7cf8d06ff3fc2ad6797525e413ee613e7bf7c68c))

## [1.36.1](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.36.0...1.36.1) (2024-09-23)


### Bug Fixes

* add hub links ([29d2a8f](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/29d2a8f1f5b271de69c5a02c12e267982cd58b34))

## [1.36.0](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.35.1...1.36.0) (2024-08-13)


### Features

* add LSC-IS-DSM-training Kenya ([7575e7c](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/7575e7c723309f8d13bd7e1130043f3b0e4073e8))

## [1.35.1](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.35.0...1.35.1) (2024-08-05)


### Bug Fixes

* remove vulnarability ([f32212a](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/f32212aecf51ea35415032b6249889dd451512a4))

## [1.35.0](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.34.0...1.35.0) (2024-07-18)


### Features

* Update 985-LSC-IS-DSM-products-workshop.md ([78a4081](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/78a40810481da8c52bebec7181b00af314740f62))

## [1.34.0](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.33.3...1.34.0) (2024-07-10)


### Features

* add Rwanda DSM workshop tile to dashboard ([c1912ba](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/c1912ba9007d67222a1dc89755dbc2997e7fbe73))

## [1.33.3](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.33.2...1.33.3) (2024-05-23)


### Bug Fixes

*  update other copyright to 2024 ([28ad7bf](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/28ad7bf8502317bcaa979a26f56db11cdc16a431))

## [1.33.2](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.33.1...1.33.2) (2024-05-22)


### Bug Fixes

* error WUR-Partner and update copyright to 2024 ([41930e9](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/41930e989563a1cfc096ebe269d0bc654dc04645))

## [1.33.1](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.33.0...1.33.1) (2024-04-15)


### Bug Fixes

* replace title of fertilizer summit to dashboard ([4d5a567](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/4d5a5671ca7fe140c3d8a53240d7cbddb3efaf12))

## [1.33.0](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.32.0...1.33.0) (2024-04-15)


### Features

* add fertilizer summit to dashboard ([1bc58e7](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/1bc58e7652fab44b4fd8031d16fbc2b410d2549c))

## [1.32.0](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.31.0...1.32.0) (2024-04-15)


### Features

* add fertilizer summit ([ce17baf](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/ce17bafbfa93a028c3982e677ffc83ad9148353e))

## [1.31.0](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.30.0...1.31.0) (2024-03-26)


### Features

* add Kenya monitoring post and dasboard ([55aaaf5](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/55aaaf5514440fb9a9ff4c687dfbdf936bf255ed))

## [1.30.0](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.29.0...1.30.0) (2024-03-21)


### Features

* add Ethiopia workshop news and tile ([af582cd](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/af582cd6af9833cc28bb9603e8b5730d8166688f))
* add Ethiopia workshop news and tile ([c649709](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/c64970956974ea73f55ece4a3ea0700320124bd9))

## [1.29.0](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.28.2...1.29.0) (2024-03-07)


### Features

* addd new design ([347fbb8](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/347fbb898a6125426c00f8a5ff1a70ac292f73b0))

## [1.28.2](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.28.1...1.28.2) (2024-02-27)


### Bug Fixes

* author's organisation ([32cd1f0](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/32cd1f0dd92b8228474f6af9916270d1b6389d1a))

## [1.28.1](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.28.0...1.28.1) (2024-02-27)


### Bug Fixes

* correct author's organisation ([4eb7fd7](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/4eb7fd73fdb1ba91dde5ec4069d531a8b20669b1))

## [1.28.0](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.27.2...1.28.0) (2024-02-23)


### Features

* update 991-kenya-training.md ([c4334d8](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/c4334d81fc15f565bc180d7bdfd7d3f5dc39c7d3))

## [1.27.2](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.27.1...1.27.2) (2024-02-21)


### Bug Fixes

* replace brackets, correct author name ([b8fd16c](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/b8fd16cc83e4fbda0ef20bf998d4d8aa2a594526))

## [1.27.1](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.27.0...1.27.1) (2024-02-19)


### Bug Fixes

* typo ([6eedbd0](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/6eedbd016d466d4b6b863539dc8cff4d10fe1c9a))

## [1.27.0](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.26.2...1.27.0) (2024-02-19)


### Features

* add kenya sub-nationale workshop, post and dashboard ([15eb3ad](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/15eb3ad595dc5e5091a0d2921897edb2a264090f))

## [1.26.2](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.26.1...1.26.2) (2024-02-16)


### Bug Fixes

* update readme ([471ac19](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/471ac19b5b7bfc61dfb04809d5e99c74e2046977))

## [1.26.1](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.26.0...1.26.1) (2024-02-08)


### Bug Fixes

* delete double kenya workshop 2023-12 ([770b26c](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/770b26c251fd922aacf7473179b870de6babf2b4))

## [1.26.0](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.25.1...1.26.0) (2024-02-08)


### Features

* kenya workshop 2023-12 ([24a52d7](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/24a52d74dc2a0152fd8729ea9ed198b157fbe4ae))

## [1.25.1](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.25.0...1.25.1) (2024-02-06)


### Bug Fixes

* change image format  kenya workshop ([287c35c](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/287c35c60db18c74631fba38643e6aab66debcd5))

## [1.25.0](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.24.0...1.25.0) (2024-02-06)


### Features

* add image kenya workshop ([c5098e6](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/c5098e6c2601757a26f53694322da716a1b9a746))

## [1.24.0](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.23.0...1.24.0) (2024-02-06)


### Features

* chage emily to silvana ([e7d831f](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/e7d831f30965d9782558602d323bfc9318c4ff2a))

## [1.23.0](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.22.0...1.23.0) (2024-02-01)


### Features

* add dashboard tile ([e5e39f5](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/e5e39f590f216637b72ff05dc54c5d2c9d8bef2c))

## [1.22.0](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.21.4...1.22.0) (2024-02-01)


### Features

* add dashboard tile ([040e0dc](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/040e0dceeed0423ac124a5369d6aacd0f1a9ab2f))
* update text ([15e183e](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/15e183ec407a7fc82cd43571ebb3bd0c881bfab2))

## [1.21.4](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.21.3...1.21.4) (2024-01-30)


### Bug Fixes

* Update methodologies.md ([3265b71](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/3265b711354d691d7ca0f4273783f0528a468821))

## [1.21.3](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.21.2...1.21.3) (2024-01-25)


### Bug Fixes

* stakeholderlink ([0247f22](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/0247f22251617c709edc1403b416bd74b0e223aa))

## [1.21.2](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.21.1...1.21.2) (2024-01-25)


### Bug Fixes

* rename kenya to Ethiopia ([fb82f62](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/fb82f622c12d4f134de4325d43b4b80de2546f88))
* stakeholderlist ([3be721f](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/3be721fe7bd740eb8e853ef528612c9b8ce87894))

## [1.21.1](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.21.0...1.21.1) (2024-01-23)


### Bug Fixes

* update ethiopia hub ([67d6c16](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/67d6c161eef60fbec3f5e95ac36ffc42cf588a31))

## [1.21.0](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.20.0...1.21.0) (2024-01-23)


### Bug Fixes

* dashboard item ([ef461df](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/ef461df71e81d97fdf2becd60d1c8206da47d1a1))
* dashboard tile ([772232c](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/772232cde8e5ddb2b94a75b3c00f8eff373df15f))


### Features

* add dashboard tile ([7e1e082](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/7e1e082dc57bd69c77519af057a0b429886ec551))
* add dashboard tile ([21a4ad1](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/21a4ad1c4406c364f5710ac56cf01532befc0b93))
* add dashboard tile ([b438aee](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/b438aeedc80d5871b02056f65163f10fd52e4d54))

## [1.20.0](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.19.0...1.20.0) (2023-12-18)


### Bug Fixes

* dashboard item ([1bec95c](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/1bec95c773f2f186f9f00bfed2f4425083a1e3b0))


### Features

* dashboard item kalro ([f54027e](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/f54027e1b38060b9a13040dc970fd3b3ab5c6858))

## [1.19.0](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.18.2...1.19.0) (2023-12-18)


### Features

* added post 17 ([ae855c5](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/ae855c54a99afdbb307c3e386c6d9de880dc7923))

## [1.18.2](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.18.1...1.18.2) (2023-12-18)


### Bug Fixes

* newsitem ([a670e35](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/a670e356820164fc5d94119ed600906f103a1307))

## [1.18.1](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.18.0...1.18.1) (2023-12-18)


### Bug Fixes

* ODK form ([248b49b](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/248b49b96da705fc55c37b9f07d4d210fe358607))
* ODK form. ([7ec250f](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/7ec250f72985e6931ac732bbe0e53980eb627f39))

## [1.18.0](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.17.0...1.18.0) (2023-12-07)


### Features

* add december 7th 2nd post ([5120c5f](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/5120c5f0cbf22d411fbdfc4d459082880b0df518))

## [1.17.0](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.16.0...1.17.0) (2023-12-07)


### Features

* add december 7th post ([42943e5](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/42943e5d909a808b183f88d01115b682dcb6e28f))

## [1.16.0](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.15.0...1.16.0) (2023-12-07)


### Features

* add december rwanda dashboard items ([fe25d6e](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/fe25d6ee025b8939fd776876b0acb67498f48f9e))

## [1.15.0](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.14.7...1.15.0) (2023-12-04)


### Features

* add 14th post ([3480274](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/3480274e8c4977d3a026ff2dfc767e069efcdde2))

## [1.14.7](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.14.6...1.14.7) (2023-11-22)


### Bug Fixes

* link broken ([910aa8a](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/910aa8ad1c4379d7e439a047f4fc5d1dcb37eac4))

## [1.14.6](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.14.5...1.14.6) (2023-11-20)


### Bug Fixes

* model ([8601153](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/860115323b3540ef943fd292894319f9ae9e7511))
* use model type  for models ([65415a0](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/65415a08c8f5834e644b276f1729577a99691bc3))

## [1.14.5](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.14.4...1.14.5) (2023-11-17)


### Bug Fixes

* add acidity story ([3aa1d96](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/3aa1d963b2b51fa9aaf54d1942f787fdd849fa9b))

## [1.14.4](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.14.3...1.14.4) (2023-11-17)


### Bug Fixes

* add methodology to rwanda ([8061b32](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/8061b32deee7d2c5fd0939670fe441dddf97bd57))

## [1.14.3](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.14.2...1.14.3) (2023-11-15)


### Bug Fixes

* spacing ([5ed4dca](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/5ed4dca1c451cf57ec5694f7e044c42ec2dceb7a))

## [1.14.2](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.14.1...1.14.2) (2023-11-02)


### Bug Fixes

* spelling mistake ([1f65dac](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/1f65dacb3dd8e277152608e945e112c06ad0631a))
* spelling mistake ([b1e58bb](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/b1e58bb8a62b5f56e0627dbb23049e94085bd312))

## [1.14.1](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.14.0...1.14.1) (2023-11-02)


### Bug Fixes

* repair without fix ([10c63c3](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/10c63c3ee5f4517a45534003068bf9ccbc23e70b))

## [1.14.0](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.13.1...1.14.0) (2023-11-02)


### Features

* add tiles mapviewer and feedback ([8467d05](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/8467d05578ec3e0f2c8184c44e10d85ceb7464c5))

## [1.13.1](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.13.0...1.13.1) (2023-10-31)


### Bug Fixes

* missing video tourimage ([65779c8](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/65779c812cfd759d0a8c291c5c4bcada00220eef))

## [1.13.0](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.12.0...1.13.0) (2023-10-31)


### Features

* videotour tile ([3778b26](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/3778b261e185a1613b59ba28a498ab2619562dba))

## [1.12.0](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.11.0...1.12.0) (2023-10-24)


### Features

* add dashboard 999 ([b12c547](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/b12c5476a22ee9fc9dd21ae5e5f8a843e466e794))

## [1.11.0](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.10.3...1.11.0) (2023-10-23)


### Features

* 135th post ([2dff844](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/2dff8442b8d026bb0eb4b5b2f7df25f6541df959))

## [1.10.3](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.10.2...1.10.3) (2023-10-13)


### Bug Fixes

* add link to maps ([88999d1](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/88999d116fdbed818b5924e1de659a3dfdab02ae))

## [1.10.2](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.10.1...1.10.2) (2023-10-10)


### Bug Fixes

* links to catalogs ([288d6be](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/288d6be80ebb1334fa281b3af15b96a91333fdaf))

## [1.10.1](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.10.0...1.10.1) (2023-09-27)


### Bug Fixes

* 1 dashboard file not updated for weight ([3683256](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/3683256815cda8ee8680f97cf0873bcf6dfc5156))

## [1.10.0](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.9.0...1.10.0) (2023-09-26)


### Features

* add news prototype to dashboard and change weight ([100d691](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/100d691d36edfd9b1fc1c46e568d95a0d2f3bd04))

## [1.9.0](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.8.24...1.9.0) (2023-09-18)


### Features

* added 12th post ([10f4fb1](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/10f4fb1d17eea054380d6f87202cc466f27a93b9))

## [1.8.24](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.8.23...1.8.24) (2023-09-12)


### Bug Fixes

* link to public git page ([3420e79](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/3420e79b25d19ae750c8133f6b03c83998ac19bb))

## [1.8.23](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.8.22...1.8.23) (2023-09-06)


### Bug Fixes

* ci test 12 ([53c9fd7](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/53c9fd7ff75c09bec6042c1efeb9a372c8c318e5))

## [1.8.22](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.8.21...1.8.22) (2023-09-06)


### Bug Fixes

* test 12 ([c9a56d6](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/c9a56d6213739b78fa6431763872073438f3fdd9))

## [1.8.21](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.8.20...1.8.21) (2023-09-06)


### Bug Fixes

* test 11 ([b9025cc](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/b9025ccb8a126e82f115b6c3144390619cbb9360))

## [1.8.20](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.8.19...1.8.20) (2023-09-06)


### Bug Fixes

* test 9 ([7750abc](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/7750abc18ff4387c04865444cdfadfdad552b88f))

## [1.8.19](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.8.18...1.8.19) (2023-09-06)


### Bug Fixes

* test 7 ([4055711](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/4055711d686dd4b7f9735173adab22f331d1e0c2))
* test 8 ([7203b96](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/7203b96baafedefd242ba81b8277510b2b3067b6))

## [1.8.18](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.8.17...1.8.18) (2023-09-06)


### Bug Fixes

* test 6 ([d4df3fb](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/d4df3fb757959f599ca42379104929dd2d79c22f))

## [1.8.17](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.8.16...1.8.17) (2023-09-06)


### Bug Fixes

* ci test5 ([473e387](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/473e3871176fbcce1ab84d271cebeba39835bb47))

## [1.8.16](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.8.15...1.8.16) (2023-09-06)


### Bug Fixes

* again ([53335c7](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/53335c795987ccccc75adc3a6e2e45e31bdd05e7))
* ci-test4 ([524ef22](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/524ef2262867b3ece75724eafd4174d72c866b01))

## [1.8.15](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.8.14...1.8.15) (2023-09-06)


### Bug Fixes

* no dev build on tags ([a1fbd5a](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/a1fbd5aed356ded1dfb3640719a307838c8b25fc))

## [1.8.14](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.8.13...1.8.14) (2023-09-06)


### Bug Fixes

* ci-test3 ([4c421fb](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/4c421fb07013cfe9ebb4e160ec63ef0ff6cf61bd))

## [1.8.13](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.8.12...1.8.13) (2023-09-06)


### Bug Fixes

* build dev if no tag ([148c5b5](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/148c5b5db9309bc13c7a2f2d8ec51452be952a95))

## [1.8.12](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.8.11...1.8.12) (2023-09-06)


### Bug Fixes

* no tags on .gitlab-ci.yml ([afb466b](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/afb466b00e9eb5d5a9df43140b961062e6e54e1d))

## [1.8.11](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.8.10...1.8.11) (2023-09-06)


### Bug Fixes

* 2 or 3? ([892ac8d](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/892ac8dd096eef9c6cfa3ebe14bd19b640c9f787))

## [1.8.10](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.8.9...1.8.10) (2023-09-06)


### Bug Fixes

* next iter CI2 ([17b84e5](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/17b84e560179d993fbeda0aaf2ac9e205a58f429))

## [1.8.9](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.8.8...1.8.9) (2023-09-06)


### Bug Fixes

* next iter CI ([6a9cac2](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/6a9cac25c76e57e18c7d4b32291bba7275b3fb56))

## [1.8.8](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.8.7...1.8.8) (2023-09-06)


### Bug Fixes

* ci next iter ([23de793](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/23de7931d1ed611580d7dfc17b0a36f2dceaac6e))

## [1.8.7](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.8.6...1.8.7) (2023-09-06)


### Bug Fixes

* test CI ([9ec2dc4](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/9ec2dc4d1dc5f62a22b03f5f8d7f9664e8389faa))

## [1.8.6](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.8.5...1.8.6) (2023-09-05)


### Bug Fixes

* update CI ([d453529](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/d4535290500f25c3b2131b8b33bfc75fa457d0c5))

## [1.8.5](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.8.4...1.8.5) (2023-09-05)


### Bug Fixes

* add git link to pages ([f78eedd](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/f78eedd3573ebd58d96faeb94b17f42b6edaa48c))
* add links to policy ([a2e3c62](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/a2e3c621874e48c987837e819e965de721009dc9))
* add policy section ([bf7fdaa](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/bf7fdaa051f19ac1f62820532a3a6d632adc661d))
* add rwanda policy and akis ([4cdd2e2](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/4cdd2e2833664264b9002398cb9638b3de252ddb))
* update CI ([5484c22](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/5484c22f677acb1490c81205014dfda8f23e9f1d))
* update methodologies ([566a10d](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/566a10d6a2a435298c331565dd76d2dbe28b9d7d))

## [1.8.4](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.8.3...1.8.4) (2023-08-25)


### Bug Fixes

* add methods from wp2 ([7f89531](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/7f8953116679d4920adbb7d80a89ea2a413e9629))

## [1.8.3](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.8.2...1.8.3) (2023-08-22)


### Bug Fixes

* extend examples ([efad6b7](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/efad6b72c255d9fd0bd2d28e3a107a061cb1478e))

## [1.8.2](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.8.1...1.8.2) (2023-08-18)


### Bug Fixes

* text review ([3102c18](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/3102c18de79c1225fd3bce5fa878198626b69a60))

## [1.8.1](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.8.0...1.8.1) (2023-08-09)


### Bug Fixes

* finaly found the title syntax ([1c96847](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/1c968475456e75942b53b90c39bdc38c7c27534a))

## [1.8.0](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.7.3...1.8.0) (2023-08-08)


### Features

* add dashboard item hub ([f908569](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/f9085699e199ea306a47cb095c726318b19efbc0))

## [1.7.3](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.7.2...1.7.3) (2023-08-08)


### Bug Fixes

* try to add : in title ([9d7381d](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/9d7381d102ce6e574a256aa37f023d5487b9913b))

## [1.7.2](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.7.1...1.7.2) (2023-08-08)


### Bug Fixes

* layout corrections ([8fd59ac](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/8fd59ace49f4f89e6af54826c9a75c07718991b0))

## [1.7.1](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.7.0...1.7.1) (2023-08-07)


### Bug Fixes

* add missing image in main  text 11th newsitem ([a211d75](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/a211d75f418bbb119c5af87d83594f1cba344cfe))

## [1.7.0](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.6.32...1.7.0) (2023-08-07)


### Features

* add 11th newsitem ([0278590](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/02785902e6fb2d43b45a61534e6849664a1192cc))

## [1.6.32](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.6.31...1.6.32) (2023-08-03)


### Bug Fixes

* add disclaimer ([ee2b314](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/ee2b3141f174bf9fe3737ce1760cd90d5d608f91))

## [1.6.31](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.6.30...1.6.31) (2023-08-02)


### Bug Fixes

* disable reactions ([cd929e7](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/cd929e72dfb56084ea513316631749fcf4ac50b2))

## [1.6.30](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.6.29...1.6.30) (2023-08-02)


### Bug Fixes

* remove foo debug message ([521fdb2](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/521fdb2b3279132d3153d69991eb3f4a22463dd8))
* script include ([3cab4a2](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/3cab4a2627d4ae5eb17070e766f477ded600189d))

## [1.6.29](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.6.28...1.6.29) (2023-08-02)


### Bug Fixes

* add comment option to hub pages ([e093b60](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/e093b60d8366222c2b6ee13d1d24adedc80b0b98))

## [1.6.28](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.6.27...1.6.28) (2023-07-26)


### Bug Fixes

* examples ([ad7cfde](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/ad7cfde098ef92cd756dfb6fe7cfe6108ee1dbc1))
* examples ([d3fcc01](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/d3fcc015568079fad8c7e2f58486941dd9a6ba4e))
* examples ([a7b1b3f](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/a7b1b3fc162c3d17b82abd53c3e865092f07a790))
* examples ([8a6986f](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/8a6986f95e415e52e092e09bbab83d15edc7a193))

## [1.6.27](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.6.26...1.6.27) (2023-07-26)


### Bug Fixes

* examples ([f4addf1](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/f4addf17fba29433770bcc15c15413c520c9170c))
* examples ([2036183](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/20361836420b8ec82a47a224bad228ecb082cb2f))

## [1.6.26](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.6.25...1.6.26) (2023-07-26)


### Bug Fixes

* example ([8191ee0](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/8191ee025a6ed7073c15171c28619ff863759c55))
* filter wocat ([a3eade0](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/a3eade0aa94d90d0e0919a983a47aafd0741d274))
* wocat filter ([6ce8edd](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/6ce8eddb2563c498a53a6cb35b24f1b2c33bd1c3))

## [1.6.25](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.6.24...1.6.25) (2023-07-26)


### Bug Fixes

* filter wocat ([ac5a35a](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/ac5a35a6956eb305bc3dee969ebeacf25f19be32))
* filter wocat ([38e79de](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/38e79de647df5e167df7f4d57009e80b3c8cded8))

## [1.6.24](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.6.23...1.6.24) (2023-07-21)


### Bug Fixes

* weight ([f1e50c4](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/f1e50c4dd1ca1e111666587a8b4a6575a4a6ba48))
* weight ([999cd84](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/999cd84e8b6d55b2c93e4a32dcf6620c61ca3e08))

## [1.6.23](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.6.22...1.6.23) (2023-07-21)


### Bug Fixes

* image ([48e2787](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/48e278776fdf8260ad8dc67d8d32059bd65d894c))

## [1.6.22](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.6.21...1.6.22) (2023-07-20)


### Bug Fixes

* dlr tile on dashboard ([ab5e626](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/ab5e6260b08e8acd305010fc01605abe26c8deb8))

## [1.6.21](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.6.20...1.6.21) (2023-07-07)


### Bug Fixes

* text ([8fc8039](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/8fc803904fb9202771781611e9cc1ce34826184d))
* text ([8705275](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/8705275af60739d7f37e162c899f3c70d5729ad8))

## [1.6.20](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.6.19...1.6.20) (2023-07-07)


### Bug Fixes

* catalogue ([0be83cb](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/0be83cbafa38941638df5ce4c9696a5f698b1bb2))

## [1.6.19](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.6.18...1.6.19) (2023-07-06)


### Bug Fixes

* rab eiar ([2cb09b8](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/2cb09b807deab9d0bbefbccc9061621dcb2aaddc))

## [1.6.18](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.6.17...1.6.18) (2023-07-06)


### Bug Fixes

* update hub UI for rwanda and ethiopia ([7f246c7](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/7f246c7351858ccda1109f4974962b26bd331e9e))

## [1.6.17](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.6.16...1.6.17) (2023-07-06)


### Bug Fixes

* text on homepage ([919e0c6](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/919e0c6c44b635f607789a75ed5aa082f78767d4))

## [1.6.16](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.6.15...1.6.16) (2023-07-06)


### Bug Fixes

* text ([0cc258e](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/0cc258eded356c1847045db0c4ba5ca7332855c3))

## [1.6.15](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.6.14...1.6.15) (2023-07-05)


### Bug Fixes

* text ([426ddc8](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/426ddc8fee18a0ca437cf1e679b7627375755153))
* text. ([4f24d95](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/4f24d95e24e36f939b55724678dd1e147680b1a7))
* update text. ([b291322](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/b291322727eb41a8351b6a3299ee4ad664c0e3c4))

## [1.6.14](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.6.13...1.6.14) (2023-07-05)


### Bug Fixes

* text ([ef5b4fb](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/ef5b4fb488e831684fb0da561e94be9f35e40d02))

## [1.6.13](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.6.12...1.6.13) (2023-07-05)


### Bug Fixes

* update text. ([550e229](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/550e2293efee89625f9ac7295f40e6554c7412d1))

## [1.6.12](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.6.11...1.6.12) (2023-07-05)


### Bug Fixes

* title had _.._ removed ([057f6d2](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/057f6d2a2a75779ece09b9d8f5445c2253a1a45d))

## [1.6.11](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.6.10...1.6.11) (2023-07-05)


### Bug Fixes

* update text ([2cf6fbf](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/2cf6fbf235b4372684add55e61138132653400ad))
* update title ([3fe75b2](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/3fe75b2627bff015b55429f10214bb86fea8c7b4))

## [1.6.10](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.6.9...1.6.10) (2023-07-05)


### Bug Fixes

* update icon ([9c02f99](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/9c02f999af4ff3b4026a1a6c682577fb391dad1a))
* update text. ([cd26301](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/cd263017107f0881b8f4ed9ce563498f431fcca6))

## [1.6.9](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.6.8...1.6.9) (2023-07-05)


### Bug Fixes

* icon update ([9bbfa04](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/9bbfa04855d19f7da41526c63fabd9bfc885bccd))
* icon update ([6058f98](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/6058f981f9148525567ae3da16f2c4759c838914))

## [1.6.8](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.6.7...1.6.8) (2023-07-05)


### Bug Fixes

* point removed ([5ad8cbf](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/5ad8cbf2a58364d55a9eff8ad2a32a00bdb7b217))
* title ([80eb46e](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/80eb46e08959571ef99cea153bfa99f83cccc6ea))

## [1.6.7](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.6.6...1.6.7) (2023-07-05)


### Bug Fixes

* icon code corrected ([f3fa823](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/f3fa823638127600b8d84bcad91a6cdafcbcb756))
* icon line corrected ([9c4be8b](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/9c4be8b5fbd9a7aa0df42c2e5b226339367f60fc))

## [1.6.6](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.6.5...1.6.6) (2023-07-05)


### Bug Fixes

* line icon changed to the right spacing ([680fa03](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/680fa036b7a4473ef87dc49d9143a6bd6dbe8f5a))

## [1.6.5](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.6.4...1.6.5) (2023-07-05)


### Bug Fixes

* icon update ([a783e4f](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/a783e4f9bee8f46d5c8b82c9a344fa7a649a31ce))

## [1.6.4](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.6.3...1.6.4) (2023-07-05)


### Bug Fixes

* update icon ([d04386d](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/d04386d3d5679ea6976462410c5ffc93dc259e7e))
* update icon application ([0c723c9](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/0c723c904b7813e8c07d8bc919a2b0a42763ee14))

## [1.6.3](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.6.2...1.6.3) (2023-07-05)


### Bug Fixes

* Update _index.md ([df2a4f9](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/df2a4f9d1ee5ee664ecfc1387a6db71c37a8404f))
* Update usecases.md ([9e261da](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/9e261da3bebf0f999be5c1fffb190228a10a09b5))

## [1.6.2](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.6.1...1.6.2) (2023-07-05)


### Bug Fixes

* split use case description to dedicated page ([2ae54ec](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/2ae54ec50c4d71ca23b9b3710bf4a0b2ed9a253a))

## [1.6.1](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.6.0...1.6.1) (2023-07-05)


### Bug Fixes

* small imprvmnts kenya hub ([90fc81c](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/90fc81c6c85a0601abf33e312ea2bac6986ec60b))

## [1.6.0](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.5.8...1.6.0) (2023-07-05)


### Features

* update kenya hub catalogue ([b6ec55b](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/b6ec55b387a45a91113cdd304b3e4f58dcfd69a7))

## [1.5.8](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.5.7...1.5.8) (2023-06-28)


### Bug Fixes

* link to asareca from dashboard ([b915f7a](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/b915f7a4211aa4f506f0e8e4189b68d669953aec))

## [1.5.7](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.5.6...1.5.7) (2023-06-22)


### Bug Fixes

* kalro link ([90a252a](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/90a252a14e126688259d2480f94102afdc0b0099))

## [1.5.6](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.5.5...1.5.6) (2023-06-22)


### Bug Fixes

* update kenya hub homepage ([7d7508c](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/7d7508cd4a5c84fe18765285685d75440c9af757))

## [1.5.5](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.5.4...1.5.5) (2023-06-13)


### Bug Fixes

* adapt categories ([ee7e3dd](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/ee7e3dd2f44e786a49dd00038cebe0cf6bb3568f))

## [1.5.4](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.5.3...1.5.4) (2023-06-08)


### Bug Fixes

* extra line, try to force up on prod ([d35eb1e](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/d35eb1e2f2d035f37f4b0a65b25ceb33b4737107))

## [1.5.3](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.5.2...1.5.3) (2023-06-08)


### Bug Fixes

* added too early ([6e514bf](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/6e514bf5bcbf51088559e3c197b772695a44e62a))

## [1.5.2](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.5.1...1.5.2) (2023-06-08)


### Bug Fixes

* add 2 meetings to dashboard ([45d9314](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/45d93144bd1e2de23910424c88a8fcc81702bb5b))

## [1.5.1](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.5.0...1.5.1) (2023-06-08)


### Bug Fixes

* correct last img and linebreak ([c7d3ce3](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/c7d3ce3b14a12a89982fe909904fca6745a4a701))

## [1.5.0](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.4.0...1.5.0) (2023-06-08)


### Features

* add ARASECA conference new item ([233fa92](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/233fa928f5accc2087939725b83cd01ad4c7ad00))

## [1.4.0](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.3.2...1.4.0) (2023-06-05)


### Features

* try if that makes display the image in the nineth post mainpage ([ba8ed13](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/ba8ed13c834ebf36eea9cdf2e2a9b5c448f7cc94))

## [1.3.2](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.3.1...1.3.2) (2023-05-31)


### Bug Fixes

* add lead image to main page ([5b5dd82](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/5b5dd82244c7dc1952260989f540af36bc3be598))

## [1.3.1](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.3.0...1.3.1) (2023-05-30)


### Bug Fixes

* some img not uploaded ([363e664](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/363e664f343907661f67642eed1f1b912302c35b))

## [1.3.0](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.76...1.3.0) (2023-05-25)


### Features

* news annual meeting ([a790108](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/a790108f3ceb4d3596a18fe1e6639a7ee122ed6d))

## [1.2.76](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.75...1.2.76) (2023-05-15)


### Bug Fixes

* oeps removed ) ([97f25c0](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/97f25c0ea13015b1ce41440099c6ca31acadcc8a))

## [1.2.75](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.74...1.2.75) (2023-05-15)


### Bug Fixes

* maybe spaces are preventing release ([2115bf3](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/2115bf37c79f8e085db904304b84223604ca4c24))

## [1.2.74](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.73...1.2.74) (2023-05-15)


### Bug Fixes

* upload newer ToT document ([4be5837](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/4be58372c89807fb7303e85f96b405f31d6b313d))

## [1.2.73](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.72...1.2.73) (2023-05-10)


### Bug Fixes

* typo news item ([27ca93a](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/27ca93a61c2a3be2a1201329e523c727017de299))

## [1.2.72](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.71...1.2.72) (2023-05-10)


### Bug Fixes

* add line break ([139f696](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/139f69609c04799f2d7078d117881dcf5f59baad))

## [1.2.71](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.70...1.2.71) (2023-05-10)


### Bug Fixes

* add spatial scope & hierarchy level ([072df79](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/072df7979dd4ce271f262247d39598449f135f75))

## [1.2.70](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.69...1.2.70) (2023-05-10)


### Bug Fixes

* add categories ([df78633](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/df78633cf7862d45299c52df9e244ca6e2fb5ac2))

## [1.2.69](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.68...1.2.69) (2023-05-10)


### Bug Fixes

* forgotten updates ([4f33955](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/4f339556e900fc844289c06172698088f73f6183))

## [1.2.68](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.67...1.2.68) (2023-05-09)


### Bug Fixes

* change caption of news image ([7aff7dd](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/7aff7dd8111f2ca05c54af32c8981151187466cc))

## [1.2.67](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.66...1.2.67) (2023-05-09)


### Bug Fixes

* add concept note and additions to ASARECA newsitem ([15a2e3e](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/15a2e3e60a40c5c276e7e94bd0a1ed85236b383f))

## [1.2.66](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.65...1.2.66) (2023-05-08)


### Bug Fixes

* ermias name ([75d940d](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/75d940d3d89e5ee3d36d656e2ff2cf425b47602a))

## [1.2.65](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.64...1.2.65) (2023-05-08)


### Bug Fixes

* asareca newsitem live ([b625dc9](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/b625dc94e42324d1a983cbffc9b0cc3530fef203))

## [1.2.64](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.63...1.2.64) (2023-04-04)


### Bug Fixes

* add partner info ([893ca82](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/893ca82813af2d965ceef59f1d86c62839044ba3))

## [1.2.63](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.62...1.2.63) (2023-03-28)


### Bug Fixes

* update link from news letter ([57de713](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/57de713c574e50297c7820fe01bdfaf10bc2b43d))

## [1.2.62](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.61...1.2.62) (2023-03-24)


### Bug Fixes

* changed method to process ([9018b3b](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/9018b3b73db735ba8345be07616e3409af30def8))
* changed weigths ([72c44aa](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/72c44aada557366ed7613bc8a386e79224a9191f))

## [1.2.61](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.60...1.2.61) (2023-03-24)


### Bug Fixes

* missing image betony ([a3d22be](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/a3d22be602ad5de8c0603843f420293dbde8acff))

## [1.2.60](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.59...1.2.60) (2023-03-24)


### Bug Fixes

* add bs-table shortcode ([abb7789](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/abb7789da12f718c497021aa66f0464530f5a85a))

## [1.2.59](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.58...1.2.59) (2023-03-24)


### Bug Fixes

* add id to dashboard item, so you can reference a tile ([1b17e96](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/1b17e962cc6217d6d2419534b4bfbf46a71585e3))

## [1.2.58](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.57...1.2.58) (2023-03-24)


### Bug Fixes

* update metadata item with link ([e8c0ee2](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/e8c0ee230977ac1015327554448bc8372ad33b76))

## [1.2.57](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.56...1.2.57) (2023-03-24)


### Bug Fixes

* add tiles and update stakeholders kenya ([c6b7fd5](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/c6b7fd5cf49979d4a6e65aa4100a8e7e9cbba34a))

## [1.2.56](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.55...1.2.56) (2023-03-23)


### Bug Fixes

* metadata news post ([b4f850b](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/b4f850b8f5acd1c490beefb4bee3264ab1f6de09))

## [1.2.55](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.54...1.2.55) (2023-03-23)


### Bug Fixes

* dashboard tile update ([754738e](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/754738e466fa0eacf8a46983ee199d808f0f3be8))
* tile arrangement and new metadata tiles ([0f314ac](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/0f314ac5e36a37610da45a49e2eb8bb928344c9a))

## [1.2.54](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.53...1.2.54) (2023-03-23)


### Bug Fixes

* new metadata dashboard tiles ([ca3481b](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/ca3481b17bb3a1480dbf3e266b0ab59ae9b37623))

## [1.2.53](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.52...1.2.53) (2023-03-22)


### Bug Fixes

* green focus color ([1413a70](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/1413a70fc5adc82edcb46b51f11c4d5a4d7f2126))

## [1.2.52](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.51...1.2.52) (2023-03-22)


### Bug Fixes

* update date ([5746540](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/574654005c8c93a786767e30655fb7bd6bdcaf15))

## [1.2.51](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.50...1.2.51) (2023-03-21)


### Bug Fixes

* add disclaimer ([9076b67](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/9076b672021bba9b896e3af58b0b6cbb26675f26))

## [1.2.50](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.49...1.2.50) (2023-03-21)


### Bug Fixes

* add subsections ([753ac1e](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/753ac1e20e148989119c090cd33c59ed42b90674))
* add subsections ([316f502](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/316f502bd3b4b2ec14568c28eb5dd0571680e55c))

## [1.2.49](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.48...1.2.49) (2023-03-21)


### Bug Fixes

* update ethiopia stakeholders ([aaaab7e](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/aaaab7eab2116bccd8b87f8162730d8b4fc71fc3))

## [1.2.48](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.47...1.2.48) (2023-03-20)


### Bug Fixes

* new video ([a311832](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/a311832dc8f2523b8d4b4adddaf05e145a76e8c0))

## [1.2.47](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.46...1.2.47) (2023-03-20)


### Bug Fixes

* added ISRIC people and photos ([c2a01bf](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/c2a01bfb8c01209fc4b5602b4783e4ba73474854))
* partner list ([e1e7b39](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/e1e7b39089e92427286b84c8ff6a22ef7a1dabfb))

## [1.2.46](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.45...1.2.46) (2023-03-07)


### Bug Fixes

* partner role description ([8b58136](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/8b581365cfa427251775c98eaf30f324b8931cd7))

## [1.2.45](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.44...1.2.45) (2023-03-07)


### Bug Fixes

* partner role description ([9dbd7fc](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/9dbd7fc9454518c87b19d164b616ff64d461731c))

## [1.2.44](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.43...1.2.44) (2023-03-07)


### Bug Fixes

* asareca people ([06f7344](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/06f7344a71c38285309f3e93ba010e6e576fd00c))

## [1.2.43](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.42...1.2.43) (2023-03-07)


### Bug Fixes

* ISRIC staff roles ([fc0b659](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/fc0b659c9cc9ecdf61d92504513d696f99b2e63d))

## [1.2.42](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.41...1.2.42) (2023-03-07)


### Bug Fixes

* Olivier photo ([9e7fb9b](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/9e7fb9b4e5fd74b13b65bb93319c63efdd39e84e))

## [1.2.41](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.40...1.2.41) (2023-03-07)


### Bug Fixes

* olivier added to partners page ([41f7fff](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/41f7fff97665d8a720e6b455ddf87ce809b26aff))

## [1.2.40](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.39...1.2.40) (2023-02-13)


### Bug Fixes

* added link ([a76c503](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/a76c50364e2593ae74862e9433a211c6fa2bbd4f))

## [1.2.39](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.38...1.2.39) (2023-02-13)


### Bug Fixes

* link ([7ff52b4](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/7ff52b4b1223f62bb19dab40c0648982bd039abd))

## [1.2.38](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.37...1.2.38) (2023-02-13)


### Bug Fixes

* publish new tile ([826c8f9](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/826c8f947fb0bf9a4ac3828c6ab8d0e74b80f6a5))

## [1.2.37](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.36...1.2.37) (2023-02-13)


### Bug Fixes

* publish new post ([a4523b1](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/a4523b11de23de3fbeaaf821a4280316b9d09cc5))

## [1.2.36](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.35...1.2.36) (2023-02-13)


### Bug Fixes

* Mamo name spelling ([12ebbfd](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/12ebbfda1ec328b0f853eed5ff3935e47bdce08e))
* trying to publish with new image formatting (previous commits failed) ([252d801](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/252d8011f678bcc9d0f17132515dfcf9beb8cee5))
* update image formatting ([0afb858](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/0afb858d583c0ca6576498aa71712b4217948a42))

## [1.2.35](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.34...1.2.35) (2023-02-13)


### Bug Fixes

* image formatting ([53f2ec0](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/53f2ec075ff96c290123f5b2346cc560fdccdb6f))

## [1.2.34](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.33...1.2.34) (2023-02-09)


### Bug Fixes

* new tile ([f51d667](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/f51d667041f5e7a126b20a2feed1454efccbc771))

## [1.2.33](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.32...1.2.33) (2023-02-09)


### Bug Fixes

* delete link ([2c8ec45](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/2c8ec4572c0f286b96629775dca62a646bcd6e13))
* link ([a4368bb](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/a4368bbb341179f6a8d554ad9afe8c9c0f4bce1a))
* new tile ([b8494b3](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/b8494b3ea8fa56d22443e9be996adb9bf15b0cfa))

## [1.2.32](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.31...1.2.32) (2023-02-09)


### Bug Fixes

* image display ([727186d](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/727186d6654058ee0beaf23c4b2a384fe58e74fb))

## [1.2.31](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.30...1.2.31) (2023-02-09)


### Bug Fixes

* image display ([dbd2af9](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/dbd2af9f0e0ac40cdd75472b37549e32ae7c5bd3))

## [1.2.30](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.29...1.2.30) (2023-02-09)


### Bug Fixes

* delete regional workshop post ([055d39c](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/055d39c6d4d1e9fd901f525445cad854219a86f4))

## [1.2.29](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.28...1.2.29) (2023-02-09)


### Bug Fixes

* remove center command ([f8e13eb](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/f8e13ebbd40174da5d476cd71c8d9ddad1f7eb88))

## [1.2.28](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.27...1.2.28) (2023-02-09)


### Bug Fixes

* center images ([ab9503d](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/ab9503df4aaf99b03fd879198dff7264ff0ce44f))

## [1.2.27](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.26...1.2.27) (2023-02-09)


### Bug Fixes

* center images ([4a7387c](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/4a7387c75ec50eb7343e537c58c18d6c034ce10d))

## [1.2.26](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.25...1.2.26) (2023-02-09)


### Bug Fixes

* image display ([3e087e0](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/3e087e0f78ba6ae5d43524678170730d34bcdf37))

## [1.2.25](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.24...1.2.25) (2023-02-09)


### Bug Fixes

* image display and authors ([5b104ee](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/5b104eeb8f1c49f15df6cf40fd1dfd32b1e9ef63))

## [1.2.24](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.23...1.2.24) (2023-02-09)


### Bug Fixes

* authors ([54c83f2](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/54c83f23a33d9f9760941ec12e04b5fb1feb0f00))

## [1.2.23](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.22...1.2.23) (2023-02-09)


### Bug Fixes

* image display ([edeb012](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/edeb01258eeb0cf2f31902d2ce855bd8db0d96eb))

## [1.2.22](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.21...1.2.22) (2023-02-09)


### Bug Fixes

* regions of focus map display ([4d14637](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/4d14637870d7e3e5c612df1d5e30110ba0644596))

## [1.2.21](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.20...1.2.21) (2023-02-09)


### Bug Fixes

* new tile ([b0b6f17](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/b0b6f17351df946988c8ff027dca26bfdaa22278))

## [1.2.20](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.19...1.2.20) (2023-02-09)


### Bug Fixes

* new ([84a8762](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/84a8762465992213966e05fc21ece26f0b259648))

## [1.2.19](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.18...1.2.19) (2023-02-09)


### Bug Fixes

* new ([0423587](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/0423587eef8d9c11b33ddb22883f3a81189e50a4))
* order ([6169933](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/61699334316f1d0a95d33d19204af42db170babc))
* order ([6fcfcb4](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/6fcfcb4fb75ee94d5ffcb3deb00236c4a16430f2))
* order ([7064a12](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/7064a121795539257ab3482da997010d12e648a2))
* order ([d5548fa](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/d5548fa459b1f80402ae509ce92bad309f06171b))
* text ([dd3722f](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/dd3722ffe9c81d6ab6ec5f09c4b924c655a9f939))
* text ([b811cea](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/b811cea8eea255a00ac93c9b7bc36308f4f86692))
* text ([747caa6](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/747caa678c00668b608ddc85a1af17561938e8fe))
* title ([3ecb4b1](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/3ecb4b16d917f59706c09dd12c7e4c082ac75244))
* title ([7892796](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/7892796624237fff3413d7192b0cd8b250be0a91))
* title ([8147d19](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/8147d19f454bc1ff56e94eab4dee41d6277dd90b))

## [1.2.18](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.17...1.2.18) (2023-02-09)


### Bug Fixes

* image formats ([77ecb2d](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/77ecb2d6a42bd32135006bab0605cd6b0ffb3865))
* image formatting ([122401d](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/122401d3794e0c41eae05b94a5268342798ad3da))

## [1.2.17](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.16...1.2.17) (2023-02-09)


### Bug Fixes

* with captions for select images ([1c515e3](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/1c515e30b472b5ff340c492f5212b74b5606445e))

## [1.2.16](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.15...1.2.16) (2023-02-09)


### Bug Fixes

* captions under select images ([6188ba9](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/6188ba9daf45f1fad7a3110ab2ba35d78f9b92c4))
* dashboard items for Ethiopia workshops ([7cd1b66](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/7cd1b66284714495ed8b6c9be8ba61a8ad53f83d))

## [1.2.15](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.14...1.2.15) (2023-02-08)


### Bug Fixes

* new tiles ([c6656f6](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/c6656f60428ebf6b8ca0bad461c1226baae098b0))

## [1.2.14](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.13...1.2.14) (2023-02-08)


### Bug Fixes

* add title to img ([2c66ed7](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/2c66ed7ae45e6b111d515d3077203d36ca6a0842))

## [1.2.13](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.12...1.2.13) (2023-02-08)


### Bug Fixes

* add banner ([879d4a0](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/879d4a0956a4f88688a765ec6c18e1c9470458d0))

## [1.2.12](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.11...1.2.12) (2023-02-08)


### Bug Fixes

* date of newsitem should be in the past ([a8cd536](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/a8cd536c26642db75e95919007ff48ce45efcace))

## [1.2.11](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.10...1.2.11) (2023-02-08)


### Bug Fixes

* dashboard update (maps & titles) ([ea75e14](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/ea75e1433e035e533f06c355bc62fc136ce9cf56))

## [1.2.10](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.9...1.2.10) (2023-02-08)


### Bug Fixes

* my new release ([222aa33](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/222aa336995bff470de8c9e3b5e7c6c4d8e82a5b))
* my new release ([8c5f16b](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/8c5f16b33cae3259ca24197fd02623e0a970cbb0))

## [1.2.9](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.8...1.2.9) (2022-12-23)


### Bug Fixes

* news item ([df129ed](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/df129ed607007e35770a61144b90156e6ae43d5f))

## [1.2.8](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.7...1.2.8) (2022-12-15)


### Bug Fixes

* add index page to tags and categories ([64bd0e6](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/64bd0e67297da6e72b6deb8ba064cde5ff58acb3))
* improve the partner and contact page ([33fe493](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/33fe49331c908e4947338f303786f414c0c7fd0b))

## [1.2.7](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.6...1.2.7) (2022-12-15)


### Bug Fixes

* updated the thesaurus ([5e25fa5](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/5e25fa5603c0cf86d18136d754e6fc01bf0cf40a))

## [1.2.6](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.5...1.2.6) (2022-12-15)


### Bug Fixes

* updated the thesaurus ([0792ef8](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/0792ef82bb74133b7513eb8be3834b5865f8c482))

## [1.2.5](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.4...1.2.5) (2022-12-15)


### Bug Fixes

* fix the partner resources section ([ab53de2](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/ab53de2d44f4b5b07a352071990d17f79d97b4ae))

## [1.2.4](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.3...1.2.4) (2022-11-24)


### Bug Fixes

* not resolved wur partners ([4f98e51](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/4f98e51ce4a449f2d8f3d623fd715d5b84758196))

## [1.2.3](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.2...1.2.3) (2022-11-24)


### Bug Fixes

* better align wur partners ([b3888cb](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/b3888cb6c9c4f8e3adff7f1a0d153ed3d9568eb0))

## [1.2.2](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.1...1.2.2) (2022-11-24)


### Bug Fixes

* to production ([5b1f947](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/5b1f947652099fdea21c001f2958748f263d415a))

## [1.2.1](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.2.0...1.2.1) (2022-11-17)


### Bug Fixes

* add robots.txt ([738cebd](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/738cebd85996b211b8a1bb0e57a38ebd99ae41f0))

## [1.2.0](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/compare/1.1.3...1.2.0) (2022-11-17)


### Features

* new website based on hugo ([6db8411](https://git.wur.nl/isric/lsc-hubs/lsc-hubs.org/commit/6db8411ee9c5af9d240beb5be41bc6c5bba49929))

### [1.1.3](https://git.wur.nl/isric/lsc-hubs.org/compare/1.1.2...1.1.3) (2022-01-24)


### Bug Fixes

* error image name ([c987b6b](https://git.wur.nl/isric/lsc-hubs.org/commit/c987b6b556f541c5382d90ac871b758a8af533f5))
* smaller header remove old text for mobile display ([18f340d](https://git.wur.nl/isric/lsc-hubs.org/commit/18f340dc8eef5d8732ba04d85647ed0122d6c583))

### [1.1.2](https://git.wur.nl/isric/lsc-hubs.org/compare/1.1.1...1.1.2) (2022-01-17)


### Bug Fixes

* adding Rab image and video frame ([7be48e2](https://git.wur.nl/isric/lsc-hubs.org/commit/7be48e279862804c376b4355e415037796b4c4ac))
* center logo RAB ([2268285](https://git.wur.nl/isric/lsc-hubs.org/commit/2268285d8e90fbcd93764633cdf8499e612765d9))
* final text corrections ([aeb208c](https://git.wur.nl/isric/lsc-hubs.org/commit/aeb208c447e73795bbd793983bd87407cb38dd5f))
* **pencil:** adjust image size RAB ([5444efb](https://git.wur.nl/isric/lsc-hubs.org/commit/5444efba5a8b15e69cd20614a224d4e3abc3c9be))

### [1.1.1](https://git.wur.nl/isric/lsc-hubs.org/compare/1.1.0...1.1.1) (2022-01-10)


### Bug Fixes

* **pencil:** Fix development url ([f57e60d](https://git.wur.nl/isric/lsc-hubs.org/commit/f57e60dab99d136a86d17d04988a958a8b2f5555))

## [1.1.0](https://git.wur.nl/isric/lsc-hubs.org/compare/1.0.0...1.1.0) (2022-01-10)


### Bug Fixes

* Add &nbsp; to see space ([1388607](https://git.wur.nl/isric/lsc-hubs.org/commit/1388607e1ad7fcc43edfc332bd046f04a9f18c94))
* Add &nbsp; to see space ([56c101d](https://git.wur.nl/isric/lsc-hubs.org/commit/56c101dc84e4680d5f3d796a33b9e157eb2d5632))
* nodejs and alpine version update. Extra echos ([0eec5f3](https://git.wur.nl/isric/lsc-hubs.org/commit/0eec5f399b1c5f98480d74111e687c7a99948b3e))
* replace card logos by placholders ([f662339](https://git.wur.nl/isric/lsc-hubs.org/commit/f662339f01ca783ffe4a8de269df4a76ef41746f))
* styling issues partners and video ([f7260d8](https://git.wur.nl/isric/lsc-hubs.org/commit/f7260d89f3bdae20f52521076e339545f2a89e54))
* **pencil:** Testing inital deployment ([339fc7c](https://git.wur.nl/isric/lsc-hubs.org/commit/339fc7c832542e591d5583187a05a0dab942acf3))


### Features

* Added logos and icon-text ([de0b8cc](https://git.wur.nl/isric/lsc-hubs.org/commit/de0b8ccc998c09cf83d46cec69b3a9402e3b3ec8))
* New images and styling, text added ([79cefbe](https://git.wur.nl/isric/lsc-hubs.org/commit/79cefbeba637328fb2931497a133b4cca7e5bc91))

## [1.0.0](https://git.wur.nl/isric/lsc-hubs.org/compare/...1.0.0) (2021-07-20)


### Bug Fixes

* Updated README.md ([fe4e457](https://git.wur.nl/isric/lsc-hubs.org/commit/fe4e45753094e16d7e081827374a3cd85f2e189c))

# Change Log

Changelog of lsc-hubs.org
